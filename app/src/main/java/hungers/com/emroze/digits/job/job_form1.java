package hungers.com.emroze.digits.job;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import hungers.com.emroze.digits.database.DatabaseHelper;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class job_form1 extends AppCompatActivity {
    public static String PREFS_NAME="digits";

    TextView tv_job,tv_salary,tv_hour,tv_date,tv_per_hour,tv_stime,tv_etime,tv_sdate,tv_per_week,tv_per_month,tv_per_year,tv_per_hour1,tv_optional;
    TextView tv_offday,tv_sun,tv_mon,tv_tue,tv_wed,tv_thu,tv_fri,tv_sat,tv_s,jan,feb,mar,apr,may,june,july,aug,sep,oct,nov,dec;

    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout1;
    private ExpandableLayout expandableLayout2;
    private ExpandableLayout expandableLayout3;
    private ExpandableLayout expandableLayout4;

    TimePicker t1;
    TimePicker t2;
    DatePicker d1;

    GifTextView btn_gif,off_day_gif;

    int hour_s, hour_e;
    int min_s, min_e;
    String AM_PM_s, AM_PM_e ;
    String db_stime = "",db_etime = "";
    String db_start_date = "",db_per_rate = "hour";
    String off_day = "0";

    Button save;

    Button pick_stime, pick_etime, pick_sdate;

    EditText et_job,et_salary;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    int i_sun = 0,i_mon = 0,i_tue = 0,i_wed = 0,i_thu = 0,i_fri = 1,i_sat = 1;

    int day ;
    int month;
    int year ;

    String display_s_time,display_e_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(hungers.com.emroze.digits.R.layout.activity_job_form1);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        display_s_time = "9 : 0";
        display_e_time = "17 : 0";
        db_stime="9:0";
        db_etime="17:0";

        tv_job = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_job_name);
        tv_salary = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_salary);
        tv_hour = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_hour);
        tv_date = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_start_date);
        tv_per_hour = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_hour);
        tv_per_hour1 = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_hour1);
        tv_per_week = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_week);
        tv_per_month = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_month);
        tv_per_year = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_year);
        tv_stime = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_stime);
        tv_etime = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_etime);
        tv_sdate = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_start_date_pick);
        tv_optional = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_optional);


        tv_stime.setText(display_s_time);
        tv_etime.setText(display_e_time);

        tv_offday = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_offday);
        tv_sun = (TextView) findViewById(hungers.com.emroze.digits.R.id.sun);
        tv_mon = (TextView) findViewById(hungers.com.emroze.digits.R.id.mon);
        tv_tue = (TextView) findViewById(hungers.com.emroze.digits.R.id.tue);
        tv_wed = (TextView) findViewById(hungers.com.emroze.digits.R.id.wed);
        tv_thu = (TextView) findViewById(hungers.com.emroze.digits.R.id.thu);
        tv_fri = (TextView) findViewById(hungers.com.emroze.digits.R.id.fri);
        tv_sat = (TextView) findViewById(hungers.com.emroze.digits.R.id.sat);
        //tv_s = (TextView) findViewById(R.id.tv_short);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        tv_job.setTypeface(type);
        tv_salary.setTypeface(type);
        tv_hour.setTypeface(type);
        tv_date.setTypeface(type);


        tv_fri.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
        tv_fri.setTextColor(Color.rgb(0,0,0));
        i_fri=1;

        tv_sat.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
        tv_sat.setTextColor(Color.rgb(0,0,0));
        i_sat=1;

        off_day_simulate();



        // for callopsing bar
        btn_gif = (GifTextView) findViewById(hungers.com.emroze.digits.R.id.btn_gif1);


        expandableLayout0 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_0);

        btn_gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_per_hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });


        expandableLayout1 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_1);

        tv_stime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        expandableLayout3 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_3);

        tv_etime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout3.isExpanded()) {
                    expandableLayout3.collapse();
                }
                else {
                    expandableLayout3.expand();
                }
            }
        });

        expandableLayout2 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_2);

        tv_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });

        expandableLayout4 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout);

        tv_optional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout4.isExpanded()) {
                    expandableLayout4.collapse();
                }
                else {
                    expandableLayout4.expand();
                }
            }
        });

//////////////////////////////////////////////

        //pick pre hour from callopsing bar


        tv_per_hour1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per hour");
                expandableLayout0.collapse();
                db_per_rate = "hour";
            }
        });
        tv_per_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per week");
                expandableLayout0.collapse();
                db_per_rate = "week";
            }
        });
        tv_per_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per month");
                expandableLayout0.collapse();
                db_per_rate = "month";
            }
        });
        tv_per_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per year");
                expandableLayout0.collapse();
                db_per_rate = "year";
            }
        });


        //time picker

        t1 = (TimePicker) findViewById(hungers.com.emroze.digits.R.id.timePicker1);
        t2 = (TimePicker) findViewById(hungers.com.emroze.digits.R.id.timePicker2);

        t1.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hour_s = hourOfDay;
                min_s = minute;

                /*if(hourOfDay < 12) {
                    AM_PM_s = "AM";
                } else {
                    AM_PM_s = "PM";
                }*/
            }
        });


        t2.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hour_e = hourOfDay;
                min_e = minute;

                /*if(hourOfDay < 12 ) {
                    AM_PM_e = "AM";
                } else {
                    AM_PM_e = "PM";
                }
*/
            }
        });

        pick_stime = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_pick_stime);
        pick_etime = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_pick_etime);


        pick_stime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pick_stime.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        pick_stime.getBackground().setAlpha(255);

                        double min1_s= min_s/60.0;

                        double hour1_s = hour_s + min1_s;

                        expandableLayout1.collapse();

                        display_s_time = String.valueOf(hour_s) + " : "+String.valueOf(min_s);

                        if(hour_s == 0 && min_s == 0){
                            tv_stime.setText(display_s_time);
                            db_stime = "";
                            Toast.makeText(job_form1.this, "Wrong Input", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            db_stime = String.valueOf(hour_s) + ":"+String.valueOf(min_s);
                            tv_stime.setText(display_s_time);
                        }


                        break;
                }
                return false;
            }
        });


        pick_etime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pick_etime.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        pick_etime.getBackground().setAlpha(255);

                        double min1_e= min_s/60.0;

                        double hour1_e = hour_e + min1_e;

                        expandableLayout3.collapse();

                        display_e_time = String.valueOf(hour_e) + " : "+String.valueOf(min_e);

                        if(hour_e == 0 && min_e == 0){
                            tv_etime.setText(display_e_time);
                            db_etime="";
                            Toast.makeText(job_form1.this, "Wrong Input", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            db_etime = String.valueOf(hour_e) + ":"+String.valueOf(min_e);
                            tv_etime.setText(display_e_time);
                        }


                        break;
                }
                return false;
            }
        });

        //pick date
        pick_sdate = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_pick_date);
        d1 = (DatePicker) findViewById(hungers.com.emroze.digits.R.id.datePicker);


        pick_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                day = d1.getDayOfMonth();
                month = d1.getMonth() + 1;
                year = d1.getYear();

                String m ="";

                if(month == 1){
                    m = "Jan";
                }
                else if(month == 2){
                    m = "Feb";
                }
                else if(month == 3){
                    m = "Mar";
                }
                else if(month == 4){
                    m = "Apr";
                }
                else if(month == 5){
                    m = "May";
                }
                else if(month == 6){
                    m = "June";
                }
                else if(month == 7){
                    m = "July";
                }
                else if(month == 8){
                    m = "Aug";
                }
                else if(month == 9){
                    m = "Sep";
                }
                else if(month == 10){
                    m = "Oct";
                }
                else if(month == 11){
                    m = "Nov";
                }
                else if(month == 12){
                    m = "Dec";
                }

                final String display_start_date = String.valueOf(day) + " "+m +", "+String.valueOf(year);

                db_start_date = String.valueOf(day) + "." + String.valueOf(month) +"."+String.valueOf(year) ;

                tv_sdate.setText(display_start_date);
                expandableLayout2.collapse();

            }
        });

        // save into the database

        et_job = (EditText) findViewById(hungers.com.emroze.digits.R.id.et_job_name);
        et_salary = (EditText) findViewById(hungers.com.emroze.digits.R.id.et_salary);


        save = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(et_job.getText().toString().equals("") || et_salary.getText().toString().equals("") ||
                        db_per_rate == "" || db_stime == "" || db_etime == "" || db_start_date == ""){

                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);

                    Toast.makeText(job_form1.this, "Totally fill the form", Toast.LENGTH_SHORT).show();
                }
                else{
                    //insert

                    final salary_rate s = new salary_rate();

                    int last_hour = s.hour(db_etime);

                    int last_min = s.min(db_etime);

                    int start_hour = s.hour(db_stime);

                    int start_min = s.min(db_stime);

                    Double s_h = (Double) (start_hour + (start_min/60.00));
                    Double l_h = (Double) (last_hour + (last_min/60.00));

                    if(s_h == l_h){
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(500);
                        Toast.makeText(job_form1.this, "Start time and End time can't be matched", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        off_day_calculation();

                        job_item job = new job_item();
                        job.setJob_name(et_job.getText().toString());
                        job.setSalary_amount(et_salary.getText().toString());
                        job.setSalary_type(db_per_rate);
                        job.setStart_time(db_stime);
                        job.setEnd_time(db_etime);
                        job.setStart_date(db_start_date);
                        job.setOff_day(off_day);


                        db_helper.insertJobs(job);
                        int i = preference.getInt("job_count",0) + 1;

                        editor.putString(et_job.getText().toString(),"0");
                        editor.putString("refresh","needed");
                        editor.putInt("job_count",i);
                        editor.putInt("s_hour",start_hour);
                        editor.putInt("s_min",start_min);
                        editor.putInt("s_date",day);
                        editor.putInt("s_month",month);
                        editor.putInt("s_year",year);
                        editor.putString("import","true");
                        editor.commit();

                        final double rate_d1 = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                                - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

                        Calendar c = Calendar.getInstance();

                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int min = c.get(Calendar.MINUTE);
                        int sec = c.get(Calendar.SECOND);
                        int date = c.get(Calendar.DAY_OF_MONTH);
                        int month1 = c.get(Calendar.MONTH);
                        int year1 = c.get(Calendar.YEAR);

                        pre_date_job_lack l = new pre_date_job_lack();

                        double lack = l.calc(getApplicationContext(),db_stime,db_etime,sec,min,hour,date,month1,year1,off_day);

                        double rate = s.calulate_salary_rate(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,off_day);

                        double rate_d = s.calulate_salary_rate_per_day(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,off_day);


                        lack = lack*rate;

                        rate_d = Double.valueOf(preference.getString("net_job_rate",""))+rate_d;

                        editor.putString("net_job_rate",String.valueOf(rate_d));
                        editor.commit();
                        //db_helper.update_rate_job(String.valueOf(rate_d));
                        //Toast.makeText(job_form1.this, "rate d "+String.valueOf(rate_d), Toast.LENGTH_SHORT).show();

                        rate_d = rate_d + Double.valueOf(preference.getString("net_asset_rate",""))
                                - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));


                        double goal = Double.valueOf(preference.getString("goal",""));

                        double net = Double.valueOf(preference.getString("net",""));

                        //Toast.makeText(getApplicationContext(), "net "+String.valueOf(net), Toast.LENGTH_SHORT).show();

                        net = goal - net - lack;

                        double days_count = net/rate_d;

                        int dayss = (int) days_count;


                        Calendar c1 = Calendar.getInstance();

                        int date11 = c1.get(Calendar.DAY_OF_MONTH);
                        int month11 = c1.get(Calendar.MONTH);
                        int year11 = c1.get(Calendar.YEAR);



                        SimpleDateFormat myFormat1 = new SimpleDateFormat("dd MM yyyy");
                        String inputString11 = String.valueOf(day) + " " + String.valueOf(month) + " " + String.valueOf(year); //start date
                        String inputString12 = String.valueOf(date11) + " " + String.valueOf(month11 + 1) + " " + String.valueOf(year11); //present date

                        Date date111 = null;
                        Date date12 = null;

                        double dif = 0;

                        try {

                            date111 = myFormat1.parse(inputString11);
                            date12 = myFormat1.parse(inputString12);
                            dif = getWorkingDaysBetweenTwoDates(date111, date12);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }



                        Calendar startc = Calendar.getInstance();
                        startc.setTime(date111);


                        Calendar endc = Calendar.getInstance();
                        endc.setTime(date12);


                        if (startc.getTimeInMillis() > endc.getTimeInMillis()) {

                            dayss = dayss + (int)dif;
                        }


                        //Toast.makeText(job_form1.this, "ddddd "+String.valueOf(dif), Toast.LENGTH_SHORT).show();

                        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                        String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date

                        Date date1 = null;
                        try {

                            date1 = myFormat.parse(inputString1);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        Calendar startCal = Calendar.getInstance();
                        startCal.setTime(date1);

                        //if(startCal.getTimeInMillis() <= endCal.getTimeInMillis())

                        //dayss = dayss+ (int)dif;

                        //Toast.makeText(job_form1.this, "dayss  "+String.valueOf(dayss), Toast.LENGTH_SHORT).show();

                        int r = 0;
                        do {
                            startCal.add(Calendar.DAY_OF_MONTH, 1);
                            r++;
                        } while (r < dayss); //excluding end date


                        String ddd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                +String.valueOf(startCal.get(Calendar.YEAR));

                        editor.putString("goal_date",ddd);
                        editor.commit();

                        //db_helper.update_goal_date(ddd);

                        //Toast.makeText(job_form1.this, "net  "+String.valueOf(rate_d), Toast.LENGTH_SHORT).show();



                        //Toast.makeText(job_form1.this, "lack  "+String.valueOf(lack), Toast.LENGTH_SHORT).show();

                        editor.putString(et_job.getText().toString(),String.valueOf(lack));
                        editor.putString(et_job.getText().toString()+"_check","true");

                        editor.putString("add","true");
                        editor.commit();

                        db_helper.update_jobs_net_amount(et_job.getText().toString(),String.valueOf(lack));

                        //Toast.makeText(getApplicationContext(), off_day, Toast.LENGTH_SHORT).show();

                        /*handler h = new handler();

                        h.job_timer(getApplicationContext(),
                                et_job.getText().toString(),
                                db_stime,
                                db_etime,
                                off_day,
                                et_salary.getText().toString(),
                                db_per_rate,
                                db_start_date
                        );


                        job_goal_date_handler j_h = new job_goal_date_handler();
                        j_h.job_goal_timer(getApplicationContext(),
                                et_job.getText().toString(),
                                db_stime,
                                db_etime,
                                off_day,
                                et_salary.getText().toString(),
                                db_per_rate,
                                db_start_date,
                                rate_d1
                        );*/
                        finish();
                    }
                }
            }
        });

    }

    public boolean start_date_extract(Context cccc,String s_date_a){

        Boolean bbb=false;

        String year_a = "";
        String month_a = "";
        String dd_a = "";
        int i =0,j=0;

        for(i = 0; i < s_date_a.length();i++){

            if(s_date_a.charAt(i) == '.'){
                break;
            }
            dd_a = dd_a.concat(String.valueOf(s_date_a.charAt(i)));
        }
        for(j = i+1; j < s_date_a.length();j++){

            if(s_date_a.charAt(j) == '.'){
                break;
            }
            month_a=month_a.concat(String.valueOf(s_date_a.charAt(j)));
        }
        for (int k = j+1; k < s_date_a.length();k++){
            year_a = year_a.concat(String.valueOf(s_date_a.charAt(k)));
        }


        int s_year_a= Integer.valueOf(year_a);
        int s_month_a = Integer.valueOf(month_a);
        int s_dd_a = Integer.valueOf(dd_a);

        s_month_a=s_month_a-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year_a){
            bbb=true;
        }
        else if(c_year == s_year_a){
            if(c_month > s_month_a){
                bbb=true;
            }
            else if(c_month == s_month_a){
                if(c_dd >= s_dd_a){
                    bbb=true;
                }
            }
        }
        return bbb;
    }

    public void off_day_calculation(){
        if(i_sun == 1){
            off_day = "1";
        }
        else {
            off_day = "";
        }
        if(i_mon == 1){
            off_day = off_day.concat("2");
        }
        if(i_tue == 1){
            off_day = off_day.concat("3");
        }
        if(i_wed == 1){
            off_day = off_day.concat("4");
        }
        if(i_thu == 1){
            off_day = off_day.concat("5");
        }
        if(i_fri == 1){
            off_day = off_day.concat("6");
        }
        if(i_sat == 1){
            off_day = off_day.concat("7");
        }

        if(i_sun == 0 && i_mon == 0 && i_tue == 0 && i_wed == 0 && i_fri == 0 && i_sat == 0){
            off_day = "0";
        }

    }
    public void off_day_simulate(){

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_sun == 0){

                    tv_sun.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_sun.setTextColor(Color.rgb(0,0,0));
                    i_sun=1;
                }
                else{

                    tv_sun.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_sun.setTextColor(Color.rgb(255,255,255));
                    i_sun=0;
                }
            }
        });
        tv_mon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_mon == 0){

                    tv_mon.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_mon.setTextColor(Color.rgb(0,0,0));
                    i_mon=1;
                }
                else{

                    tv_mon.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_mon.setTextColor(Color.rgb(255,255,255));
                    i_mon=0;
                }
            }
        });
        tv_tue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_tue == 0){

                    tv_tue.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_tue.setTextColor(Color.rgb(0,0,0));
                    i_tue=1;
                }
                else{

                    tv_tue.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_tue.setTextColor(Color.rgb(255,255,255));
                    i_tue=0;
                }
            }
        });
        tv_wed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_wed == 0){
                    tv_wed.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_wed.setTextColor(Color.rgb(0,0,0));
                    i_wed=1;
                }
                else{
                    tv_wed.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_wed.setTextColor(Color.rgb(255,255,255));
                    i_wed=0;
                }

            }
        });
        tv_thu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_thu == 0){
                    tv_thu.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_thu.setTextColor(Color.rgb(0,0,0));
                    i_thu=1;
                }
                else{
                    tv_thu.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_thu.setTextColor(Color.rgb(255,255,255));
                    i_thu=0;
                }

            }
        });
        tv_fri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_fri == 0){

                    tv_fri.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_fri.setTextColor(Color.rgb(0,0,0));
                    i_fri=1;
                }
                else{

                    tv_fri.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_fri.setTextColor(Color.rgb(255,255,255));
                    i_fri=0;
                }
            }
        });
        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_sat == 0){

                    tv_sat.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_sat.setTextColor(Color.rgb(0,0,0));
                    i_sat=1;
                }
                else{

                    tv_sat.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_sat.setTextColor(Color.rgb(255,255,255));
                    i_sat=0;
                }
            }
        });
    }

    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
            //return 0.0;
        }

        do {

            startCal.add(Calendar.DAY_OF_MONTH, 1);

            ++workDays;

        } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()); //excluding end date

        return workDays;
    }

}
