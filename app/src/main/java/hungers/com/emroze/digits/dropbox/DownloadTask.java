package hungers.com.emroze.digits.dropbox;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;


public class DownloadTask extends AsyncTask {
    public static String PREFS_NAME="digits";

    private DbxClientV2 dbxClient;
    private File file;
    private Context context;

    public DownloadTask(DbxClientV2 dbxClient, File file, Context context) {
        this.dbxClient = dbxClient;
        this.file = file;
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        try {
            OutputStream out = new FileOutputStream(file);
            FileMetadata f = dbxClient.files().download("/digits.db")
                    .download(out);



        } catch (DbxException e) {
            //Toast.makeText(context,e.toString(), Toast.LENGTH_SHORT).show();
            editor.putString("download","false");
            editor.commit();
        } catch (IOException e) {
            //Toast.makeText(context,e.toString(), Toast.LENGTH_SHORT).show();
            editor.putString("download","false");
            editor.commit();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        //importDB1();
        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("download","").equals("false")){
            Toast.makeText(context, "File not exist", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "File downloaded successfully", Toast.LENGTH_SHORT).show();
            editor.putString("download_ok","true");
            editor.commit();
        }



    }


    private void importDB1(){

        final SharedPreferences preference =context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/data/"+ "hungers.com.emroze.digits" +"/databases/digits.db";
        String backupDBPath = "/Digits/Download/digits.db";
        File currentDB = new File(sd, backupDBPath);
        File backupDB = new File(data, currentDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();

            Calendar cc = Calendar.getInstance();

            int hour = cc.get(Calendar.HOUR_OF_DAY);
            int min = cc.get(Calendar.MINUTE);
            int sec = cc.get(Calendar.SECOND);
            int date = cc.get(Calendar.DAY_OF_MONTH);
            int month = cc.get(Calendar.MONTH);
            int year = cc.get(Calendar.YEAR);

            editor.putString("import","true");
            editor.putString("backup_exit","true");
            editor.putInt("hour",hour);
            editor.putInt("min",min);
            editor.putInt("sec",sec);
            editor.putInt("date",date);
            editor.putInt("month",month);
            editor.putInt("year",year);
            editor.putString("trigger","on");
            editor.putString("resume_main","on");
            editor.putString("page","0");
            editor.putString("download","false");
            editor.putString("restart_after_download","true");
            editor.commit();

            //System.exit(0);

        } catch(IOException e) {
            e.printStackTrace();
        }
    }


}