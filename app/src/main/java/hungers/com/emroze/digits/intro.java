package hungers.com.emroze.digits;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import hungers.com.emroze.digits.database.DatabaseHelper;

import java.util.Calendar;

import io.saeid.fabloading.LoadingView;
import tyrantgit.explosionfield.ExplosionField;

public class intro extends AppCompatActivity {
    public static String PREFS_NAME="digits";
    
    private LoadingView mLoadingView;
    private LoadingView mLoadViewLong;
    private LoadingView mLoadViewNoRepeat;
    private ExplosionField m;
    DatabaseHelper db_helper = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        m = ExplosionField.attach2Window(this);

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.ddd : R.drawable.d1;
        int marvel_2 = isLollipop ? R.drawable.iii : R.drawable.i1;
        int marvel_3 = isLollipop ? R.drawable.g_new : R.drawable.g_new1;
        int marvel_4 = isLollipop ? R.drawable.iii : R.drawable.i1;
        int marvel_5 = isLollipop ? R.drawable.ttt : R.drawable.t1;
        int marvel_6 = isLollipop ? R.drawable.sss : R.drawable.s1;

        mLoadingView.addAnimation(Color.parseColor("#27F5BD"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#27F5BD"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#27F5BD"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#27F5BD"), marvel_4,
                LoadingView.FROM_BOTTOM);
        mLoadingView.addAnimation(Color.parseColor("#27F5BD"), marvel_5,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#27F5BD"), marvel_6,
                LoadingView.FROM_RIGHT);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        editor.putString("resume_main","on");
        editor.putString("resume_main_bill","on");
        editor.putString("resume_home","on");
        editor.putString("resume_home1","on");
        editor.putString("dropbox","true");

        editor.commit();



        if(preference.getString("intro",null) == null){


            final SQLiteDatabase db = db_helper.getWritableDatabase();
            db_helper.onCreate(db);

            Calendar c = Calendar.getInstance();

            int hour = c.get(Calendar.HOUR_OF_DAY);
            int min = c.get(Calendar.MINUTE);
            int sec = c.get(Calendar.SECOND);
            int date = c.get(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH);
            int year = c.get(Calendar.YEAR);

            editor.putString("intro","k");
            editor.putString("amount_jobs","0");
            editor.putString("amount_bills","0");
            editor.putString("amount_assets","0");
            editor.putString("amount_libs","0");
            editor.putString("page","0");
            editor.putString("goal","0");
            editor.putString("net","0");
            editor.putString("net_job_rate","0");
            editor.putString("net_asset_rate","0");
            editor.putString("net_lib_rate","0");
            editor.putString("net_bill_rate","0");
            editor.putString("job_main_thread","0");
            editor.putString("goal_date","00/00/0000");
            editor.putString("import","false");
            editor.putString("error","true");
            editor.putString("db_version","1");


            editor.putInt("intro_hour",hour);
            editor.putInt("intro_min",min);
            editor.putInt("intro_sec",sec);
            editor.putInt("intro_date",date);
            editor.putInt("intro_month",month);
            editor.putInt("intro_year",year);

            editor.commit();

        }
        /*mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });*/

        /*mLoadViewNoRepeat = (LoadingView) findViewById(R.id.loading_view);
        mLoadViewNoRepeat.addAnimation(Color.parseColor("#2F5DA9"), marvel_2, LoadingView.FROM_LEFT);
        mLoadViewNoRepeat.addAnimation(Color.parseColor("#FF4218"), marvel_3, LoadingView.FROM_LEFT);
        mLoadViewNoRepeat.addAnimation(Color.parseColor("#FFD200"), marvel_1, LoadingView.FROM_RIGHT);
        mLoadViewNoRepeat.addAnimation(Color.parseColor("#C7E7FB"), marvel_4, LoadingView.FROM_RIGHT);

        mLoadViewLong = (LoadingView) findViewById(R.id.loading_view_long);
        mLoadViewLong.addAnimation(Color.parseColor("#FF4218"), marvel_3, LoadingView.FROM_TOP);
        mLoadViewLong.addAnimation(Color.parseColor("#C7E7FB"), marvel_4, LoadingView.FROM_BOTTOM);
        mLoadViewLong.addAnimation(Color.parseColor("#FF4218"), marvel_3, LoadingView.FROM_TOP);
        mLoadViewLong.addAnimation(Color.parseColor("#C7E7FB"), marvel_4, LoadingView.FROM_BOTTOM);*/

        final Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(500);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mLoadingView.startAnimation();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                m.explode(mLoadingView);

            }
        }, 3000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();
                t.interrupt();

                Intent slideactivity = new Intent(intro.this, MainActivity.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
                //startActivity(slideactivity);

            }
        }, 4000);

    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    public void pause(View v) {
        mLoadingView.pauseAnimation();
        mLoadViewLong.pauseAnimation();
        mLoadViewNoRepeat.pauseAnimation();
    }

    public void start() {
        mLoadingView.startAnimation();
        mLoadViewLong.startAnimation();
        mLoadViewNoRepeat.startAnimation();
    }

    public void resume(View v) {
        mLoadingView.resumeAnimation();
        mLoadViewLong.resumeAnimation();
        mLoadViewNoRepeat.resumeAnimation();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
