package hungers.com.emroze.digits.job;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import hungers.com.emroze.digits.Fab;
import hungers.com.emroze.digits.database.DatabaseHelper;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.gordonwong.materialsheetfab.MaterialSheetFabEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class display_jobs extends AppCompatActivity {
    public static String PREFS_NAME="digits";

    TextView tv_d, tv_d_name,tv_wh,tv_sd,tv_rate,tv_total_sal,tv_salary,tv_delete;
    DatabaseHelper db_helper = new DatabaseHelper(this);


    String job_name,amount,s_type,s_time,e_time,s_date,off_day,start_date = "";
    String jobName;
    Double rate=0.0;

    private MaterialSheetFab materialSheetFab_job;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(hungers.com.emroze.digits.R.layout.activity_display_jobs);

        tv_d = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_jd);
        tv_d_name = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_jd_jo_name);
        tv_wh = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_job_wh);
        tv_sd = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_job_sd);
        tv_rate = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_jd_rate);
        tv_total_sal = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_total_amount);
        tv_salary = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_jd_salary);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        tv_d.setTypeface(type);
        tv_d_name.setTypeface(type);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        SQLiteDatabase db = db_helper.getReadableDatabase();

        jobName = preference.getString("job_name","");

        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
            if(cursor.moveToFirst()){
                do{

                    if(cursor.getString(cursor.getColumnIndexOrThrow("job_name")).equals(preference.getString("job_name",""))){

                        job_name = cursor.getString(cursor.getColumnIndexOrThrow("job_name")).toString();
                        tv_d_name.setText(cursor.getString(cursor.getColumnIndexOrThrow("job_name")));
                        amount = cursor.getString(cursor.getColumnIndexOrThrow("salary_amount"));
                        s_type = cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));
                        s_time = cursor.getString(cursor.getColumnIndexOrThrow("start_time"));
                        e_time = cursor.getString(cursor.getColumnIndexOrThrow("end_time"));
                        s_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));
                        off_day = cursor.getString(cursor.getColumnIndexOrThrow("off_day"));


                        break;
                    }
                }
                while(cursor.moveToNext());
            }

        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }



        salary_rate s = new salary_rate();

        rate = s.calulate_salary_rate(s_time,e_time,amount,s_type,off_day);

        String str_rate = String.format("%.5f",rate);

        //Toast.makeText(this,str_rate, Toast.LENGTH_SHORT).show();

        tv_rate.setText("$ "+str_rate);
        tv_wh.setText(s_time+" to "+e_time);
        tv_sd.setText(s_date);
        tv_salary.setText("$ "+amount+"/"+s_type);

        tv_delete = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_delete);



        Fab fab = (Fab) findViewById(hungers.com.emroze.digits.R.id.fab_job_edit);
        View sheetView = findViewById(hungers.com.emroze.digits.R.id.fab_sheet);
        View overlay = findViewById(hungers.com.emroze.digits.R.id.overlay);
        int sheetColor = Color.rgb(255,255,255);
        int fabColor = Color.rgb(0,0,0);

        // Create material sheet FAB
        materialSheetFab_job = new MaterialSheetFab<>(fab, sheetView, overlay, sheetColor, fabColor);

        materialSheetFab_job.showFab();
        // Set material sheet event listener
        materialSheetFab_job.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {

            }

            @Override
            public void onHideSheet() {

            }
        });

        TextView add_job = (TextView) findViewById(hungers.com.emroze.digits.R.id.edit_job);
        add_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialSheetFab_job.hideSheet();
                /*Intent intent = new Intent(getApplicationContext(),edit_job.class);
                startActivity(intent);*/
                /*editor.putString("edit","on");
                editor.commit();*/
            }
        });


        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialSheetFab_job.hideSheet();
                new SweetAlertDialog(display_jobs.this, SweetAlertDialog.WARNING_TYPE)

                        .setTitleText("Are you sure??")
                        .setContentText("It can delete your item")
                        .setConfirmText("Yes,delete it!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {

                                db_helper.delete_job(job_name);
                                double job_dif = 0;
                                double bill_dif = 0;
                                double asset_dif = 0;
                                double lib_dif = 0;


                                Calendar c1 = Calendar.getInstance();

                                int date11 = c1.get(Calendar.DAY_OF_MONTH);
                                int month11 = c1.get(Calendar.MONTH);
                                int year11 = c1.get(Calendar.YEAR);

                                SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                                String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date


                                final SQLiteDatabase db = db_helper.getWritableDatabase();
                                final Cursor cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
                                final Cursor cursor1 = db.rawQuery("SELECT rowid _id,* FROM bills", null);
                                final Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM asset", null);
                                final Cursor cursor3 = db.rawQuery("SELECT rowid _id,* FROM lib", null);


                                if(cursor.moveToFirst()) {
                                    do {

                                        String s_d = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));
                                        String y = "";
                                        String m = "";
                                        String d1 = "";
                                        int i =0,jj=0;


                                        for(i = 0; i < s_d.length();i++){

                                            if(s_d.charAt(i) == '.'){
                                                break;
                                            }
                                            d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                        }
                                        for(jj = i+1; jj < s_d.length();jj++){

                                            if(s_d.charAt(jj) == '.'){
                                                break;
                                            }
                                            m=m.concat(String.valueOf(s_d.charAt(jj)));
                                        }
                                        for (int k = jj+1; k < s_d.length();k++){
                                            y = y.concat(String.valueOf(s_d.charAt(k)));
                                        }


                                        int s_year= Integer.valueOf(y);
                                        int s_month = Integer.valueOf(m);
                                        int s_dd = Integer.valueOf(d1);


                                        String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                        Date date111 = null;
                                        Date date12 = null;



                                        try {

                                            date111 = myFormat.parse(inputString11);
                                            date12 = myFormat.parse(inputString1);
                                            job_dif = job_dif + getWorkingDaysBetweenTwoDates(date111, date12);

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }


                                    } while (cursor.moveToNext());
                                }


                                if(cursor1.moveToFirst()) {
                                    do {

                                        String s_d = cursor1.getString(cursor.getColumnIndexOrThrow("start_date"));
                                        String y = "";
                                        String m = "";
                                        String d1 = "";
                                        int i =0,jj=0;


                                        for(i = 0; i < s_d.length();i++){

                                            if(s_d.charAt(i) == '.'){
                                                break;
                                            }
                                            d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                        }
                                        for(jj = i+1; jj < s_d.length();jj++){

                                            if(s_d.charAt(jj) == '.'){
                                                break;
                                            }
                                            m=m.concat(String.valueOf(s_d.charAt(jj)));
                                        }
                                        for (int k = jj+1; k < s_d.length();k++){
                                            y = y.concat(String.valueOf(s_d.charAt(k)));
                                        }


                                        int s_year= Integer.valueOf(y);
                                        int s_month = Integer.valueOf(m);
                                        int s_dd = Integer.valueOf(d1);


                                        String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                        Date date111 = null;
                                        Date date12 = null;



                                        try {

                                            date111 = myFormat.parse(inputString11);
                                            date12 = myFormat.parse(inputString1);
                                            bill_dif = bill_dif + getWorkingDaysBetweenTwoDates(date111, date12);

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }


                                    } while (cursor1.moveToNext());

                                    if(cursor2.moveToFirst()) {
                                        do {

                                            String s_d = cursor2.getString(cursor.getColumnIndexOrThrow("start_date"));
                                            String y = "";
                                            String m = "";
                                            String d1 = "";
                                            int i = 0, jj = 0;


                                            for (i = 0; i < s_d.length(); i++) {

                                                if (s_d.charAt(i) == '.') {
                                                    break;
                                                }
                                                d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                            }
                                            for (jj = i + 1; jj < s_d.length(); jj++) {

                                                if (s_d.charAt(jj) == '.') {
                                                    break;
                                                }
                                                m = m.concat(String.valueOf(s_d.charAt(jj)));
                                            }
                                            for (int k = jj + 1; k < s_d.length(); k++) {
                                                y = y.concat(String.valueOf(s_d.charAt(k)));
                                            }


                                            int s_year = Integer.valueOf(y);
                                            int s_month = Integer.valueOf(m);
                                            int s_dd = Integer.valueOf(d1);


                                            String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                            Date date111 = null;
                                            Date date12 = null;


                                            try {

                                                date111 = myFormat.parse(inputString11);
                                                date12 = myFormat.parse(inputString1);
                                                asset_dif = asset_dif + getWorkingDaysBetweenTwoDates(date111, date12);

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                        } while (cursor2.moveToNext());
                                    }

                                        if(cursor3.moveToFirst()) {
                                            do {

                                                String s_d = cursor3.getString(cursor.getColumnIndexOrThrow("start_date"));
                                                String y = "";
                                                String m = "";
                                                String d1 = "";
                                                int i = 0, jj = 0;


                                                for (i = 0; i < s_d.length(); i++) {

                                                    if (s_d.charAt(i) == '.') {
                                                        break;
                                                    }
                                                    d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                                }
                                                for (jj = i + 1; jj < s_d.length(); jj++) {

                                                    if (s_d.charAt(jj) == '.') {
                                                        break;
                                                    }
                                                    m = m.concat(String.valueOf(s_d.charAt(jj)));
                                                }
                                                for (int k = jj + 1; k < s_d.length(); k++) {
                                                    y = y.concat(String.valueOf(s_d.charAt(k)));
                                                }


                                                int s_year = Integer.valueOf(y);
                                                int s_month = Integer.valueOf(m);
                                                int s_dd = Integer.valueOf(d1);


                                                String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                                Date date111 = null;
                                                Date date12 = null;


                                                try {

                                                    date111 = myFormat.parse(inputString11);
                                                    date12 = myFormat.parse(inputString1);
                                                    lib_dif = lib_dif + getWorkingDaysBetweenTwoDates(date111, date12);

                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }


                                            } while (cursor3.moveToNext());
                                        }
                                }

                                // reuse previous dialog instance
                                final Double[] total = {Double.valueOf(preference.getString("amount_jobs",""))};
                                double temp = Double.valueOf(preference.getString(job_name,""));
                                total[0] = total[0] -temp;


                                final salary_rate s = new salary_rate();

                                double rate_d = s.calulate_salary_rate_per_day(s_time,e_time,amount,s_type,off_day);

                                //Toast.makeText(getApplicationContext(), "current "+String.valueOf(rate_d), Toast.LENGTH_SHORT).show();

                                rate_d = Double.valueOf(preference.getString("net_job_rate",""))-rate_d;

                                if(rate_d <= 0.00001){
                                    rate_d = 0.0;
                                }

                                editor.putString("net_job_rate",String.valueOf(rate_d));
                                editor.commit();

                                //db_helper.update_rate_job(String.valueOf(rate_d));

                                rate_d = rate_d + Double.valueOf(preference.getString("net_asset_rate",""))
                                        - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));


                                if(rate_d <= 0.00001){
                                    rate_d = 0.0;
                                }


                                if(rate_d <= 0){
                                    editor.putString("goal_date","0");
                                    editor.commit();
                                }else {
                                    double goal = Double.valueOf(preference.getString("goal",""));

                                    double net = Double.valueOf(preference.getString("net",""));

                                    if(start_date_extract(s_date)){
                                        net = net - Double.valueOf(preference.getString(job_name,"")) ;
                                    }

                                    Toast.makeText(getApplicationContext(), "net  "+String.valueOf(net), Toast.LENGTH_SHORT).show();

                                    net = goal - net ;

                                    double days_count = net/rate_d;

                                    int dayss = (int) days_count;


                                    Date date1 = null;
                                    try {

                                        date1 = myFormat.parse(inputString1);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }


                                    Calendar startCal = Calendar.getInstance();
                                    startCal.setTime(date1);

                                    dayss = (int) (dayss + job_dif + asset_dif - bill_dif - lib_dif);
                                    int r = 0;
                                    do {
                                        startCal.add(Calendar.DAY_OF_MONTH, 1);
                                        r++;
                                    } while (r <= dayss); //excluding end date


                                    String dd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                            +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                            +String.valueOf(startCal.get(Calendar.YEAR));

                                    editor.putString("goal_date",dd);
                                    editor.commit();
                                    //db_helper.update_goal_date(dd);

                                }


                                editor.putString("amount_jobs",String.valueOf(total[0]));
                                editor.remove(jobName);
                                editor.remove(jobName+"_check");
                                editor.apply();
                                editor.putString("refresh","needed");
                                editor.commit();


                                sDialog.setTitleText("Deleted!")
                                        .setContentText("Your item has been deleted!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sDialog.dismiss();
                                                finish();
                                            }
                                        })
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                            }
                        })
                        .show();
            }
        });
    }
    public boolean start_date_extract(String s_date){

        Boolean bbb=false;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < s_date.length();i++){

            if(s_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(s_date.charAt(i)));
        }
        for(j = i+1; j < s_date.length();j++){

            if(s_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(s_date.charAt(j)));
        }
        for (int k = j+1; k < s_date.length();k++){
            year = year.concat(String.valueOf(s_date.charAt(k)));
        }


        int s_year= Integer.valueOf(year);
        int s_month = Integer.valueOf(month);
        int s_dd = Integer.valueOf(dd);

        s_month=s_month-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year){
            bbb=true;
        }
        else if(c_year == s_year){
            if(c_month > s_month){
                bbb=true;
            }
            else if(c_month == s_month){
                if(c_dd >= s_dd){
                    bbb=true;
                }
            }
        }
        return bbb;
    }

    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
            do {

                startCal.add(Calendar.DAY_OF_MONTH, 1);

                ++workDays;

            } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()); //excluding end date

            //return 0.0;
        }


        return workDays;
    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("restart","").equals("on")){
            editor.putString("restart","off");
            editor.commit();

            SQLiteDatabase db = db_helper.getReadableDatabase();

            jobName = preference.getString("job_name","");

            Cursor cursor = null;
            try {
                cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);

                if(cursor.moveToFirst()){
                    do{

                        if(cursor.getString(cursor.getColumnIndexOrThrow("job_name")).equals(preference.getString("job_name",""))){

                            job_name = cursor.getString(cursor.getColumnIndexOrThrow("job_name")).toString();
                            tv_d_name.setText(cursor.getString(cursor.getColumnIndexOrThrow("job_name")));
                            amount = cursor.getString(cursor.getColumnIndexOrThrow("salary_amount"));
                            s_type = cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));
                            s_time = cursor.getString(cursor.getColumnIndexOrThrow("start_time"));
                            e_time = cursor.getString(cursor.getColumnIndexOrThrow("end_time"));
                            s_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));
                            off_day = cursor.getString(cursor.getColumnIndexOrThrow("off_day"));


                            break;
                        }
                    }
                    while(cursor.moveToNext());
                }


            } finally {
                // this gets called even if there is an exception somewhere above
                if(cursor != null)
                    cursor.close();
            }


            salary_rate s = new salary_rate();

            rate = s.calulate_salary_rate(s_time,e_time,amount,s_type,off_day);

            String str_rate = String.format("%.5f",rate);

            //Toast.makeText(this,str_rate, Toast.LENGTH_SHORT).show();

            tv_rate.setText("$ "+str_rate);
            tv_wh.setText(s_time+" to "+e_time);
            tv_sd.setText(s_date);
            tv_salary.setText("$ "+amount+"/"+s_type);


            startService(new Intent(getApplicationContext(),watcher.class));

            cursor = null;
            try {
                cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
                if(cursor.moveToFirst()){
                    do{

                        job_name = cursor.getString(cursor.getColumnIndexOrThrow("job_name")).toString();
                        amount = cursor.getString(cursor.getColumnIndexOrThrow("salary_amount"));
                        s_type = cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));
                        s_time = cursor.getString(cursor.getColumnIndexOrThrow("start_time"));
                        e_time = cursor.getString(cursor.getColumnIndexOrThrow("end_time"));
                        s_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));
                        off_day = cursor.getString(cursor.getColumnIndexOrThrow("off_day"));
                        start_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));


                        /*handler h = new handler();

                        h.job_timer(getApplicationContext(),
                                job_name,
                                s_time,
                                e_time,
                                off_day,
                                amount,
                                s_type,
                                start_date
                        );*/

                    }
                    while(cursor.moveToNext());
                }

            } finally {
                // this gets called even if there is an exception somewhere above
                if(cursor != null)
                    cursor.close();
            }

            Cursor cursor1 = null;
            try {
                cursor1 = db.rawQuery("SELECT rowid _id,* FROM bills", null);
                if(cursor1.moveToFirst()){
                    do{

                        job_name = cursor1.getString(cursor1.getColumnIndexOrThrow("bills_name")).toString();
                        amount = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_amount"));
                        s_type = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_type"));
                        s_time = cursor1.getString(cursor1.getColumnIndexOrThrow("start_time"));
                        e_time = cursor1.getString(cursor1.getColumnIndexOrThrow("end_time"));
                        s_date = cursor1.getString(cursor1.getColumnIndexOrThrow("start_date"));
                        off_day = cursor1.getString(cursor1.getColumnIndexOrThrow("off_day"));
                        start_date = cursor1.getString(cursor1.getColumnIndexOrThrow("start_date"));


                        /*bills_handler h = new bills_handler();

                        h.job_timer(getApplicationContext(),
                                job_name,
                                s_time,
                                e_time,
                                off_day,
                                amount,
                                s_type,
                                start_date
                        );*/

                    }
                    while(cursor1.moveToNext());
                }

            } finally {
                // this gets called even if there is an exception somewhere above
                if(cursor1 != null)
                    cursor1.close();
            }


        }

        int count = 0;


        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv_total_sal.setText("$ "+preference.getString(jobName,""));
                                //Toast.makeText(display_jobs.this, preference.getString(jobName,"")+" "+jobName, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
        /*final Handler handler= new Handler();
        final Timer timer = new Timer();

        final TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {


                    }
                });
            }
        };
        timer.schedule(timerTask, 0,1000);*/
    }

    @Override
    public void onBackPressed() {

        if(materialSheetFab_job.isSheetVisible()){

            materialSheetFab_job.hideSheet();
        }
        else{
            super.onBackPressed();
        }
    }
}
