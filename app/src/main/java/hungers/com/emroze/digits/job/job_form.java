package hungers.com.emroze.digits.job;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import hungers.com.emroze.digits.R;
import hungers.com.emroze.digits.database.DatabaseHelper;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.Calendar;

import pl.droidsonroids.gif.GifTextView;

public class job_form extends AppCompatActivity {

    public static String PREFS_NAME="digits";

    TextView tv_job,tv_salary,tv_hour,tv_date,tv_per_hour,tv_stime,tv_etime,tv_sdate,tv_per_week,tv_per_month,tv_per_year,tv_per_hour1;

    TextView tv_offday,tv_sun,tv_mon,tv_tue,tv_wed,tv_thu,tv_fri,tv_sat,tv_s,jan,feb,mar,apr,may,june,july,aug,sep,oct,nov,dec;

    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout1;
    private ExpandableLayout expandableLayout2;
    private ExpandableLayout expandableLayout3;
    private ExpandableLayout expandableLayout4;


    TimePicker t1;
    TimePicker t2;
    DatePicker d1;

    GifTextView btn_gif,off_day_gif;

    int hour_s, hour_e;
    int min_s, min_e;
    String AM_PM_s, AM_PM_e ;
    String db_stime = "",db_etime = "";
    String db_start_date = "",db_per_rate = "hour";
    String off_day = "0";

    Button save;

    Button pick_stime, pick_etime, pick_sdate;

    EditText et_job,et_salary;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    int i_sun,i_mon,i_tue,i_wed,i_thu,i_fri,i_sat = 0;

    int day ;
    int month;
    int year ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_form);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();


        tv_job = (TextView) findViewById(R.id.tv_job_name);
        tv_salary = (TextView) findViewById(R.id.tv_salary);
        tv_hour = (TextView) findViewById(R.id.tv_hour);
        tv_date = (TextView) findViewById(R.id.tv_start_date);
        tv_per_hour = (TextView) findViewById(R.id.tv_per_hour);
        tv_per_hour1 = (TextView) findViewById(R.id.tv_per_hour1);
        tv_per_week = (TextView) findViewById(R.id.tv_per_week);
        tv_per_month = (TextView) findViewById(R.id.tv_per_month);
        tv_per_year = (TextView) findViewById(R.id.tv_per_year);
        tv_stime = (TextView) findViewById(R.id.tv_stime);
        tv_etime = (TextView) findViewById(R.id.tv_etime);
        tv_sdate = (TextView) findViewById(R.id.tv_start_date_pick);


        tv_offday = (TextView) findViewById(R.id.tv_offday);
        tv_sun = (TextView) findViewById(R.id.sun);
        tv_mon = (TextView) findViewById(R.id.mon);
        tv_tue = (TextView) findViewById(R.id.tue);
        tv_wed = (TextView) findViewById(R.id.wed);
        tv_thu = (TextView) findViewById(R.id.thu);
        tv_fri = (TextView) findViewById(R.id.fri);
        tv_sat = (TextView) findViewById(R.id.sat);
        //tv_s = (TextView) findViewById(R.id.tv_short);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        tv_job.setTypeface(type);
        tv_salary.setTypeface(type);
        tv_hour.setTypeface(type);
        tv_date.setTypeface(type);


        off_day_simulate();

        // for callopsing bar
        btn_gif = (GifTextView) findViewById(R.id.btn_gif1);


        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);

        btn_gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_per_hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });


        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);

        tv_stime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        expandableLayout3 = (ExpandableLayout) findViewById(R.id.expandable_layout_3);

        tv_etime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout3.isExpanded()) {
                    expandableLayout3.collapse();
                }
                else {
                    expandableLayout3.expand();
                }
            }
        });

        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);

        tv_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });

//////////////////////////////////////////////

        //pick pre hour from callopsing bar


        tv_per_hour1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per hour");
                expandableLayout0.collapse();
                db_per_rate = "hour";
            }
        });
        tv_per_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per week");
                expandableLayout0.collapse();
                db_per_rate = "week";
            }
        });
        tv_per_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per month");
                expandableLayout0.collapse();
                db_per_rate = "month";
            }
        });
        tv_per_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per year");
                expandableLayout0.collapse();
                db_per_rate = "year";
            }
        });


        //time picker

        t1 = (TimePicker) findViewById(R.id.timePicker1);
        t2 = (TimePicker) findViewById(R.id.timePicker2);

        t1.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hour_s = hourOfDay;
                min_s = minute;

                /*if(hourOfDay < 12) {
                    AM_PM_s = "AM";
                } else {
                    AM_PM_s = "PM";
                }*/
            }
        });


        t2.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hour_e = hourOfDay;
                min_e = minute;

                /*if(hourOfDay < 12 ) {
                    AM_PM_e = "AM";
                } else {
                    AM_PM_e = "PM";
                }
*/
            }
        });

        pick_stime = (Button) findViewById(R.id.btn_pick_stime);
        pick_etime = (Button) findViewById(R.id.btn_pick_etime);


        pick_stime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pick_stime.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        pick_stime.getBackground().setAlpha(255);

                        double min1_s= min_s/60.0;

                        double hour1_s = hour_s + min1_s;

                        expandableLayout1.collapse();

                        String display_s_time = String.valueOf(hour_s) + " : "+String.valueOf(min_s);

                        if(hour_s == 0 && min_s == 0){
                            tv_stime.setText(display_s_time);
                            db_stime = "";
                            Toast.makeText(job_form.this, "Wrong Input", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            db_stime = String.valueOf(hour_s) + ":"+String.valueOf(min_s);
                            tv_stime.setText(display_s_time);
                        }


                        break;
                }
                return false;
            }
        });

        pick_etime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pick_etime.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        pick_etime.getBackground().setAlpha(255);

                        double min1_e= min_s/60.0;

                        double hour1_e = hour_e + min1_e;

                        expandableLayout3.collapse();

                        String display_e_time = String.valueOf(hour_e) + " : "+String.valueOf(min_e);

                        if(hour_e == 0 && min_e == 0){
                            tv_etime.setText(display_e_time);
                            db_etime="";
                            Toast.makeText(job_form.this, "Wrong Input", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            db_etime = String.valueOf(hour_e) + ":"+String.valueOf(min_e);
                            tv_etime.setText(display_e_time);
                        }


                        break;
                }
                return false;
            }
        });

        //pick date
        pick_sdate = (Button) findViewById(R.id.btn_pick_date);
        d1 = (DatePicker) findViewById(R.id.datePicker);


        pick_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                day = d1.getDayOfMonth();
                month = d1.getMonth() + 1;
                year = d1.getYear();

                String m ="";

                if(month == 1){
                    m = "Jan";
                }
                else if(month == 2){
                    m = "Feb";
                }
                else if(month == 3){
                    m = "Mar";
                }
                else if(month == 4){
                    m = "Apr";
                }
                else if(month == 5){
                    m = "May";
                }
                else if(month == 6){
                    m = "June";
                }
                else if(month == 7){
                    m = "July";
                }
                else if(month == 8){
                    m = "Aug";
                }
                else if(month == 9){
                    m = "Sep";
                }
                else if(month == 10){
                    m = "Oct";
                }
                else if(month == 11){
                    m = "Nov";
                }
                else if(month == 12){
                    m = "Dec";
                }

                final String display_start_date = String.valueOf(day) + " "+m +", "+String.valueOf(year);

                db_start_date = String.valueOf(day) + "." + String.valueOf(month) +"."+String.valueOf(year) ;

                tv_sdate.setText(display_start_date);
                expandableLayout2.collapse();

            }
        });

        // save into the database

        et_job = (EditText) findViewById(R.id.et_job_name);
        et_salary = (EditText) findViewById(R.id.et_salary);


        save = (Button) findViewById(R.id.btn_save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(et_job.getText().toString().equals("") || et_salary.getText().toString().equals("") ||
                        db_per_rate == "" || db_stime == "" || db_etime == "" || db_start_date == ""){

                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);

                    Toast.makeText(job_form.this, "Totally fill the form", Toast.LENGTH_SHORT).show();
                }
                else{
                    //insert

                    final salary_rate s = new salary_rate();

                    int last_hour = s.hour(db_etime);

                    int last_min = s.min(db_etime);

                    int start_hour = s.hour(db_stime);

                    int start_min = s.min(db_stime);

                    Double s_h = (Double) (start_hour + (start_min/60.00));
                    Double l_h = (Double) (last_hour + (last_min/60.00));

                    if(s_h == l_h){
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(500);
                        Toast.makeText(job_form.this, "Start time and End time can't be matched", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        off_day_calculation();

                        job_item job = new job_item();
                        job.setJob_name(et_job.getText().toString());
                        job.setSalary_amount(et_salary.getText().toString());
                        job.setSalary_type(db_per_rate);
                        job.setStart_time(db_stime);
                        job.setEnd_time(db_etime);
                        job.setStart_date(db_start_date);
                        job.setOff_day(off_day);


                            db_helper.insertJobs(job);
                            int i = preference.getInt("job_count",0) + 1;

                            editor.putString(et_job.getText().toString(),"0");
                            editor.putString("refresh","needed");
                            editor.putInt("job_count",i);
                            editor.putInt("s_hour",start_hour);
                            editor.putInt("s_min",start_min);
                            editor.putInt("s_date",day);
                            editor.putInt("s_month",month);
                            editor.putInt("s_year",year);
                            editor.commit();

                            Calendar c = Calendar.getInstance();

                            int hour = c.get(Calendar.HOUR_OF_DAY);
                            int min = c.get(Calendar.MINUTE);
                            int sec = c.get(Calendar.SECOND);
                            int date = c.get(Calendar.DAY_OF_MONTH);
                            int month = c.get(Calendar.MONTH);
                            int year = c.get(Calendar.YEAR);

                            pre_date_job_lack l = new pre_date_job_lack();

                            Double lack = l.calc(getApplicationContext(),db_stime,db_etime,sec,min,hour,date,month,year,off_day);

                            Double rate = s.calulate_salary_rate(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,off_day);

                            Toast.makeText(job_form.this, String.valueOf(lack), Toast.LENGTH_SHORT).show();

                            lack = lack*rate;

                            editor.putString(et_job.getText().toString(),String.valueOf(lack));
                            editor.commit();
                            db_helper.update_jobs_net_amount(et_job.getText().toString(),String.valueOf(lack));

                            //Toast.makeText(getApplicationContext(), String.valueOf(lack), Toast.LENGTH_SHORT).show();

                           /* handler h = new handler();

                            h.job_timer(getApplicationContext(),
                                    et_job.getText().toString(),
                                    db_stime,
                                    db_etime,
                                    off_day,
                                    et_salary.getText().toString(),
                                    db_per_rate,
                                    db_start_date
                            );*/


                        finish();
                    }

                }
            }
        });




    }

    public void off_day_calculation(){
        if(i_sun == 1){
            off_day = "1";
        }
        else {
            off_day = "";
        }
        if(i_mon == 1){
            off_day = off_day.concat("2");
        }
        if(i_tue == 1){
            off_day = off_day.concat("3");
        }
        if(i_wed == 1){
            off_day = off_day.concat("4");
        }
        if(i_thu == 1){
            off_day = off_day.concat("5");
        }
        if(i_fri == 1){
            off_day = off_day.concat("6");
        }
        if(i_sat == 1){
            off_day = off_day.concat("7");
        }

        if(i_sun == 0 && i_mon == 0 && i_tue == 0 && i_wed == 0 && i_fri == 0 && i_sat == 0){
            off_day = "0";
        }

    }
    public void off_day_simulate(){

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_sun == 0){

                    tv_sun.setBackgroundResource(R.drawable.border);
                    tv_sun.setTextColor(Color.rgb(0,0,0));
                    i_sun=1;
                }
                else{

                    tv_sun.setBackgroundResource(R.drawable.border4);
                    tv_sun.setTextColor(Color.rgb(255,255,255));
                    i_sun=0;
                }
            }
        });
        tv_mon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_mon == 0){

                    tv_mon.setBackgroundResource(R.drawable.border);
                    tv_mon.setTextColor(Color.rgb(0,0,0));
                    i_mon=1;
                }
                else{

                    tv_mon.setBackgroundResource(R.drawable.border4);
                    tv_mon.setTextColor(Color.rgb(255,255,255));
                    i_mon=0;
                }
            }
        });
        tv_tue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_tue == 0){

                    tv_tue.setBackgroundResource(R.drawable.border);
                    tv_tue.setTextColor(Color.rgb(0,0,0));
                    i_tue=1;
                }
                else{

                    tv_tue.setBackgroundResource(R.drawable.border4);
                    tv_tue.setTextColor(Color.rgb(255,255,255));
                    i_tue=0;
                }
            }
        });
        tv_wed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_wed == 0){
                    tv_wed.setBackgroundResource(R.drawable.border);
                    tv_wed.setTextColor(Color.rgb(0,0,0));
                    i_wed=1;
                }
                else{
                    tv_wed.setBackgroundResource(R.drawable.border4);
                    tv_wed.setTextColor(Color.rgb(255,255,255));
                    i_wed=0;
                }

            }
        });
        tv_thu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_thu == 0){

                    tv_thu.setBackgroundResource(R.drawable.border);
                    tv_thu.setTextColor(Color.rgb(0,0,0));
                    i_thu=1;
                }
                else{

                    tv_thu.setBackgroundResource(R.drawable.border4);
                    tv_thu.setTextColor(Color.rgb(255,255,255));
                    i_thu=0;
                }

            }
        });
        tv_fri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_fri == 0){

                    tv_fri.setBackgroundResource(R.drawable.border);
                    tv_fri.setTextColor(Color.rgb(0,0,0));
                    i_fri=1;
                }
                else{

                    tv_fri.setBackgroundResource(R.drawable.border4);
                    tv_fri.setTextColor(Color.rgb(255,255,255));
                    i_fri=0;
                }
            }
        });
        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_sat == 0){

                    tv_sat.setBackgroundResource(R.drawable.border);
                    tv_sat.setTextColor(Color.rgb(0,0,0));
                    i_sat=1;
                }
                else{

                    tv_sat.setBackgroundResource(R.drawable.border4);
                    tv_sat.setTextColor(Color.rgb(255,255,255));
                    i_sat=0;
                }
            }
        });
    }
}