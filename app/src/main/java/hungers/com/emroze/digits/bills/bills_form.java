package hungers.com.emroze.digits.bills;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import hungers.com.emroze.digits.database.DatabaseHelper;
import hungers.com.emroze.digits.job.pre_date_job_lack;
import hungers.com.emroze.digits.job.salary_rate;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class bills_form extends AppCompatActivity {
    public static String PREFS_NAME="digits";

    TextView tv_job,tv_salary,tv_hour,tv_date,tv_per_hour,tv_stime,tv_etime,tv_sdate,tv_per_week,tv_per_month,tv_per_year,tv_per_hour1,tv_optional;

    TextView tv_offday,tv_sun,tv_mon,tv_tue,tv_wed,tv_thu,tv_fri,tv_sat,tv_s,jan,feb,mar,apr,may,june,july,aug,sep,oct,nov,dec;

    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout1;
    private ExpandableLayout expandableLayout2;
    private ExpandableLayout expandableLayout3;
    private ExpandableLayout expandableLayout4;

    double pre_rate = 0;

    TimePicker t1;
    TimePicker t2;
    DatePicker d1;

    GifTextView btn_gif,off_day_gif;


    int hour_s, hour_e;
    int min_s, min_e;
    String AM_PM_s, AM_PM_e ;
    String db_stime = "",db_etime = "";
    String db_start_date = "",db_per_rate = "hour";
    String off_day = "0";

    Button save;

    Button pick_stime, pick_etime, pick_sdate;

    EditText et_job,et_salary;

    DatabaseHelper db_helper = new DatabaseHelper(this);

    int i_sun = 0,i_mon = 0,i_tue = 0,i_wed = 0,i_thu = 0,i_fri = 0,i_sat = 0;

    int day ;
    int month;
    int year ;

    String display_s_time,display_e_time;

    double pre_net = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(hungers.com.emroze.digits.R.layout.activity_bills_form);

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        display_s_time = "0 : 1";
        display_e_time = "23 : 59";
        db_stime="0:1";
        db_etime="23:59";

        tv_job = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_job_name);
        tv_salary = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_salary);
        tv_hour = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_hour);
        tv_date = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_start_date);
        tv_per_hour = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_hour);
        tv_per_hour1 = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_hour1);
        tv_per_week = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_week);
        tv_per_month = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_month);
        tv_per_year = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_year);
        tv_stime = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_stime);
        tv_etime = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_etime);
        tv_sdate = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_start_date_pick);
        tv_optional = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_optional);


        tv_offday = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_offday);
        tv_sun = (TextView) findViewById(hungers.com.emroze.digits.R.id.sun);
        tv_mon = (TextView) findViewById(hungers.com.emroze.digits.R.id.mon);
        tv_tue = (TextView) findViewById(hungers.com.emroze.digits.R.id.tue);
        tv_wed = (TextView) findViewById(hungers.com.emroze.digits.R.id.wed);
        tv_thu = (TextView) findViewById(hungers.com.emroze.digits.R.id.thu);
        tv_fri = (TextView) findViewById(hungers.com.emroze.digits.R.id.fri);
        tv_sat = (TextView) findViewById(hungers.com.emroze.digits.R.id.sat);
        //tv_s = (TextView) findViewById(R.id.tv_short);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");

        tv_job.setTypeface(type);
        tv_salary.setTypeface(type);
        tv_hour.setTypeface(type);
        tv_date.setTypeface(type);

        tv_stime.setText(display_s_time);
        tv_etime.setText(display_e_time);

        off_day_simulate();

        // for callopsing bar
        btn_gif = (GifTextView) findViewById(hungers.com.emroze.digits.R.id.btn_gif1);


        expandableLayout0 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_0);

        btn_gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_per_hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });


        expandableLayout1 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_1);

        tv_stime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout1.isExpanded()) {
                    expandableLayout1.collapse();
                }
                else {
                    expandableLayout1.expand();
                }
            }
        });

        expandableLayout3 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_3);

        tv_etime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout3.isExpanded()) {
                    expandableLayout3.collapse();
                }
                else {
                    expandableLayout3.expand();
                }
            }
        });

        expandableLayout2 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_2);

        tv_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });

        expandableLayout4 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout);

        tv_optional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout4.isExpanded()) {
                    expandableLayout4.collapse();
                }
                else {
                    expandableLayout4.expand();
                }
            }
        });
//////////////////////////////////////////////

        //pick pre hour from callopsing bar


        tv_per_hour1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per hour");
                expandableLayout0.collapse();
                db_per_rate = "hour";
            }
        });
        tv_per_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per week");
                expandableLayout0.collapse();
                db_per_rate = "week";
            }
        });
        tv_per_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per month");
                expandableLayout0.collapse();
                db_per_rate = "month";
            }
        });
        tv_per_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per year");
                expandableLayout0.collapse();
                db_per_rate = "year";
            }
        });


        //time picker

        t1 = (TimePicker) findViewById(hungers.com.emroze.digits.R.id.timePicker1);
        t2 = (TimePicker) findViewById(hungers.com.emroze.digits.R.id.timePicker2);

        t1.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hour_s = hourOfDay;
                min_s = minute;

                /*if(hourOfDay < 12) {
                    AM_PM_s = "AM";
                } else {
                    AM_PM_s = "PM";
                }*/
            }
        });


        t2.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                hour_e = hourOfDay;
                min_e = minute;

                /*if(hourOfDay < 12 ) {
                    AM_PM_e = "AM";
                } else {
                    AM_PM_e = "PM";
                }
*/
            }
        });

        pick_stime = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_pick_stime);
        pick_etime = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_pick_etime);


        pick_stime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pick_stime.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        pick_stime.getBackground().setAlpha(255);

                        double min1_s= min_s/60.0;

                        double hour1_s = hour_s + min1_s;

                        expandableLayout1.collapse();

                        display_s_time = String.valueOf(hour_s) + " : "+String.valueOf(min_s);

                        if(hour_s == 0 && min_s == 0){
                            tv_stime.setText(display_s_time);
                            db_stime = "";
                            Toast.makeText(bills_form.this, "Wrong Input", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            db_stime = String.valueOf(hour_s) + ":"+String.valueOf(min_s);
                            tv_stime.setText(display_s_time);
                        }


                        break;
                }
                return false;
            }
        });

        pick_etime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pick_etime.getBackground().setAlpha(100);
                        break;
                    case MotionEvent.ACTION_UP:
                        pick_etime.getBackground().setAlpha(255);

                        double min1_e= min_s/60.0;

                        double hour1_e = hour_e + min1_e;

                        expandableLayout3.collapse();

                        display_e_time = String.valueOf(hour_e) + " : "+String.valueOf(min_e);

                        if(hour_e == 0 && min_e == 0){
                            tv_etime.setText(display_e_time);
                            db_etime="";
                            Toast.makeText(bills_form.this, "Wrong Input", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            db_etime = String.valueOf(hour_e) + ":"+String.valueOf(min_e);
                            tv_etime.setText(display_e_time);
                        }


                        break;
                }
                return false;
            }
        });

        //pick date
        pick_sdate = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_pick_date);
        d1 = (DatePicker) findViewById(hungers.com.emroze.digits.R.id.datePicker);


        pick_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                day = d1.getDayOfMonth();
                month = d1.getMonth() + 1;
                year = d1.getYear();

                String m ="";

                if(month == 1){
                    m = "Jan";
                }
                else if(month == 2){
                    m = "Feb";
                }
                else if(month == 3){
                    m = "Mar";
                }
                else if(month == 4){
                    m = "Apr";
                }
                else if(month == 5){
                    m = "May";
                }
                else if(month == 6){
                    m = "June";
                }
                else if(month == 7){
                    m = "July";
                }
                else if(month == 8){
                    m = "Aug";
                }
                else if(month == 9){
                    m = "Sep";
                }
                else if(month == 10){
                    m = "Oct";
                }
                else if(month == 11){
                    m = "Nov";
                }
                else if(month == 12){
                    m = "Dec";
                }

                final String display_start_date = String.valueOf(day) + " "+m +", "+String.valueOf(year);

                db_start_date = String.valueOf(day) + "." + String.valueOf(month) +"."+String.valueOf(year) ;

                tv_sdate.setText(display_start_date);
                expandableLayout2.collapse();

            }
        });

        // save into the database

        et_job = (EditText) findViewById(hungers.com.emroze.digits.R.id.et_job_name);
        et_salary = (EditText) findViewById(hungers.com.emroze.digits.R.id.et_salary);


        save = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(et_job.getText().toString().equals("") || et_salary.getText().toString().equals("") ||
                        db_per_rate == "" || db_stime == "" || db_etime == "" || db_start_date == ""){

                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);

                    Toast.makeText(bills_form.this, "Totally fill the form", Toast.LENGTH_SHORT).show();
                }
                else{
                    //insert

                    final salary_rate s = new salary_rate();

                    int last_hour = s.hour(db_etime);

                    int last_min = s.min(db_etime);

                    int start_hour = s.hour(db_stime);

                    int start_min = s.min(db_stime);

                    Double s_h = (Double) (start_hour + (start_min/60.00));
                    Double l_h = (Double) (last_hour + (last_min/60.00));

                    if(s_h == l_h){
                        Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(500);
                        Toast.makeText(bills_form.this, "Start time and End time can't be matched", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        off_day_calculation();

                        if(off_day.length() == 7){
                            Toast.makeText(bills_form.this, "7 days can't be off day at a time", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            bills_item_details job = new bills_item_details();
                            job.setBills_name(et_job.getText().toString());
                            job.setBills_amount(et_salary.getText().toString());
                            job.setBills_type(db_per_rate);
                            job.setStart_time(db_stime);
                            job.setEnd_time(db_etime);
                            job.setStart_date(db_start_date);
                            job.setOff_day(off_day);

                            if(preference.getString("update_bills","").equals("on")){


                                salary_rate ss = new salary_rate();
                                int st_hour = ss.hour(db_stime);
                                int st_min = ss.min(db_stime);

                                editor.putString("update_bills","off");
                                editor.putString("refresh_bills","needed");
                                editor.putInt("s_hour",st_hour);
                                editor.putInt("s_min",st_min);
                                editor.putInt("s_date",d_day(db_start_date));
                                editor.putInt("s_month",d_month(db_start_date));
                                editor.putInt("s_year",d_year(db_start_date));
                                editor.commit();

                                String jobName = preference.getString("bills_name","");
                                db_helper.update_bills_item(job,jobName);

                                editor.remove("bill_"+jobName);
                                editor.apply();

                                double rate11 = 0;
                                rate11 = Double.valueOf(preference.getString("net_bill_rate",""))-pre_rate;


                                /*final double rate_d1 = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                                        - Double.valueOf(preference.getString("net_lib_rate",""))- rate11;
*/
                                Calendar c = Calendar.getInstance();

                                int hour = c.get(Calendar.HOUR_OF_DAY);
                                int min = c.get(Calendar.MINUTE);
                                int sec = c.get(Calendar.SECOND);
                                int date1 = c.get(Calendar.DAY_OF_MONTH);
                                int month1 = c.get(Calendar.MONTH);
                                int year1 = c.get(Calendar.YEAR);

                                pre_date_job_lack l = new pre_date_job_lack();

                                Double lack = l.calc(getApplicationContext(),db_stime,db_etime,sec,min,hour,date1,month,year,off_day);

                                Double rate = s.calulate_salary_rate(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,off_day);

                                lack = lack*rate;

                                double rate_d = s.calulate_salary_rate_per_day(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,off_day);

                                rate_d = rate11+rate_d;

                                editor.putString("net_bill_rate",String.valueOf(rate_d));
                                editor.commit();

                                //db_helper.update_rate_bill(String.valueOf(rate_d));


                                rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                                        - Double.valueOf(preference.getString("net_lib_rate",""))- rate_d;



                                double goal = Double.valueOf(preference.getString("goal",""));

                                double net = Double.valueOf(preference.getString("net","")) - pre_net;

                                net = goal + net - lack;

                                double days_count = net/rate_d;

                                int dayss = (int) days_count;

                                Calendar c1 = Calendar.getInstance();

                                int date11 = c1.get(Calendar.DAY_OF_MONTH);
                                int month11 = c1.get(Calendar.MONTH);
                                int year11 = c1.get(Calendar.YEAR);


                                SimpleDateFormat myFormat1 = new SimpleDateFormat("dd MM yyyy");
                                String inputString11 = String.valueOf(day) + " " + String.valueOf(month) + " " + String.valueOf(year); //start date
                                String inputString2 = String.valueOf(date11) + " " + String.valueOf(month11 + 1) + " " + String.valueOf(year11); //present date

                                Date date111 = null;
                                Date date12 = null;

                                double dif = 0;

                                try {

                                    date111 = myFormat1.parse(inputString11);
                                    date12 = myFormat1.parse(inputString2);
                                    dif = getWorkingDaysBetweenTwoDates(date111, date12);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Calendar startc = Calendar.getInstance();
                                startc.setTime(date111);

                                Calendar endc = Calendar.getInstance();
                                endc.setTime(date12);

                                if (startc.getTimeInMillis() > endc.getTimeInMillis()) {

                                    dayss = dayss - (int)dif;
                                }

                                SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                                String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date

                                Date date13 = null;
                                try {
                                    date13 = myFormat.parse(inputString1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Calendar startCal = Calendar.getInstance();
                                startCal.setTime(date111);


                                //if(startCal.getTimeInMillis() <= endCal.getTimeInMillis())

                                //dayss = dayss+ (int)dif;

                                //Toast.makeText(getApplicationContext(), "dayys "+String.valueOf(dayss), Toast.LENGTH_SHORT).show();

                                int r = 0;
                                do {
                                    startCal.add(Calendar.DAY_OF_MONTH, 1);
                                    r++;
                                } while (r < dayss); //excluding end date


                                String ddd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                        +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                        +String.valueOf(startCal.get(Calendar.YEAR));

                                editor.putString("goal_date",ddd);
                                editor.commit();
                                //db_helper.update_goal_date(ddd);



                                editor.putString("bill_"+et_job.getText().toString(),String.valueOf(lack));
                                editor.putString("bills_name",et_job.getText().toString());
                                editor.putString("resume_main","on");
                                editor.putString("add","true");
                                editor.putString("resume_main_bill","on");
                                editor.putString(et_job.getText().toString()+"_check_bill","true");
                                editor.putString("resume_home","on");
                                editor.commit();
                                db_helper.update_bills_net_amount(et_job.getText().toString(),String.valueOf(lack));
/*
                                bills_handler h = new bills_handler();

                                h.job_timer(getApplicationContext(),
                                        et_job.getText().toString(),
                                        db_stime,
                                        db_etime,
                                        off_day,
                                        et_salary.getText().toString(),
                                        db_per_rate,
                                        db_start_date
                                );
                                bill_goal_date_handler hhh = new bill_goal_date_handler();

                                hhh.bill_goal_timer(getApplicationContext(),
                                        et_job.getText().toString(),
                                        db_stime,
                                        db_etime,
                                        off_day,
                                        et_salary.getText().toString(),
                                        db_per_rate,
                                        db_start_date,
                                        rate_d1);*/
                            }
                            else{
                                db_helper.insertBills(job);

                                editor.putString("bill_"+et_job.getText().toString(),"0");
                                editor.putString("refresh_bills","needed");
                                editor.putInt("s_hour",start_hour);
                                editor.putInt("s_min",start_min);
                                editor.putInt("s_date",day);
                                editor.putInt("s_month",month);
                                editor.putInt("s_year",year);

                                editor.putString("import","true");
                                editor.commit();


                                Calendar c = Calendar.getInstance();

                                int hour = c.get(Calendar.HOUR_OF_DAY);
                                int min = c.get(Calendar.MINUTE);
                                int sec = c.get(Calendar.SECOND);
                                int date1 = c.get(Calendar.DAY_OF_MONTH);
                                int month1 = c.get(Calendar.MONTH);
                                int year1 = c.get(Calendar.YEAR);

                                pre_date_job_lack l = new pre_date_job_lack();

                                Double lack = l.calc(getApplicationContext(),db_stime,db_etime,sec,min,hour,date1,month1,year1,off_day);

                                Double rate = s.calulate_salary_rate(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,off_day);
                                double rate_d = s.calulate_salary_rate_per_day(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,off_day);

                                rate_d = Double.valueOf(preference.getString("net_bill_rate",""))+rate_d;

                                editor.putString("net_bill_rate",String.valueOf(rate_d));
                                editor.commit();

                                //db_helper.update_rate_bill(String.valueOf(rate_d));


                                lack = lack*rate;


                                rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                                        - rate_d- Double.valueOf(preference.getString("net_lib_rate",""));

                                //Toast.makeText(job_form1.this, "rate d "+String.valueOf(rate_d), Toast.LENGTH_SHORT).show();

                                double goal = Double.valueOf(preference.getString("goal",""));

                                double net = Double.valueOf(preference.getString("net",""));

                                net = goal - net + lack;

                                double days_count = net/rate_d;

                                int dayss = (int) days_count;

                                //Toast.makeText(getApplicationContext(), "dayys "+String.valueOf(dayss), Toast.LENGTH_SHORT).show();

                                Calendar c1 = Calendar.getInstance();

                                int date11 = c1.get(Calendar.DAY_OF_MONTH);
                                int month11 = c1.get(Calendar.MONTH);
                                int year11 = c1.get(Calendar.YEAR);



                                SimpleDateFormat myFormat1 = new SimpleDateFormat("dd MM yyyy");
                                String inputString11 = String.valueOf(day) + " " + String.valueOf(month) + " " + String.valueOf(year); //start date
                                String inputString2 = String.valueOf(date11) + " " + String.valueOf(month11 + 1) + " " + String.valueOf(year11); //present date

                                Date date111 = null;
                                Date date12 = null;

                                double dif = 0;

                                try {

                                    date111 = myFormat1.parse(inputString11);
                                    date12 = myFormat1.parse(inputString2);
                                    dif = getWorkingDaysBetweenTwoDates(date111, date12);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }



                                Calendar startc = Calendar.getInstance();
                                startc.setTime(date111);


                                Calendar endc = Calendar.getInstance();
                                endc.setTime(date12);


                                if (startc.getTimeInMillis() > endc.getTimeInMillis()) {

                                    dayss = dayss - (int)dif;
                                }


                                //Toast.makeText(job_form1.this, "diff day "+String.valueOf(dif), Toast.LENGTH_SHORT).show();

                                SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                                String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date

                                Date date13 = null;
                                try {

                                    date13 = myFormat.parse(inputString1);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Calendar startCal = Calendar.getInstance();
                                startCal.setTime(date13);


                                //if(startCal.getTimeInMillis() <= endCal.getTimeInMillis())

                                //dayss = dayss+ (int)dif;

                                //Toast.makeText(job_form1.this, "dayss  "+String.valueOf(dayss), Toast.LENGTH_SHORT).show();

                                int r = 0;
                                do {
                                    startCal.add(Calendar.DAY_OF_MONTH, 1);
                                    r++;
                                } while (r <= dayss); //excluding end date


                                String ddd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                        +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                        +String.valueOf(startCal.get(Calendar.YEAR));

                                editor.putString("goal_date",ddd);
                                editor.commit();
                                //db_helper.update_goal_date(ddd);

                                //Toast.makeText(bills_form.this, off_day, Toast.LENGTH_SHORT).show();
                                editor.putString("bill_"+et_job.getText().toString(),String.valueOf(lack));
                                editor.putString(et_job.getText().toString()+"_check_bill","true");
                                editor.putString("add","true");
                                editor.putString("main_thread","on");
                                editor.commit();
                                db_helper.update_bills_net_amount(et_job.getText().toString(),String.valueOf(lack));

                                /*bills_handler h = new bills_handler();

                                h.job_timer(getApplicationContext(),
                                        et_job.getText().toString(),
                                        db_stime,
                                        db_etime,
                                        off_day,
                                        et_salary.getText().toString(),
                                        db_per_rate,
                                        db_start_date
                                );

                                bill_goal_date_handler hhh = new bill_goal_date_handler();

                                hhh.bill_goal_timer(getApplicationContext(),
                                        et_job.getText().toString(),
                                        db_stime,
                                        db_etime,
                                        off_day,
                                        et_salary.getText().toString(),
                                        db_per_rate,
                                        db_start_date,
                                        rate_d1);*/

                            }
                        }




                    /*Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("insert", true);
                    startActivity(intent);*/



                    /*watcher w = new watcher();
                    w.onStartCommand(getIntent(),0,1);*/




                        //startService(new Intent(getApplicationContext(),watcher.class));

                        /*alarm a = new alarm();
                        a.setAlarm(getApplicationContext(),min_s,hour_s,day,month-1,i);*/

                        //startActivity(new Intent(getApplicationContext(),jobs_list.class));
                       finish();
                    }

                }
            }
        });


        if(preference.getString("edit_bills","").equals("on")){


            editor.putString("update_bills","on");
            editor.putString("edit_bills","off");
            editor.commit();

            save.setText("Update");
            SQLiteDatabase db = db_helper.getReadableDatabase();

            Cursor cursor = null;
            try {
                cursor = db.rawQuery("SELECT rowid _id,* FROM bills", null);
                if(cursor.moveToFirst()){
                    do{

                        if(cursor.getString(cursor.getColumnIndexOrThrow("bills_name")).equals(preference.getString("bills_name",""))){

                            et_job.setText(cursor.getString(cursor.getColumnIndexOrThrow("bills_name")));
                            et_salary.setText(cursor.getString(cursor.getColumnIndexOrThrow("salary_amount")));
                            tv_per_hour.setText(cursor.getString(cursor.getColumnIndexOrThrow("salary_type")));
                            tv_stime.setText(cursor.getString(cursor.getColumnIndexOrThrow("start_time")));
                            tv_etime.setText(cursor.getString(cursor.getColumnIndexOrThrow("end_time")));;
                            tv_sdate.setText(cursor.getString(cursor.getColumnIndexOrThrow("start_date")));
                            String offday = cursor.getString(cursor.getColumnIndexOrThrow("off_day"));

                            db_per_rate = cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));
                            db_stime = cursor.getString(cursor.getColumnIndexOrThrow("start_time"));
                            db_etime = cursor.getString(cursor.getColumnIndexOrThrow("end_time"));
                            db_start_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));

                            String net_am = cursor.getString(cursor.getColumnIndexOrThrow("net_amount"));


                            final salary_rate s = new salary_rate();
                            pre_rate = s.calulate_salary_rate_per_day(db_stime,db_etime,et_salary.getText().toString(),db_per_rate,offday);
                            pre_net = Double.valueOf(net_am);

                            for(int i = 0; i < offday.length();i++){
                                if(offday.charAt(i) == '1'){
                                    tv_sun.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border2);
                                    tv_sun.setTextColor(Color.rgb(0,0,0));
                                    i_sun=1;
                                }
                                else if(offday.charAt(i) == '2'){
                                    i_mon=1;
                                    tv_mon.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                                    tv_mon.setTextColor(Color.rgb(0,0,0));

                                }
                                else if(offday.charAt(i) == '3'){
                                    i_tue=1;
                                    tv_tue.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                                    tv_tue.setTextColor(Color.rgb(0,0,0));

                                }
                                else if(offday.charAt(i) == '4'){
                                    i_wed=1;
                                    tv_wed.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                                    tv_wed.setTextColor(Color.rgb(0,0,0));

                                }
                                else if(offday.charAt(i) == '5'){
                                    i_thu=1;
                                    tv_thu.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                                    tv_thu.setTextColor(Color.rgb(0,0,0));

                                }
                                else if(offday.charAt(i) == '6'){
                                    i_fri=1;
                                    tv_fri.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                                    tv_fri.setTextColor(Color.rgb(0,0,0));

                                }
                                else if(offday.charAt(i) == '7'){
                                    i_sat=1;
                                    tv_sat.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                                    tv_sat.setTextColor(Color.rgb(0,0,0));

                                }


                            }


                            break;
                        }
                    }
                    while(cursor.moveToNext());
                }

            } finally {
                // this gets called even if there is an exception somewhere above
                if(cursor != null)
                    cursor.close();
            }


        }
    }
    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
            //return 0.0;
        }

        do {

            startCal.add(Calendar.DAY_OF_MONTH, 1);

            ++workDays;

        } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()); //excluding end date

        return workDays;
    }
    public void off_day_calculation(){
        if(i_sun == 1){
            off_day = "1";
        }
        else {
            off_day = "";
        }
        if(i_mon == 1){
            off_day = off_day.concat("2");
        }
        if(i_tue == 1){
            off_day = off_day.concat("3");
        }
        if(i_wed == 1){
            off_day = off_day.concat("4");
        }
        if(i_thu == 1){
            off_day = off_day.concat("5");
        }
        if(i_fri == 1){
            off_day = off_day.concat("6");
        }
        if(i_sat == 1){
            off_day = off_day.concat("7");
        }

        if(i_sun == 0 && i_mon == 0 && i_tue == 0 && i_wed == 0 && i_fri == 0 && i_sat == 0){
            off_day = "0";
        }

    }

    public void off_day_simulate(){

        tv_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_sun == 0){

                    tv_sun.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_sun.setTextColor(Color.rgb(0,0,0));
                    i_sun=1;
                }
                else{

                    tv_sun.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_sun.setTextColor(Color.rgb(255,255,255));
                    i_sun=0;
                }
            }
        });
        tv_mon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_mon == 0){

                    tv_mon.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_mon.setTextColor(Color.rgb(0,0,0));
                    i_mon=1;
                }
                else{

                    tv_mon.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_mon.setTextColor(Color.rgb(255,255,255));
                    i_mon=0;
                }
            }
        });
        tv_tue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_tue == 0){

                    tv_tue.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_tue.setTextColor(Color.rgb(0,0,0));
                    i_tue=1;
                }
                else{

                    tv_tue.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_tue.setTextColor(Color.rgb(255,255,255));
                    i_tue=0;
                }
            }
        });
        tv_wed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_wed == 0){
                    tv_wed.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_wed.setTextColor(Color.rgb(0,0,0));
                    i_wed=1;
                }
                else{
                    tv_wed.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_wed.setTextColor(Color.rgb(255,255,255));
                    i_wed=0;
                }

            }
        });
        tv_thu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_thu == 0){

                    tv_thu.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_thu.setTextColor(Color.rgb(0,0,0));
                    i_thu=1;
                }
                else{

                    tv_thu.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_thu.setTextColor(Color.rgb(255,255,255));
                    i_thu=0;
                }

            }
        });
        tv_fri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_fri == 0){

                    tv_fri.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_fri.setTextColor(Color.rgb(0,0,0));
                    i_fri=1;
                }
                else{

                    tv_fri.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_fri.setTextColor(Color.rgb(255,255,255));
                    i_fri=0;
                }
            }
        });
        tv_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i_sat == 0){

                    tv_sat.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border);
                    tv_sat.setTextColor(Color.rgb(0,0,0));
                    i_sat=1;
                }
                else{

                    tv_sat.setBackgroundResource(hungers.com.emroze.digits.R.drawable.border4);
                    tv_sat.setTextColor(Color.rgb(255,255,255));
                    i_sat=0;
                }
            }
        });
    }

    public int d_day(String date)
    {
        String hh="";

        for(int i = 0; i < date.length(); i++){
            if(date.charAt(i) == '.'){
                break;
            }
            hh = hh.concat(String.valueOf(date.charAt(i)));
        }

        return Integer.valueOf(hh);
    }
    public int d_month(String date){
        int i = 0;

        String mm="";
        for(i = 0; i < date.length(); i++){

            if(date.charAt(i) == '.'){
                break;
            }
        }

        for (int j = i+1; j < date.length();j++){
            if(date.charAt(j) == '.'){
                break;
            }
            mm = mm.concat(String.valueOf(date.charAt(j)));
        }



        return Integer.valueOf(mm);
    }
    public int d_year(String date){
        int i = 0,k=0;

        String mm="";
        for(i = 0; i < date.length(); i++){

            if(date.charAt(i) == '.'){
                break;
            }
        }
        for(k = i+1; k < date.length(); k++){

            if(date.charAt(k) == '.'){
                break;
            }
        }

        for (int j = k+1; j < date.length();j++){
            mm = mm.concat(String.valueOf(date.charAt(j)));
        }



        return Integer.valueOf(mm);
    }
}
