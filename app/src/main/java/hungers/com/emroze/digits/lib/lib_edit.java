package hungers.com.emroze.digits.lib;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hungers.com.emroze.digits.R;
import hungers.com.emroze.digits.asset.asset_lack_calculation;
import hungers.com.emroze.digits.database.DatabaseHelper;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class lib_edit extends AppCompatActivity {
    EditText et1,et2;
    Button save;
    TextView tv1,tv2;

    public static String PREFS_NAME="digits";
    DatabaseHelper db_helper = new DatabaseHelper(this);

    TextView tv_per_week,tv_per_month,tv_per_year,tv_per_hour,tv_sdate,tv_date,tv_permanent;

    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout2;

    String db_start_date = "",db_per_rate = "week";

    GifTextView btn_gif,off_day_gif;

    DatePicker d1;
    Button pick_stime, pick_etime, pick_sdate;

    double p_rate = 0.0;

    int day ;
    int month;
    int year ;
    static ProgressDialog pd;
    double  pre_net = 0.0;

    String pre_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(hungers.com.emroze.digits.R.layout.activity_lib_edit);
        et1 = (EditText) findViewById(hungers.com.emroze.digits.R.id.et_asset_name);
        et2 = (EditText) findViewById(hungers.com.emroze.digits.R.id.et_amount);
        pd = new ProgressDialog(this);

        save = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_save);

        tv1 = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_asset_name);
        tv2 = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_amount);

        tv_per_hour = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_hour);
        tv_per_week = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_week);
        tv_per_month = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_month);
        tv_per_year = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_per_year);
        tv_date = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_start_date);
        tv_sdate = (TextView) findViewById(hungers.com.emroze.digits.R.id.tv_start_date_pick);
        tv_permanent = (TextView) findViewById(R.id.tv_permanent);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        tv1.setTypeface(type);
        tv2.setTypeface(type);
        tv_date.setTypeface(type);


        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        btn_gif = (GifTextView) findViewById(hungers.com.emroze.digits.R.id.btn_gif1);

        expandableLayout0 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_0);

        btn_gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });
        tv_per_hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout0.isExpanded()) {
                    expandableLayout0.collapse();
                }
                else {
                    expandableLayout0.expand();
                }
            }
        });

        tv_per_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per week");
                expandableLayout0.collapse();
                db_per_rate = "week";
            }
        });
        tv_per_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per month");
                expandableLayout0.collapse();
                db_per_rate = "month";
            }
        });
        tv_per_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("per year");
                expandableLayout0.collapse();
                db_per_rate = "year";
            }
        });
        tv_permanent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_per_hour.setText("fixed");
                expandableLayout0.collapse();
                db_per_rate = "fixed";
            }
        });

        expandableLayout2 = (ExpandableLayout) findViewById(hungers.com.emroze.digits.R.id.expandable_layout_2);

        tv_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                if (expandableLayout2.isExpanded()) {
                    expandableLayout2.collapse();
                }
                else {
                    expandableLayout2.expand();
                }
            }
        });

        //pick date
        pick_sdate = (Button) findViewById(hungers.com.emroze.digits.R.id.btn_pick_date);
        d1 = (DatePicker) findViewById(hungers.com.emroze.digits.R.id.datePicker);


        pick_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                day = d1.getDayOfMonth();
                month = d1.getMonth() + 1;
                year = d1.getYear();

                String m ="";

                if(month == 1){
                    m = "Jan";
                }
                else if(month == 2){
                    m = "Feb";
                }
                else if(month == 3){
                    m = "Mar";
                }
                else if(month == 4){
                    m = "Apr";
                }
                else if(month == 5){
                    m = "May";
                }
                else if(month == 6){
                    m = "June";
                }
                else if(month == 7){
                    m = "July";
                }
                else if(month == 8){
                    m = "Aug";
                }
                else if(month == 9){
                    m = "Sep";
                }
                else if(month == 10){
                    m = "Oct";
                }
                else if(month == 11){
                    m = "Nov";
                }
                else if(month == 12){
                    m = "Dec";
                }

                final String display_start_date = String.valueOf(day) + " "+m +", "+String.valueOf(year);

                db_start_date = String.valueOf(day) + "." + String.valueOf(month) +"."+String.valueOf(year) ;

                tv_sdate.setText(display_start_date);
                expandableLayout2.collapse();

            }
        });

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor = null;
        cursor = db.rawQuery("SELECT rowid _id,* FROM lib", null);
        try {

            if(cursor.moveToFirst()){
                do{

                    if(cursor.getString(cursor.getColumnIndexOrThrow("lib_name")).equals(preference.getString("lib_name",""))){

                        et1.setText(cursor.getString(cursor.getColumnIndexOrThrow("lib_name")));
                        et2.setText(cursor.getString(cursor.getColumnIndexOrThrow("lib_amount")));
                        tv_per_hour.setText(cursor.getString(cursor.getColumnIndexOrThrow("lib_type")));
                        tv_sdate.setText(cursor.getString(cursor.getColumnIndexOrThrow("start_date")));
                        db_start_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));
                        db_per_rate = cursor.getString(cursor.getColumnIndexOrThrow("lib_type"));

                        pre_name = cursor.getString(cursor.getColumnIndexOrThrow("lib_name"));

                        pre_net = Double.valueOf(preference.getString("lib_"+cursor.getString(cursor.getColumnIndexOrThrow("lib_name")),""));



                        if(db_per_rate.equals("week")){
                            p_rate = Double.valueOf(et2.getText().toString())/7.0;
                        }
                        else if(db_per_rate.equals("month")){
                            p_rate = Double.valueOf(et2.getText().toString())/30.0;
                        }
                        else if(db_per_rate.equals("year")){
                            p_rate = Double.valueOf(et2.getText().toString())/365.0;
                        }
                        else if(db_per_rate.equals("fixed")){
                            p_rate = 0.0;
                        }

                        break;
                    }
                }
                while(cursor.moveToNext());
            }

        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et1.getText().toString().equals("") || et1.getText().toString().equals("")){
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);
                    Toast.makeText(getApplicationContext(), "Fill the form", Toast.LENGTH_SHORT).show();

                }
                else {

                    asset_lack_calculation a =  new asset_lack_calculation();

                    double lack = a.calc_lack(getApplicationContext(),db_start_date,et2.getText().toString(),db_per_rate);

                    //Toast.makeText(getApplicationContext(), "lack "+String.valueOf(lack), Toast.LENGTH_SHORT).show();

                    double rate = 0.0;
                    double rate1 = 0;
                    double goal = Double.valueOf(preference.getString("goal",""));

                    rate1 = Double.valueOf(preference.getString("net_lib_rate",""))-p_rate;

                    //Toast.makeText(getApplicationContext(), "net lib rate pre  "+Double.valueOf(preference.getString("net_lib_rate","")), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), "old lib rate  "+String.valueOf(p_rate), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), "net - old "+String.valueOf(rate1), Toast.LENGTH_LONG).show();

                    if(db_per_rate.equals("week")){
                        rate = Double.valueOf(et2.getText().toString())/7.0;
                    }
                    else if(db_per_rate.equals("month")){
                        rate = Double.valueOf(et2.getText().toString())/30.0;
                    }
                    else if(db_per_rate.equals("year")){
                        rate = Double.valueOf(et2.getText().toString())/365.0;
                    }
                    else if(db_per_rate.equals("fixed")){
                        rate = 0.0;
                        lack = 0.0;
                        goal = goal + Double.valueOf(et2.getText().toString());
                    }

                    rate1 = rate1+rate;

                    //Toast.makeText(getApplicationContext(), "net lib rate updated  "+String.valueOf(rate1), Toast.LENGTH_LONG).show();

                    double rate_d = Double.valueOf(preference.getString("net_job_rate","")) - rate1
                            + Double.valueOf(preference.getString("net_asset_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));


                    double net = Double.valueOf(preference.getString("net","")) - pre_net;

                    //Toast.makeText(getApplicationContext(), "net "+String.valueOf(net), Toast.LENGTH_SHORT).show();

                    //Toast.makeText(getApplicationContext(), "Goal  "+String.valueOf(rate1), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), "pre_net "+String.valueOf(pre_net), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), "net - pre_net  "+String.valueOf(net), Toast.LENGTH_LONG).show();

                    net = goal + net - lack;

                    double days_count = net/rate_d;

                    int dayss = (int) days_count;

                    //Toast.makeText(getApplicationContext(), "final net  "+String.valueOf(net), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), "rate_d  "+String.valueOf(rate_d), Toast.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), "dayss  "+String.valueOf(dayss), Toast.LENGTH_LONG).show();

                    //Toast.makeText(getApplicationContext(), "net "+String.valueOf(net), Toast.LENGTH_SHORT).show();


                    Calendar ccc = Calendar.getInstance();

                    int ddd1 = ccc.get(Calendar.DAY_OF_MONTH);
                    int mmm1 = ccc.get(Calendar.MONTH);
                    int yyy1 = ccc.get(Calendar.YEAR);

                    SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy");
                    String in1 = String.valueOf(day)+" "+String.valueOf(month)+" "+String.valueOf(year);
                    String in2 = String.valueOf(ddd1)+" "+String.valueOf(mmm1+1)+" "+String.valueOf(yyy1);


                    Date date_start = null;
                    Date date_present = null;

                    double dif = 0;

                    try {
                        date_start = format.parse(in1);
                        date_present = format.parse(in2);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Calendar startc = Calendar.getInstance();
                    startc.setTime(date_start);

                    Calendar endc = Calendar.getInstance();
                    endc.setTime(date_present);


                    if (startc.getTimeInMillis() > endc.getTimeInMillis()) {
                        pd.setMessage("Loading...");
                        pd.show();
                        //Toast.makeText(asset_edit.this, "pd", Toast.LENGTH_SHORT).show();
                        dif = getWorkingDaysBetweenTwoDates(date_start, date_present);
                        dayss = dayss + (int)dif;
                    }


                    Calendar startCal = Calendar.getInstance();
                    startCal.setTime(date_present);

                    //Toast.makeText(getApplicationContext(), "dayys "+String.valueOf(dayss), Toast.LENGTH_SHORT).show();

                    int r = 0;
                    do {
                        startCal.add(Calendar.DAY_OF_MONTH, 1);
                        r++;
                    } while (r <= dayss); //excluding end date


                    String ddd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                            +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                            +String.valueOf(startCal.get(Calendar.YEAR));

                    editor.putString("goal_date",ddd);
                    editor.commit();
                    //db_helper.update_goal_date(ddd);

                    //db_helper.update_rate_lib(String.valueOf(rate1));

                    if(db_per_rate.equals("fixed")){
                        lack = Double.valueOf(et2.getText().toString());
                        //Toast.makeText(getApplicationContext(), "net amount  "+String.valueOf(lack), Toast.LENGTH_LONG).show();
                    }
                    else {
                        lack = a.calc_lack(getApplicationContext(),db_start_date,et2.getText().toString(),db_per_rate);
                    }

                    db_helper.update_lib_item(preference.getString("lib_name",""),et1.getText().toString(),et2.getText().toString(),db_per_rate,db_start_date,String.valueOf(lack));
                    editor.remove("lib_"+pre_name);
                    editor.apply();
                    editor.putString("net_lib_rate",String.valueOf(rate1));
                    editor.putString("lib_"+et1.getText().toString(),String.valueOf(lack));
                    editor.putString("refresh_lib","needed");
                    editor.putString("import","true");
                    editor.putString(et1.getText().toString()+"_check_lib","true");
                    editor.commit();


                    //asset_goal_date_handler g = new asset_goal_date_handler();
                    //g.asset_goal_timer(getApplicationContext(),et2.getText().toString());

                    finish();


                }
            }
        });

    }

    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
            //return 0.0;
        }

        do {

            startCal.add(Calendar.DAY_OF_MONTH, 1);

            ++workDays;

        } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis());

        pd.hide();
        return workDays;
    }

}
