package hungers.com.emroze.digits.bills;

/**
 * Created by emroze on 6/3/17.
 */

public class bills_item_details {

    public String bills_name;
    public String bills_amount;
    public String bills_type;
    public String start_time;
    public String end_time;
    public String start_date;
    public String off_day;

    public bills_item_details() {
    }

    public String getBills_name() {
        return bills_name;
    }

    public void setBills_name(String bills_name) {
        this.bills_name = bills_name;
    }

    public String getBills_amount() {
        return bills_amount;
    }

    public void setBills_amount(String bills_amount) {
        this.bills_amount = bills_amount;
    }

    public String getBills_type() {
        return bills_type;
    }

    public void setBills_type(String bills_type) {
        this.bills_type = bills_type;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getOff_day() {
        return off_day;
    }

    public void setOff_day(String off_day) {
        this.off_day = off_day;
    }
}