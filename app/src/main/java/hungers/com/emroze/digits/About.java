package hungers.com.emroze.digits;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class About extends AppCompatActivity {

    TextView tv1,tv2,tv3,tv4,tv5,tv6,tv_terms1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        tv_terms1 = (TextView ) findViewById(R.id.tv_terms1);
        tv1 = (TextView ) findViewById(R.id.tv_about);
        tv2 = (TextView ) findViewById(R.id.tv_content);
        tv3 = (TextView ) findViewById(R.id.tv_email1);
        tv4 = (TextView ) findViewById(R.id.tv111);
        tv5 = (TextView ) findViewById(R.id.tv_email2);
        tv6 = (TextView ) findViewById(R.id.tv_email3);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        tv1.setTypeface(typeface2);
        tv2.setTypeface(typeface);
        tv3.setTypeface(typeface);
        tv4.setTypeface(typeface2);
        tv_terms1.setTypeface(typeface2);
        tv5.setTypeface(typeface);
        tv6.setTypeface(typeface);

        //tv6.setText(Html.fromHtml("<a href=\"emrozeae@gmail.com\">Send Feedback</a>"));
        //tv6.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
