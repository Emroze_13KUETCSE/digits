package hungers.com.emroze.digits.job;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by emroze on 6/3/17.
 */

public class pre_date_job_lack {
    public static String PREFS_NAME="digits";

    public pre_date_job_lack() {
    }

    public Double calc(Context contex, String start_time, String end_time, int sec, int min, int hour, int date, int month, int year,String off_day){

        final SharedPreferences preference =contex.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);


        /*Calendar c1 = Calendar.getInstance();*/
        salary_rate ss = new salary_rate();

        //current hour
        /*int hour = c1.get(Calendar.HOUR_OF_DAY);
        int min = c1.get(Calendar.MINUTE);
        int sec = c1.get(Calendar.SECOND);

        int date = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH);
        int year = c1.get(Calendar.YEAR);*/

        //start hour
        int s_hour = preference.getInt("s_hour",0);
        int s_min = preference.getInt("s_min",0);
        int s_sec = 0;

        int s_date = preference.getInt("s_date",0);
        int s_month = preference.getInt("s_month",0);
        int s_year = preference.getInt("s_year",0);


        //start time and end time
        int start_hour = ss.hour(start_time);
        int start_min = ss.min(start_time);
        int end_hour = ss.hour(end_time);
        int end_min = ss.min(end_time);

        //convert to sec
        Double s_t = (Double)(((start_hour*60.0)+start_min)*60.0);
        Double e_t = (Double)(((end_hour*60.0)+end_min)*60.0);

        Double s = (Double)((((s_hour*60.0)+s_min)*60.0)+s_sec);
        Double c = (Double)((((hour*60.0)+min)*60.0)+sec);

        Double last = ((((23.0*60.0)+59.0)*60.0)+59.0);

        //convert to day
        Double c_d = (Double)((((year*12.0)+month+1)*30.0)+date);
        Double s_d = (Double)((((s_year*12.0)+s_month)*30.0)+s_date);

        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = String.valueOf(s_date)+" "+String.valueOf(s_month)+" "+String.valueOf(s_year);
        String inputString2 = String.valueOf(date)+" "+String.valueOf(month+1)+" "+String.valueOf(year);

        Double calc_sec = 0.0;

        Double diff = 0.0;

        if(s_d < c_d){

            try {
                    Date date1 = myFormat.parse(inputString1);
                    Date date2 = myFormat.parse(inputString2);
                    diff = getWorkingDaysBetweenTwoDates(contex,date1,date2,off_day);
                    //Toast.makeText(contex, String.valueOf(diff), Toast.LENGTH_SHORT).show();
                } catch (ParseException e) {
                    e.printStackTrace();

                }
            if(s_t < e_t){
                calc_sec = (diff)*(e_t-s_t);

                if(c > s_t && c < e_t){
                    calc_sec = calc_sec+(c-s_t);
                }
                else if(c >= e_t){
                    calc_sec = calc_sec + (e_t-s_t);
                }
            }
            else if(s_t > e_t){

                if(diff.equals(0.0)){
                    if(c > s_t && c <= last){
                        calc_sec = c-s_t;
                    }
                }
                else if(diff.equals(1.0)){

                    if(c >= 0 && c < e_t){
                        calc_sec = (last - s_t) + (c);
                    }
                    else if(c >= e_t && c < s_t){

                        calc_sec = (last - s_t) + e_t;

                    }
                    else  if(c >= s_t && c <= last){
                        calc_sec = (last - s_t) + (e_t) + (c-s_t);
                    }
                }
                else if(diff > 1){
                    calc_sec = ((last - s_t) + e_t)*(diff);
                    if(c >= 0 && c < e_t){
                        calc_sec = calc_sec + (last - s_t) + (c);
                    }
                    else if(c >= e_t && c < s_t){
                        calc_sec = calc_sec + (last - s_t) + e_t;
                    }
                    else  if(c >= s_t && c <= last){
                        calc_sec = calc_sec + (last - s_t) + (e_t) + (c-s_t);
                    }
                }
            }

        }
        else if(s_d.equals(c_d)){
            if(s_t < e_t){
                if(c >= s_t && c < e_t){
                    calc_sec = calc_sec+(c-s_t);
                }
                else if(c >= e_t){
                    calc_sec = calc_sec + (e_t-s_t);
                }
            }
            else if(s_t > e_t){
                if(c > s_t && c <= last){
                    calc_sec = c-s_t;
                }
            }

        }

        Toast.makeText(contex, "Total sec: "+String.valueOf(calc_sec) + "", Toast.LENGTH_SHORT).show();

        return calc_sec;
    }
    public static Double getWorkingDaysBetweenTwoDates(Context cccc,Date startDate, Date endDate,String off) {

        boolean sun,mon,tue,wed,thu,fri,sat;

        sun=false;
        mon=false;
        tue=false;
        wed=false;
        thu=false;
        fri=false;
        sat=false;

        if(off.equals(0)){
            sun=false;
            mon=false;
            tue=false;
            wed=false;
            thu=false;
            fri=false;
            sat=false;
        }
        else {
            for (int i =0; i < off.length();i++){
                if(off.charAt(i) == '1'){
                    sun = true;
                }
                else if(off.charAt(i) == '2'){
                    mon = true;
                }
                else if(off.charAt(i) == '3'){
                    tue = true;
                }
                else if(off.charAt(i) == '4'){
                    wed = true;
                }
                else if(off.charAt(i) == '5'){
                    thu = true;
                }
                else if(off.charAt(i) == '6'){
                    fri = true;
                }
                else if(off.charAt(i) == '7'){
                    sat = true;
                }
            }
        }

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;
        Double closeDays = 0.0;

        //Return 0 if start and end are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        if (startCal.getTimeInMillis() >= endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
        }

        do {

            if(sun == true){
                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                    ++closeDays;
                }
            }
            if(mon == true){
                if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                    ++closeDays;
                }
            }
            if(tue == true){
                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                    ++closeDays;
                }
            }
            if(wed == true){
                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                    ++closeDays;
                }
            }
            if(thu == true){
                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                    ++closeDays;
                }
            }
            if(fri == true){
                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
                    ++closeDays;
                }
            }
            if(sat == true){
                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                    ++closeDays;
                }
            }

            ++workDays;
            startCal.add(Calendar.DAY_OF_MONTH, 1);

        } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        Toast.makeText(cccc, "Total days "+String.valueOf(workDays)+"\n off days "+String.valueOf(closeDays), Toast.LENGTH_SHORT).show();

        return workDays-closeDays;
    }
}
