package hungers.com.emroze.digits.asset;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by emroze on 6/4/17.
 */

public class asset_cursor_adapter extends CursorAdapter {

    public asset_cursor_adapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(hungers.com.emroze.digits.R.layout.tv_asset_listview, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tv_job_name = (TextView) view.findViewById(hungers.com.emroze.digits.R.id.tv_asset_name_lv);
        TextView big = (TextView) view.findViewById(hungers.com.emroze.digits.R.id.tv_big_lv);
        TextView s_d = (TextView) view.findViewById(hungers.com.emroze.digits.R.id.tv_asset_s_date);

        TextView amount = (TextView) view.findViewById(hungers.com.emroze.digits.R.id.tv_amount_lv);

        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");
        tv_job_name.setTypeface(type);
        big.setTypeface(type);
        amount.setTypeface(type);


        String body = cursor.getString(cursor.getColumnIndexOrThrow("asset_name"));
       String asset1_amount = cursor.getString(cursor.getColumnIndexOrThrow("asset_amount"));

        String asset1_s_type = cursor.getString(cursor.getColumnIndexOrThrow("asset_type"));
        String amm = cursor.getString(cursor.getColumnIndexOrThrow("net_amount"));

        Double temp;

        if(asset1_s_type.equals("fixed")){
            temp= Double.valueOf(asset1_amount);
        }
        else {
            temp = Double.valueOf(amm);

        }



        String amount_and_type = "$ "+String.valueOf(temp)
                +"\n"+cursor.getString(cursor.getColumnIndexOrThrow("asset_amount"))+
                "/"+cursor.getString(cursor.getColumnIndexOrThrow("asset_type"));

        String str = String.valueOf(body.charAt(0));

        s_d.setText(cursor.getString(cursor.getColumnIndexOrThrow("start_date")));
        big.setText(str);
        amount.setText(amount_and_type);
        tv_job_name.setText(body);
    }
}
