package hungers.com.emroze.digits.lib;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.PowerManager;
import android.widget.Toast;

import hungers.com.emroze.digits.database.DatabaseHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.POWER_SERVICE;

/**
 * Created by emroze on 8/5/17.
 */

public class lib_handler {
    public static String PREFS_NAME="digits";
    public Double sum ;
    Double rate ;
    int flag = 0;
    int flag1 = 0;
    float s_h,l_h,c_h;
    int calculation=0;

    public boolean ttt = true;

    int start_day,start_month,start_year;

    private String current_date = "";

    public Thread t;

    public TimerTask timerTask;
    final Handler handler = new Handler();
    final Timer timer = new Timer();

    double diff = 0.0;
    double net_amount = 0.0;

    public lib_handler(){}

    public void lib_handler(final Context context , final String name, final String amount, final String start_date, final String rate){
        final DatabaseHelper db_helper = new DatabaseHelper(context);

        final Context finalCc = context;
        PowerManager powerManager = (PowerManager) finalCc.getSystemService(POWER_SERVICE);
        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");

        final SharedPreferences preference = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        t = new Thread() {

            @Override
            public void run() {
                try {
                    while (ttt) {

                        Calendar c = Calendar.getInstance();

                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int min = c.get(Calendar.MINUTE);

                        int date = c.get(Calendar.DAY_OF_MONTH);
                        int month = c.get(Calendar.MONTH);
                        int year = c.get(Calendar.YEAR);


                        if(start_date_extract(context,start_date)
                                && preference.getString("lib_"+name,null) != null){

                           /* if(preference.getString(name+"_check_lib","").equals("true")) {
                                lib_goal_date_handler hh = new lib_goal_date_handler();
                                hh.timer(finalCc,name);
                                editor.putString(name+"_check_lib","false");
                                editor.commit();
                            }
*/
                            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                            String inputString1 = String.valueOf(start_day)+" "+String.valueOf(start_month)+" "+String.valueOf(start_year); //start date
                            String inputString2 = String.valueOf(date)+" "+String.valueOf(month+1)+" "+String.valueOf(year); //present date

                            Date date1 = null;
                            Date date2 = null;

                            net_amount = Double.valueOf(preference.getString("lib_"+name,""));

                            try {
                                date1 = myFormat.parse(inputString1);
                                date2 = myFormat.parse(inputString2);
                                diff = getWorkingDaysBetweenTwoDates(date1,date2);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if(rate.equals("week")){
                                if(diff != 0 && (diff % 7.0) == 0){
                                    net_amount = net_amount + Double.valueOf(amount);

                                }
                            }
                            else if(rate.equals("month")){
                                if(diff != 0 && (diff % 30.0) == 0){
                                    net_amount = net_amount + Double.valueOf(amount);
                                }
                            }
                            else if(rate.equals("year")){
                                if(diff != 0 && (diff % 365.0) == 0){
                                    net_amount = net_amount + Double.valueOf(amount);
                                }
                            }

                            db_helper.update_jobs_net_amount(name,String.format("%.5f",net_amount));
                            editor.putString("lib_"+name,String.format("%.5f",net_amount));
                            editor.commit();
                        }

                        Thread.sleep(1000*60*5);
                    }

                } catch (InterruptedException e) {
                    Toast.makeText(finalCc, "inttt", Toast.LENGTH_SHORT).show();
                }
            }
        };

        t.start();
    }

    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            //startCal.setTime(endDate);
            //endCal.setTime(startDate);
            return 0.0;
        }

        do {

            startCal.add(Calendar.DAY_OF_MONTH, 1);

            ++workDays;

        } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()); //excluding end date

        return workDays;
    }

    public boolean start_date_extract(Context cccc,String s_date){

        Boolean bbb=false;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < s_date.length();i++){

            if(s_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(s_date.charAt(i)));
        }
        for(j = i+1; j < s_date.length();j++){

            if(s_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(s_date.charAt(j)));
        }
        for (int k = j+1; k < s_date.length();k++){
            year = year.concat(String.valueOf(s_date.charAt(k)));
        }


        int s_year= Integer.valueOf(year);
        int s_month = Integer.valueOf(month);
        int s_dd = Integer.valueOf(dd);

        start_day = Integer.valueOf(dd);
        start_month = Integer.valueOf(month);
        start_year = Integer.valueOf(year);

        s_month=s_month-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year){
            bbb=true;
        }
        else if(c_year == s_year){
            if(c_month > s_month){
                bbb=true;
            }
            else if(c_month == s_month){
                if(c_dd >= s_dd){
                    bbb=true;
                }
            }
        }
        return bbb;
    }
}
