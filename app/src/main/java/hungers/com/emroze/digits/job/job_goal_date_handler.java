package hungers.com.emroze.digits.job;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by emroze on 6/30/17.
 */

public class job_goal_date_handler {
    public static String PREFS_NAME="digits";

    private int daay,moonth,yeear;

    private Boolean check = false;

    public job_goal_date_handler(){}

    public void job_goal_timer(Context c, final String job_name, final String s_time, final String e_time, final String off_day, final String amount, final String s_type , final String start_date, double pre_rate) {

        //start_date_extract(c,start_date);
        final SharedPreferences preference = c.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final salary_rate s = new salary_rate();

        final double rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

        final double[] lack = {0.0};
        lack[0] = Double.valueOf(preference.getString(job_name,""));


        final double[] net_worth = {0.0};
        net_worth[0] = Double.valueOf(preference.getString("net",""));
        int net_worth_ = (int) net_worth[0];
        final double goal = Double.valueOf(preference.getString("goal","")) - lack[0];

        Calendar cd = Calendar.getInstance();

        int c_dd = cd.get(Calendar.DAY_OF_MONTH);
        int c_month = cd.get(Calendar.MONTH);
        int c_year = cd.get(Calendar.YEAR);

        //Toast.makeText(job_form1.this, String.valueOf(net_worth_), Toast.LENGTH_SHORT).show();

        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = String.valueOf(c_dd)+" "+String.valueOf(c_month+1)+" "+String.valueOf(c_year);
        Date date1 = null;
        final Calendar startCal = Calendar.getInstance();
        try {
            date1 = myFormat.parse(inputString1);
            startCal.setTime(date1);
            //Toast.makeText(getApplicationContext(), startCal.getTime().toString(), Toast.LENGTH_SHORT).show();
        } catch (ParseException e) {
            e.printStackTrace();

        }




        //Toast.makeText(getApplicationContext(),startCal.getTime().toString() , Toast.LENGTH_SHORT).show();
        boolean sun,mon,tue,wed,thu,fri,sat;

        sun=false;
        mon=false;
        tue=false;
        wed=false;
        thu=false;
        fri=false;
        sat=false;

        String off = off_day;

        int i = 0;
        for (i = 0; i < off.length(); i++){
            if(off.charAt(i) == '1'){
                sun = true;
            }
            else if(off.charAt(i) == '2'){
                mon = true;
            }
            else if(off.charAt(i) == '3'){
                tue = true;
            }
            else if(off.charAt(i) == '4'){
                wed = true;
            }
            else if(off.charAt(i) == '5'){
                thu = true;
            }
            else if(off.charAt(i) == '6'){
                fri = true;
                //
            }
            else if(off.charAt(i) == '7'){
                sat = true;
                //Toast.makeText(job_form1.this, "7", Toast.LENGTH_SHORT).show();
            }
        }

        final String[] ddd = {String.valueOf(startCal.get(Calendar.DAY_OF_MONTH)) + "/"
                + String.valueOf(startCal.get(Calendar.MONTH)) + "/"
                + String.valueOf(startCal.get(Calendar.YEAR))};


        //Toast.makeText(job_form1.this, ddd, Toast.LENGTH_SHORT).show();


        Double workDays = 0.0;
        Double closeDays = 0.0;
        Double day = 0.0;


        int goal_int = (int) goal;
//        Toast.makeText(c, String.valueOf(goal_int), Toast.LENGTH_SHORT).show();

                        /*do {

                            if(sun == false){
                                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                                    net_worth = net_worth + rate_d;
                                }
                            }
                            if(mon == false){
                                if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                                    net_worth = net_worth + rate_d;
                                }
                            }
                            if(tue == false){
                                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                                    net_worth = net_worth + rate_d;
                                }
                            }
                            if(wed == false){
                                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                                    net_worth = net_worth + rate_d;
                                }
                            }
                            if(thu == false){
                                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                                    net_worth = net_worth + rate_d;
                                }
                            }
                            if(fri == false){
                                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
                                    net_worth = net_worth + rate_d;
                                }
                            }
                            if(sat == false){
                                if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                                    net_worth = net_worth + rate_d;
                                }
                            }
                            count++;
                            startCal.add(Calendar.DAY_OF_MONTH, 1);
                            //net_worth_ = (int) net_worth;
                            net_worth_++;

                        } while ( net_worth_ != 130);*/

        ddd[0] =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                +String.valueOf(startCal.get(Calendar.YEAR));

        //Toast.makeText(job_form1.this, String.valueOf(net_worth_), Toast.LENGTH_SHORT).show();

        //Toast.makeText(c, ddd[0], Toast.LENGTH_SHORT).show();


        final boolean finalSun = sun;
        final boolean finalMon = mon;
        final boolean finalTue = tue;
        final boolean finalWed = wed;
        final boolean finalThu = thu;
        final boolean finalFri = fri;
        final boolean finalSat = sat;

        double rate = 0;

        if(lack[0] == 0){
            start_date_extract(c,start_date);
            rate = pre_rate;
            final double[] finalRate = {rate};
            Thread t = new Thread() {

                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {


                            if(net_worth[0] <= goal && rate_d > 0  && preference.getString("net_pos","").equals("1")) {

                                if(startCal.get(Calendar.DAY_OF_MONTH) == daay
                                        && (startCal.get(Calendar.MONTH)+1) == moonth
                                        && startCal.get(Calendar.YEAR) == yeear){
                                    finalRate[0] = rate_d;
                                }

                                if(finalSun == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                                        net_worth[0] = net_worth[0] + finalRate[0];
                                    }
                                }
                                if(finalMon == false){
                                    if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                                        net_worth[0] = net_worth[0] + finalRate[0];
                                    }
                                }
                                if(finalTue == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                                        net_worth[0] = net_worth[0] + finalRate[0];
                                    }
                                }
                                if(finalWed == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                                        net_worth[0] = net_worth[0] + finalRate[0];
                                    }
                                }
                                if(finalThu == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                                        net_worth[0] = net_worth[0] + finalRate[0];
                                    }
                                }
                                if(finalFri == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
                                        net_worth[0] = net_worth[0] + finalRate[0];
                                    }
                                }
                                if(finalSat == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                                        net_worth[0] = net_worth[0] + finalRate[0];
                                    }
                                }
                                startCal.add(Calendar.DAY_OF_MONTH, 1);

                                ddd[0] =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                        +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                        +String.valueOf(startCal.get(Calendar.YEAR));

                                editor.putString("goal_date",ddd[0]);
                                editor.commit();
                            }


                            Thread.sleep(8);
                        }
                    } catch (InterruptedException e) {
                    }
                }
            };
            t.start();
        }
        else {
            Thread t = new Thread() {

                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {


                            if(net_worth[0] <= goal && preference.getString("net_pos","").equals("1") && rate_d > 0) {
                                if(finalSun == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                                        net_worth[0] = net_worth[0] + rate_d;
                                    }
                                }
                                if(finalMon == false){
                                    if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                                        net_worth[0] = net_worth[0] + rate_d;
                                    }
                                }
                                if(finalTue == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                                        net_worth[0] = net_worth[0] + rate_d;
                                    }
                                }
                                if(finalWed == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY){
                                        net_worth[0] = net_worth[0] + rate_d;
                                    }
                                }
                                if(finalThu == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY){
                                        net_worth[0] = net_worth[0] + rate_d;
                                    }
                                }
                                if(finalFri == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
                                        net_worth[0] = net_worth[0] + rate_d;
                                    }
                                }
                                if(finalSat == false){
                                    if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                                        net_worth[0] = net_worth[0] + rate_d;
                                    }
                                }
                                startCal.add(Calendar.DAY_OF_MONTH, 1);

                                ddd[0] =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                        +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                        +String.valueOf(startCal.get(Calendar.YEAR));

                                editor.putString("goal_date",ddd[0]);
                                editor.commit();
                            }

                        /*editor.putString("goal_date",String.valueOf(net_worth[0])+" "+String.valueOf(goal));
                        editor.commit();*/

                            Thread.sleep(8);
                        }
                    } catch (InterruptedException e) {
                    }
                }
            };
            t.start();
        }


    }

    public boolean start_date_extract(Context cccc,String s_date){

        Boolean bbb=false;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < s_date.length();i++){

            if(s_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(s_date.charAt(i)));
        }
        for(j = i+1; j < s_date.length();j++){

            if(s_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(s_date.charAt(j)));
        }
        for (int k = j+1; k < s_date.length();k++){
            year = year.concat(String.valueOf(s_date.charAt(k)));
        }


        int s_year= Integer.valueOf(year);
        int s_month = Integer.valueOf(month);
        int s_dd = Integer.valueOf(dd);

        yeear= Integer.valueOf(year);
        moonth = Integer.valueOf(month);
        daay = Integer.valueOf(dd);

        s_month=s_month-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year){
            bbb=true;
        }
        else if(c_year == s_year){
            if(c_month > s_month){
                bbb=true;
            }
            else if(c_month == s_month){
                if(c_dd >= s_dd){
                    bbb=true;
                }
            }
        }
        return bbb;
    }

}
