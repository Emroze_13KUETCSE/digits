package hungers.com.emroze.digits;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Terms extends AppCompatActivity {

    TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,tv_terms1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        tv_terms1 = (TextView ) findViewById(R.id.tv_terms1);
        t1 = (TextView ) findViewById(R.id.t1);
        t2 = (TextView ) findViewById(R.id.t2);
        t3 = (TextView ) findViewById(R.id.t3);
        t4 = (TextView ) findViewById(R.id.t4);
        t5 = (TextView ) findViewById(R.id.t5);
        t6 = (TextView ) findViewById(R.id.t6);
        t7 = (TextView ) findViewById(R.id.t7);
        t8 = (TextView ) findViewById(R.id.t8);
        t9 = (TextView ) findViewById(R.id.t9);
        t10 = (TextView )findViewById(R.id.t10);
        t11 = (TextView ) findViewById(R.id.t11);
        t12 = (TextView ) findViewById(R.id.t12);
        t13 = (TextView ) findViewById(R.id.t13);
        t14 = (TextView ) findViewById(R.id.t14);
        t15 = (TextView ) findViewById(R.id.t15);
        t16 = (TextView ) findViewById(R.id.t16);
        t17 = (TextView ) findViewById(R.id.t17);
        t18 = (TextView ) findViewById(R.id.t18);
        t19 = (TextView ) findViewById(R.id.t19);
        t20 = (TextView ) findViewById(R.id.t20);
        t21 = (TextView ) findViewById(R.id.t21);
        t22 = (TextView ) findViewById(R.id.t22);
        t23 = (TextView ) findViewById(R.id.t23);
        t24 = (TextView ) findViewById(R.id.t24);


        t2.setTypeface(typeface);
        t4.setTypeface(typeface);
        t6.setTypeface(typeface);
        t8.setTypeface(typeface);
        t10.setTypeface(typeface);
        t12.setTypeface(typeface);
        t14.setTypeface(typeface);
        t16.setTypeface(typeface);
        t18.setTypeface(typeface);
        t20.setTypeface(typeface);
        t22.setTypeface(typeface);
        t24.setTypeface(typeface);

        t1.setTypeface(typeface2);
        t3.setTypeface(typeface2);
        t5.setTypeface(typeface2);
        t7.setTypeface(typeface2);
        t9.setTypeface(typeface2);
        t11.setTypeface(typeface2);
        t13.setTypeface(typeface2);
        t15.setTypeface(typeface2);
        t17.setTypeface(typeface2);
        t19.setTypeface(typeface2);
        t21.setTypeface(typeface2);
        t23.setTypeface(typeface2);
        tv_terms1.setTypeface(typeface2);

    }
}
