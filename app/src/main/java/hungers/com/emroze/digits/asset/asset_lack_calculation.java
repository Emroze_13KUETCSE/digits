package hungers.com.emroze.digits.asset;

import android.content.Context;

import hungers.com.emroze.digits.job.salary_rate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by emroze on 7/31/17.
 */

public class asset_lack_calculation  {
    public static String PREFS_NAME="digits";

    double lack = 0.0, diff = 0.0;

    int start_day;
    int start_month;
    int start_year;

    public asset_lack_calculation() {
    }

    public double calc_lack(Context context, String start_date, String amount, String type ){

        double am = Integer.valueOf(amount);

        start_date_extract(context,start_date);

        Calendar c1 = Calendar.getInstance();
        salary_rate ss = new salary_rate();

        //current hour

        int date = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH);
        int year = c1.get(Calendar.YEAR);

        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = String.valueOf(start_day)+" "+String.valueOf(start_month)+" "+String.valueOf(start_year); //start date
        String inputString2 = String.valueOf(date)+" "+String.valueOf(month+1)+" "+String.valueOf(year); //present date

        //Toast.makeText(context, inputString1, Toast.LENGTH_SHORT).show();

        //Toast.makeText(context, inputString2, Toast.LENGTH_SHORT).show();

        Date date1 = null;

        try {

            date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            diff = getWorkingDaysBetweenTwoDates(date1,date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Toast.makeText(context, "diff "+String.valueOf(am), Toast.LENGTH_SHORT).show();

        if(type.equals("week")){
            lack = am * ((int)(diff/7.0));
        }
        else if(type.equals("month")){

                lack = am * (int)(diff/30.0);
        }
        else if(type.equals("year")){
            lack = am * ((diff/365.0));
        }

       // Toast.makeText(context, "lack "+String.valueOf(lack), Toast.LENGTH_SHORT).show();

        return lack;
    }
    public boolean start_date_extract(Context cccc,String s_date){

        Boolean bbb=false;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < s_date.length();i++){

            if(s_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(s_date.charAt(i)));
        }
        for(j = i+1; j < s_date.length();j++){

            if(s_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(s_date.charAt(j)));
        }
        for (int k = j+1; k < s_date.length();k++){
            year = year.concat(String.valueOf(s_date.charAt(k)));
        }


        int s_year= Integer.valueOf(year);
        int s_month = Integer.valueOf(month);
        int s_dd = Integer.valueOf(dd);

        start_day = Integer.valueOf(dd);
        start_month = Integer.valueOf(month);
        start_year = Integer.valueOf(year);

        s_month=s_month-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year){
            bbb=true;
        }
        else if(c_year == s_year){
            if(c_month > s_month){
                bbb=true;
            }
            else if(c_month == s_month){
                if(c_dd >= s_dd){
                    bbb=true;
                }
            }
        }
        return bbb;
    }
    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() >= endCal.getTimeInMillis()) {
            //startCal.setTime(endDate);
            //endCal.setTime(startDate);
            return 0.0;
        }

        do {

            startCal.add(Calendar.DAY_OF_MONTH, 1);

            ++workDays;

        } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        return workDays;
    }
}
