package hungers.com.emroze.digits.job;

/**
 * Created by emroze on 6/3/17.
 */

public class salary_rate {
    private Double s_h,s_m,e_h,e_m = 0.0;
    private Double s_m_convert_to_h,e_m_convert_to_h = 0.0;
    private Double total,diff_hour,rate = 0.0;
    private Double rate_d = 0.0;

    public salary_rate() {
    }

    public Double calulate_salary_rate(String s_time,String e_time,String amount,String type,String off_day){

        //s_time = 9:0, e_time = 17:0, amount = 1000, type = hour/week/month/year

        s_h = Double.valueOf(hour(s_time));
        s_m = Double.valueOf(min(s_time));
        e_h = Double.valueOf(hour(e_time));
        e_m = Double.valueOf(min(e_time));

        total = Double.valueOf(amount);

        diff_hour = calculate_diff_of_hour(s_h,s_m,e_h,e_m);

        int len = work_day_calc(off_day);

        if(type.equals("hour")){
            rate = total/(60*60);
        }
        else if(type.equals("week")){
            rate = total/(len*diff_hour*60*60);
        }
        else if(type.equals("month")){
            rate = total/(4*len*diff_hour*60*60);
        }
        else if(type.equals("year")){
            rate = total/(52*len*diff_hour*60*60);
        }

        return rate;
    }
    public Double calulate_salary_rate_per_day(String s_time,String e_time,String amount,String type,String off_day){

        //s_time = 9:0, e_time = 17:0, amount = 1000, type = hour/week/month/year

        s_h = Double.valueOf(hour(s_time));
        s_m = Double.valueOf(min(s_time));
        e_h = Double.valueOf(hour(e_time));
        e_m = Double.valueOf(min(e_time));

        total = Double.valueOf(amount);

        diff_hour = calculate_diff_of_hour(s_h,s_m,e_h,e_m);

        int len = work_day_calc(off_day);

        if(type.equals("hour")){
            rate_d = total*diff_hour;
        }
        else if(type.equals("week")){
            rate_d = total/(7.0);
        }
        else if(type.equals("month")){
            rate_d = total/(30);
        }
        else if(type.equals("year")){
            rate_d = total/(365);
        }

        return rate_d;
    }

    public int work_day_calc(String off_day){

        int off_day_len = off_day.length();
        int len = 0;

        if(off_day_len == 1){
            if(off_day.equals("0")){
                len = 7;
            }
            else {
                len = 6;
            }
        }
        else {
            len = 7-off_day_len;
        }

        return len;
    }
    public Double calculate_diff_of_hour(Double s_hh,Double s_mm, Double e_hh,Double e_mm){


        s_m_convert_to_h = (Double)(s_mm/60.0);
        s_hh = s_hh + s_m_convert_to_h;

        e_m_convert_to_h = (Double)(e_mm/60.0);
        e_hh = e_hh + e_m_convert_to_h;


        if(s_hh > e_hh){
            diff_hour = (e_hh + 24) - s_hh;
        }
        else{
            diff_hour=e_hh-s_hh;
        }


        return diff_hour;
    }

    public int hour(String time){

        String hh="";

        for(int i = 0; i < time.length(); i++){
            if(time.charAt(i) == ':'){
                break;
            }
            hh = hh.concat(String.valueOf(time.charAt(i)));
        }

        return Integer.valueOf(hh);
    }
    public int min(String time){

        int i = 0;

        String mm="";
        for(i = 0; i < time.length(); i++){

            if(time.charAt(i) == ':'){
                break;
            }
        }

        for (int j = i+1; j < time.length();j++){
            mm = mm.concat(String.valueOf(time.charAt(j)));
        }



        return Integer.valueOf(mm);
    }


}
