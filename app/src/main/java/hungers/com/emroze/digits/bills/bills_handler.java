package hungers.com.emroze.digits.bills;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.PowerManager;
import android.widget.Toast;

import hungers.com.emroze.digits.database.DatabaseHelper;
import hungers.com.emroze.digits.job.salary_rate;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.POWER_SERVICE;

/**
 * Created by emroze on 6/4/17.
 */

public class bills_handler {
    public static String PREFS_NAME="digits";
    public Double sum ;
    Double rate ;
    int flag = 0;
    int flag1 = 0;
    float s_h,l_h,c_h;
    int calculation=0;

    public boolean ttt = true;
    private String current_date = "";

    public Thread t;

    public TimerTask timerTask;
    final Handler handler = new Handler();
    final Timer timer = new Timer();
    public bills_handler() {
    }

    public void job_timer(Context c, final String job_name, final String s_time, final String e_time, final String off_day, final String amount, final String s_type , final String start_date){

        final DatabaseHelper db_helper = new DatabaseHelper(c);

        final SharedPreferences preference = c.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        final boolean[] check = {true};
        final boolean[] check1 = {true};


        Context cc = null;
        cc = c;
        final float[] c_h = {0};

        final salary_rate s = new salary_rate();

        Calendar c1 = Calendar.getInstance();

        //sum = Double.valueOf(net_salary);

        final Context finalCc = cc;
        PowerManager powerManager = (PowerManager) c.getSystemService(POWER_SERVICE);
        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");


        t = new Thread() {

            @Override
            public void run() {
                try {
                    while (ttt) {

                        Calendar c = Calendar.getInstance();

                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int min = c.get(Calendar.MINUTE);
                        int date = c.get(Calendar.DAY_OF_WEEK);

                        int ddd = c.get(Calendar.DAY_OF_MONTH);
                        int month = c.get(Calendar.MONTH);
                        int year = c.get(Calendar.YEAR);

                        current_date = String.valueOf(ddd) + "." + String.valueOf(month) +"."+String.valueOf(year) ;

                        String ssss= String.valueOf(date);

                        for(int i = 0; i < off_day.length(); i++ ){

                            if(ssss.charAt(0) == off_day.charAt(i)){
                                calculation = 0;
                                break;
                            }
                            else {
                                calculation = 1;
                            }
                        }



                        c_h[0] = (float) (hour +(min/60.0));

                        //Sunday == 1, Monday == 2, tuesday == 3.......Satarday == 7

                        //Toast.makeText(finalCc, off_day/*String.valueOf(calculation)*/, Toast.LENGTH_SHORT).show();

                        if(start_date_extract(finalCc,start_date) && preference.getString(job_name,null) != null){
                            //Toast.makeText(finalCc, "calculation "+String.valueOf(calculation), Toast.LENGTH_SHORT).show();

                            if(calculation == 1){

                                    /*if(preference.getString(job_name+"_check_bill","").equals("true")){

                                        if (check1[0]){


                                        }
                                        else {
                                            bill_goal_date_handler j_h = new bill_goal_date_handler();

                                            j_h.bill_goal_timer(finalCc,
                                                    job_name,
                                                    s_time,
                                                    e_time,
                                                    off_day,
                                                    amount,
                                                    s_type,
                                                    start_date
                                            );
                                        }

                                        editor.putString(job_name+"_check_bill","false");
                                        editor.commit();
                                        check[0] = false;
                                    }
*/


                                int last_hour = s.hour(e_time);

                                int last_min = s.min(e_time);

                                int start_hour = s.hour(s_time);

                                int start_min = s.min(s_time);

                                s_h = (float) (start_hour + (start_min/60.00));
                                l_h = (float) (last_hour + (last_min/60.00));

                                rate = s.calulate_salary_rate(s_time,e_time,amount,s_type,off_day);

                                if(s_h < l_h){
                                    if (c_h[0] >= s_h && c_h[0] <= l_h) {
                                        flag = 1;
                                        flag1 = 1;
                                        //Toast.makeText(finalCc, "here1", Toast.LENGTH_SHORT).show();
                                        wakeLock.acquire();
                                    }
                                    else {
                                        flag = 2;
                                        flag1 = 1;
                                        //Toast.makeText(finalCc, "here2", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else{
                                    if (c_h[0] <= l_h && c_h[0] <= s_h) {
                                        flag = 1;
                                        flag1 = 1;

                                        //Toast.makeText(finalCc, "here3", Toast.LENGTH_SHORT).show();
                                        wakeLock.acquire();
                                    }
                                    else {
                                        flag = 2;
                                        flag1 = 1;
                                        //Toast.makeText(finalCc, "here4", Toast.LENGTH_SHORT).show();
                                    }
                                }


                                if (c_h[0] == l_h) {
                                    flag = 2;
                                    flag1 = 1;
                                    wakeLock.release();
                                }

                                if(flag == 1){

                                    if (flag1 == 1) {
                                        flag1 = 2;
                                        try{
                                            sum = Double.valueOf(preference.getString(job_name,""));
                                        }
                                        catch (NumberFormatException e){

                                        }

                                    }
                                    sum = sum + rate;

                                    db_helper.update_bills_net_amount(job_name,String.format("%.5f",sum));
                                    editor.putString(job_name,String.format("%.5f",sum));
                                    editor.commit();


                                    //Toast.makeText(finalCc, preference.getString(job_name,"")+job_name, Toast.LENGTH_SHORT).show();

                                }
                            }
                        }

                        Thread.sleep(10000);
                    }

                } catch (InterruptedException e) {
                    Toast.makeText(finalCc, "inttt", Toast.LENGTH_SHORT).show();
                }
            }
        };

        t.start();


    }

    public void stop(){
        t.interrupt();
    }

    public boolean start_date_extract(Context cccc,String s_date){

        Boolean bbb=false;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < s_date.length();i++){

            if(s_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(s_date.charAt(i)));
        }
        for(j = i+1; j < s_date.length();j++){

            if(s_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(s_date.charAt(j)));
        }
        for (int k = j+1; k < s_date.length();k++){
            year = year.concat(String.valueOf(s_date.charAt(k)));
        }


        int s_year= Integer.valueOf(year);
        int s_month = Integer.valueOf(month);
        int s_dd = Integer.valueOf(dd);

        s_month=s_month-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year){
            bbb=true;
        }
        else if(c_year == s_year){
            if(c_month > s_month){
                bbb=true;
            }
            else if(c_month == s_month){
                if(c_dd >= s_dd){
                    bbb=true;
                }
            }
        }
        return bbb;
    }
}
