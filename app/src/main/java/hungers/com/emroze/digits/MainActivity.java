package hungers.com.emroze.digits;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.ajts.androidmads.library.SQLiteToExcel;
import com.dropbox.core.android.Auth;
import com.dropbox.core.v2.users.FullAccount;
import hungers.com.emroze.digits.asset.asset_cursor_adapter;
import hungers.com.emroze.digits.asset.asset_edit;
import hungers.com.emroze.digits.asset.asset_insert;
import hungers.com.emroze.digits.asset.asset_lack_calculation;
import hungers.com.emroze.digits.bills.bills_cursor_adapter;
import hungers.com.emroze.digits.bills.bills_details;
import hungers.com.emroze.digits.bills.bills_form;
import hungers.com.emroze.digits.bills.bills_lack_calculation;
import hungers.com.emroze.digits.bills.bills_rate;
import hungers.com.emroze.digits.database.DatabaseHelper;
import hungers.com.emroze.digits.dropbox.DownloadTask;
import hungers.com.emroze.digits.dropbox.DropboxClient;
import hungers.com.emroze.digits.dropbox.UploadTask;
import hungers.com.emroze.digits.dropbox.UserAccountTask;
import hungers.com.emroze.digits.job.TodoCursorAdapter;
import hungers.com.emroze.digits.job.display_jobs;
import hungers.com.emroze.digits.job.job_form1;
import hungers.com.emroze.digits.job.salary_lack_calculation;
import hungers.com.emroze.digits.job.salary_rate;
import hungers.com.emroze.digits.job.watcher;
import hungers.com.emroze.digits.lib.lib_cursor_adapter;
import hungers.com.emroze.digits.lib.lib_edit;
import hungers.com.emroze.digits.lib.lib_handler;
import hungers.com.emroze.digits.lib.lib_insert;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.gordonwong.materialsheetfab.MaterialSheetFabEventListener;
import com.mingle.sweetpick.CustomDelegate;
import com.mingle.sweetpick.DimEffect;
import com.mingle.sweetpick.SweetSheet;
import com.squareup.picasso.Picasso;
import com.tomerrosenfeld.customanalogclockview.CustomAnalogClock;
import com.xenione.digit.TabDigit;
import net.cachapa.expandablelayout.ExpandableLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import cn.pedant.SweetAlert.SweetAlertDialog;
import devlight.io.library.ntb.NavigationTabBar;
import pl.droidsonroids.gif.GifTextView;

public class MainActivity extends AppCompatActivity implements Runnable{
    public static String PREFS_NAME="digits";
    DatabaseHelper db_helper = new DatabaseHelper(this);

    private ViewPager income_viewPager,cost_viewPager;

    int count = 0;
    View view0,view1,view2,view3,view4;

    private TabDigit mCharHighSecond;
    private TabDigit mCharLowSecond;
    private TabDigit mCharHighMinute;
    private TabDigit mCharLowMinute;
    private TabDigit mCharHighHour;
    private TabDigit mCharLowHour;

    ExpandableLayout e1,e2,e3,e4;

    private LinearLayout mClock;

    private boolean mPause = true;

    private long elapsedTime = 0;

    private static final char[] HOURS = new char[]{'0', '1', '2'};

    private static final char[] SEXAGISIMAL = new char[]{'0', '1', '2', '3', '4', '5'};

    private SweetSheet mSweetSheet;
    private RelativeLayout home,rl;
    private LinearLayout gif_layout;

    ExpandableLayout expandableLayout0;
    GifTextView g1;

    private MaterialSheetFab materialSheetFab_job,materialSheetFab_asset,materialSheetFab_bill,materialSheetFab_lib;

    private NavigationTabStrip mTopNavigationTabStrip,mTopNavigationTabStrip2;

    private LinearLayout ll_concat;
    private RelativeLayout rl_job,rl_asset;

    ListView lv_job,lv_asset,lv_bill,lv_lib;

    TextView tv_amount_asset,tv_amount_job,tv_amount_bill,tv_amount_lib;

    TextView tv_total_job,tv_goal,tv_total_asset,tv_total_bill,tv_total_lib,tv_net,tv_time;

    info.hoang8f.widget.FButton btn;

    final Handler handler = new Handler();
    final Timer timer = new Timer();

    String job_name,amount,s_type,s_time,e_time,s_date,off_day,start_date = "";
    String bills_name,bills_amount,bills_s_type,bills_s_time,bills_e_time,bills_s_date,bills_off_day,bills_start_date = "";
    String jobName;

    View income_assets,income_jobs,cost_libs,cost_bills;
    View income_assets_edit,income_jobs_edit,cost_libs_edit,cost_bills_edit;

    FileChannel source=null;
    FileChannel destination=null;

    TextView tv_1,tv_2,tv_auto,tv_clock,tv_about,tv_terms,tv_ack,tv_help;
    TextView tv_pro,tv_uname,tv_email,tv_dp;

    private static final int FILE_REQUEST_CODE = 101;
    private String ACCESS_TOKEN;
    private LinearLayout l;


    private String job_details [][];
    private String bill_details [][];
    private String asset_details [][];
    private String lib_details [][];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        setContentView(R.layout.activity_main);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        checkFilePermissions();

        startService(new Intent(getApplicationContext(),watcher.class));

        if(preference.getString("backup_exit","").equals("true")){
            editor.putString("backup_exit","false");
            editor.commit();
            create_preference();

        }

        initUI();

        //exportDB();

        //SQLite2Excel();

        final Handler handler= new Handler();
        final Timer timer = new Timer();

        /*final TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        //exportDB();
                        //SQLite2Excel();
                       importDB1();
                    }
                });
            }
        };
        timer.schedule(timerTask, 0,5000);
*/

        if(preference.getString("resume_home1","").equals("on")){
            editor.putString("resume_home1","off");
            editor.commit();


            final TimerTask timerTask = new TimerTask() {

                @Override
                public void run() {
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            //exportDB();
                            //

                            if(preference.getString("import","").equals("false")){

                                importDB();

                            }
                            else if(preference.getString("import","").equals("true")){
                                exportDB();
                                //SQLite2Excel();
                            }
                        }
                    });
                }
            };
            timer.schedule(timerTask, 0,1000*10);
        }
    }

    private void initUI() {

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        rl = (RelativeLayout) findViewById(R.id.rl);
        view0 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.home1, null, false);
        view1 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.income1, null, false);
        view2 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.cost, null, false);
        view3 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.setting, null, false);
        view4 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.profile1, null, false);

        view0(view0);
        View1(view1);
        View2(view2);
        View3(view3);
        View4(view4);

        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.home),
                        Color.rgb(48,57,55))
                        //.selectedIcon(getResources().getDrawable(R.drawable.ic_sixth))
                        .title("Heart")
                        .badgeTitle("NTB")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.income),
                        Color.rgb(48,57,55))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("Cup")
                        .badgeTitle("with")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.cost),
                        Color.rgb(48,57,55))
                        // .selectedIcon(getResources().getDrawable(R.drawable.ic_seventh))
                        .title("Diploma")
                        .badgeTitle("state")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.setting),
                        Color.rgb(48,57,55))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("Flag")
                        .badgeTitle("icon")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.profile),
                        Color.rgb(48,57,55))
                        //.selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("Medal")
                        .badgeTitle("777")
                        .build()
        );

        navigationTabBar.setModels(models);

        if(preference.getString("page","").equals("1")){
            navigationTabBar.setModelIndex(1,true);
            rl.addView(view1);
        }
        else if(preference.getString("page","").equals("2")){
            navigationTabBar.setModelIndex(2,true);
            rl.addView(view2);
        }
        else if(preference.getString("page","").equals("3")){
            navigationTabBar.setModelIndex(3,true);
            rl.addView(view3);
        }
        else if(preference.getString("page","").equals("4")){
            navigationTabBar.setModelIndex(4,true);
            rl.addView(view4);
        }
        else if(preference.getString("page","").equals("0")){
            navigationTabBar.setModelIndex(0,true);
            rl.addView(view0);
        }

        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(NavigationTabBar.Model model, int i) {
                if(i == 0){
                    rl.removeAllViews();
                    //rl.setRotationY(i*-30);
                    rl.addView(view0);
                    //view0.setAnimation(inFromRightAnimation());
                    fade_in_out(view0);
                    editor.putString("page","0");
                    editor.commit();
                }
                else if(i == 1){
                    rl.removeAllViews();
                    //rl.setRotationY(i*-30);
                    rl.addView(view1);
                    fade_in_out(view1);
                    onResume_job();
                    editor.putString("page","1");
                    editor.commit();
                }
                else if(i == 2){
                    rl.removeAllViews();
                    rl.addView(view2);
                    fade_in_out(view2);
                    onResume_bill();
                    editor.putString("page","2");
                    editor.commit();
                }
                else if(i == 3){
                    rl.removeAllViews();
                    rl.addView(view3);
                    fade_in_out(view3);
                    editor.putString("page","3");
                    editor.commit();
                }
                else if(i == 4){
                    rl.removeAllViews();
                    rl.addView(view4);
                    fade_in_out(view4);
                    editor.putString("page","4");
                    editor.commit();
                }
            }

            @Override
            public void onEndTabSelected(NavigationTabBar.Model model, int index) {

            }
        });
        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);



    }

    private void View4(View view4) {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        l = (LinearLayout) view4.findViewById(R.id.l_dropbox);
        tv_pro = (TextView ) view4.findViewById(R.id.tv_profile);
        tv_uname = (TextView ) view4.findViewById(R.id.tv_name);
        tv_email = (TextView ) view4.findViewById(R.id.tv_email);
        tv_dp = (TextView ) view4.findViewById(R.id.tv_dp);

        tv_pro.setTypeface(typeface2);
        tv_uname.setTypeface(typeface2);
        tv_email.setTypeface(typeface);
        tv_dp.setTypeface(typeface);

        TextView name = (TextView) view4.findViewById(R.id.tv_name);
        TextView email = (TextView) view4.findViewById(R.id.tv_email);



        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putString("auth","true");
                editor.commit();
                Auth.startOAuth2Authentication(getApplicationContext(), getString(R.string.APP_KEY));
                //Toast.makeText(MainActivity.this, "view", Toast.LENGTH_SHORT).show();
            }
        });
        ACCESS_TOKEN = retrieveAccessToken();
        if(ACCESS_TOKEN != null){
            name.setText(preference.getString("user_name",""));
            email.setText(preference.getString("email",""));
        }
        getUserAccount();
    }

    private boolean tokenExists() {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        String accessToken = preference.getString("access-token", null);

        return accessToken != null;
    }
    private void upload() {
        if (ACCESS_TOKEN == null)
            return;
        //Select image to upload

       // Toast.makeText(this, "upload", Toast.LENGTH_SHORT).show();
        File sd = Environment.getExternalStorageDirectory();

        String backupDBPath = "/Digits/digits.db";
        File currentDB = new File(sd, backupDBPath);

        if (currentDB != null) {
            //Initialize UploadTask

            new UploadTask(DropboxClient.getClient(ACCESS_TOKEN), currentDB, getApplicationContext()).execute();

        }
    }
    private void download() {
        if (ACCESS_TOKEN == null)return;
        //Select image to upload
        //Toast.makeText(this, ACCESS_TOKEN, Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, "download", Toast.LENGTH_SHORT).show();

        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Digits/Download";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();

        String backupDBPath = "/Digits/digits.db";
        String down = "/Digits/Download/digits.db";
        File currentDB = new File(sd, backupDBPath);
        File path = new File(sd, down);


            //Toast.makeText(this, "kk1", Toast.LENGTH_SHORT).show();
        new DownloadTask(DropboxClient.getClient(ACCESS_TOKEN), path, getApplicationContext()).execute();

    }
    protected void getUserAccount() {
        if (ACCESS_TOKEN == null)return;
        //Toast.makeText(this, "user account", Toast.LENGTH_SHORT).show();

        new UserAccountTask(DropboxClient.getClient(ACCESS_TOKEN), new UserAccountTask.TaskDelegate() {
            @Override
            public void onAccountReceived(FullAccount account) {
                //Print account's info
                updateUI(account);
            }
            @Override
            public void onError(Exception error) {

            }
        }).execute();
    }
    private void updateUI(FullAccount account) {

        //Toast.makeText(this, "update ui", Toast.LENGTH_SHORT).show();

        ImageView profile = (ImageView) view4.findViewById(R.id.pro_pic);
        ImageView profile1 = (ImageView) view4.findViewById(R.id.imageView);

        TextView name = (TextView) view4.findViewById(R.id.tv_name);
        TextView email = (TextView) view4.findViewById(R.id.tv_email);
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        editor.putString("user_name",account.getName().getDisplayName());
        editor.putString("email",account.getEmail());
        editor.commit();

        name.setText(preference.getString("user_name",""));
        email.setText(preference.getString("email",""));

        Picasso pic = null;
        pic.with(this)
                .load(account.getProfilePhotoUrl())
                .resize(200, 200)
                .into(profile);

        pic.with(this)
                .load(account.getProfilePhotoUrl())
                .resize(45, 45)
                .into(profile1);
    }
    private void Auth_dropbox(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        String accessToken = Auth.getOAuth2Token(); //generate Access Token

        //Toast.makeText(this, "Auth", Toast.LENGTH_SHORT).show();

        if (accessToken != null) {
            //Store accessToken in SharedPreferences
            editor.putString("access-token", accessToken);
            editor.commit();
            ACCESS_TOKEN = retrieveAccessToken();
        }
        else {
            ACCESS_TOKEN = null;
        }
        getUserAccount();
        download();
    }
    private String retrieveAccessToken() {
        //Toast.makeText(this, "token", Toast.LENGTH_SHORT).show();

        //check if ACCESS_TOKEN is stored on previous app launches
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        String accessToken = preference.getString("access-token", "");
        if (accessToken == null) {

            return null;
        } else {
            //accessToken already exists
            return accessToken;
        }
    }


    private void View3(View view3) {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        SwitchCompat s1 = (SwitchCompat) view3.findViewById(R.id.s1);
        TextView t = (TextView ) view3.findViewById(R.id.tv_settings);
        tv_1 = (TextView ) view3.findViewById(R.id.tv1);
        tv_2 = (TextView ) view3.findViewById(R.id.tv2);
        tv_auto = (TextView ) view3.findViewById(R.id.tv_auto);
        tv_clock = (TextView ) view3.findViewById(R.id.tv_clock);
        tv_about = (TextView ) view3.findViewById(R.id.tv_about_app);
        tv_terms = (TextView ) view3.findViewById(R.id.tv_terms);
        tv_ack = (TextView ) view3.findViewById(R.id.tv_acknowledge);



        tv_1.setTypeface(typeface2);
        tv_2.setTypeface(typeface2);
        t.setTypeface(typeface2);
        tv_about.setTypeface(typeface);
        tv_terms.setTypeface(typeface);
        tv_ack.setTypeface(typeface);
        tv_clock.setTypeface(typeface);
        tv_auto.setTypeface(typeface);

        tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent slideactivity = new Intent(MainActivity.this, About.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

        tv_ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent slideactivity = new Intent(MainActivity.this, library.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });
        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent slideactivity = new Intent(MainActivity.this, Terms.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

        if(preference.getString("sync",null)==null){
            editor.putString("sync", "on");
            editor.commit();
        }

        if(preference.getString("sync",null).equals("off")){
            s1.setChecked(false);
        } else{
            s1.setChecked(true);
        }

        if(s1.isChecked()){
            editor.putString("sync", "on");
            editor.commit();
        }
        else{
            editor.putString("sync", "off");
            editor.commit();
        }
        s1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(!isChecked){
                    editor.putString("sync", "off");
                    editor.commit();
                }
                else{
                    editor.putString("sync", "on");
                    editor.commit();
                }

            }
        });
        tv_clock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               importDB_dropBox();
            }
        });

    }


    private void View2(View vv) {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        TextView t = (TextView ) vv.findViewById(R.id.tv_costs);
        t.setTypeface(typeface2);

        //ll_concat=(LinearLayout) vv.findViewById(R.id.ll_concat);
        mTopNavigationTabStrip2 = (NavigationTabStrip) vv.findViewById(R.id.nts_top);
        //mTopNavigationTabStrip.setTabIndex(0, true);
        mTopNavigationTabStrip2.setTypeface(typeface);

        cost_viewPager = (ViewPager) vv.findViewById(R.id.cost_viewpage);
        cost_bills = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.bills, null, false);
        cost_libs = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.libility, null, false);


        cost_viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {



                if(position == 0){
                    container.addView(cost_bills);
                    return cost_bills;
                }
                else {
                    container.addView(cost_libs);
                    return cost_libs;
                }
            }
        });
        View2_Bill(cost_bills);
        View2_Libs(cost_libs);

        mTopNavigationTabStrip2.setViewPager( cost_viewPager,0);


    }

    private void View2_Libs(final View vv) {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        Fab fab = (Fab) vv.findViewById(R.id.fab_lib);
        View sheetView = vv.findViewById(R.id.fab_sheet);
        View overlay = vv.findViewById(R.id.overlay);
        int sheetColor = Color.rgb(255,255,255);
        int fabColor = Color.rgb(0,0,0);

        // Create material sheet FAB
        materialSheetFab_lib = new MaterialSheetFab<>(fab, sheetView, overlay, sheetColor, fabColor);

        materialSheetFab_lib.showFab();
        // Set material sheet event listener
        materialSheetFab_lib.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {

            }

            @Override
            public void onHideSheet() {

            }
        });
        TextView add_lib = (TextView) vv.findViewById(R.id.add_lib);
        add_lib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialSheetFab_lib.hideSheet();
                Intent slideactivity = new Intent(MainActivity.this, lib_insert.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
                //startActivity(new Intent(getApplicationContext(),lib_insert.class));
            }
        });

        TextView cancle_edit_asset = (TextView) vv.findViewById(R.id.tv_cancel_edit_libs);
        e4 = (ExpandableLayout) vv.findViewById(R.id.expandable_edit_libs);
        final TextView edit_asset = (TextView) vv.findViewById(R.id.edit_lib);

        edit_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_libs","on");
                editor.commit();
                Toast.makeText(MainActivity.this, "Lib Edit Mode is On. Click item to edit", Toast.LENGTH_LONG).show();
                materialSheetFab_lib.hideSheet();
                if(!e4.isExpanded()){
                    e4.expand();
                }
            }
        });

        cancle_edit_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_libs","off");
                editor.commit();
                Toast.makeText(MainActivity.this, "Lib Edit Mode is OFF", Toast.LENGTH_LONG).show();
                if(e4.isExpanded()){
                    e4.collapse();
                }
            }
        });

        //list
        tv_amount_lib = (TextView) vv.findViewById(R.id.total_lib_amount);
        lv_lib = (ListView) vv.findViewById(R.id.lib_list);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        tv_amount_lib.setTypeface(type);



        // for list view in job details

        final SQLiteDatabase db = db_helper.getReadableDatabase();

        final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM lib", null);

// Setup cursor adapter using cursor from last step
        final lib_cursor_adapter todoAdapter = new lib_cursor_adapter(this, todoCursor);
// Attach cursor adapter to the ListView
        lv_lib.setAdapter(todoAdapter);
        //todoCursor.close();

        lv_lib.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView tv_job_name = (TextView) view.findViewById(R.id.tv_asset_name_lv);
                editor.putString("lib_name",tv_job_name.getText().toString());
                editor.commit();

                final TextView ammm = (TextView) view.findViewById(R.id.tv_amount_lv);

                final String a_name1 = tv_job_name.getText().toString();
                final String am = ammm.getText().toString();

                final TextView s_d = (TextView) view.findViewById(R.id.tv_lib_s_date);
                final String ss_d1 = s_d.getText().toString();

                editor.putString("lib_name",tv_job_name.getText().toString());
                editor.commit();
                final String[] tempp = {""};
                final Double[] total = {Double.valueOf(preference.getString("amount_libs",""))};

                if(preference.getString("edit_libs","").equals("on")){
                    Intent slideactivity = new Intent(MainActivity.this, lib_edit.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                    startActivity(slideactivity, bndlanimation);
                }
                else{
                    new SweetAlertDialog(vv.getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Are you sure?")
                            .setContentText("It can delete your item")
                            .setConfirmText("Yes,delete it!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sDialog) {
                                    // reuse previous dialog instance
                                    db_helper.delete_lib(a_name1);

                                    double job_dif = 0;
                                    double bill_dif = 0;
                                    double asset_dif = 0;
                                    double lib_dif = 0;


                                    Calendar c1 = Calendar.getInstance();

                                    int date11 = c1.get(Calendar.DAY_OF_MONTH);
                                    int month11 = c1.get(Calendar.MONTH);
                                    int year11 = c1.get(Calendar.YEAR);

                                    SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                                    String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date


                                    final SQLiteDatabase db = db_helper.getWritableDatabase();
                                    final Cursor cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
                                    final Cursor cursor1 = db.rawQuery("SELECT rowid _id,* FROM bills", null);
                                    final Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM asset", null);
                                    final Cursor cursor3 = db.rawQuery("SELECT rowid _id,* FROM lib", null);


                                    if(cursor.moveToFirst()) {
                                        do {

                                            String s_d = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));
                                            String y = "";
                                            String m = "";
                                            String d1 = "";
                                            int i =0,jj=0;


                                            for(i = 0; i < s_d.length();i++){

                                                if(s_d.charAt(i) == '.'){
                                                    break;
                                                }
                                                d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                            }
                                            for(jj = i+1; jj < s_d.length();jj++){

                                                if(s_d.charAt(jj) == '.'){
                                                    break;
                                                }
                                                m=m.concat(String.valueOf(s_d.charAt(jj)));
                                            }
                                            for (int k = jj+1; k < s_d.length();k++){
                                                y = y.concat(String.valueOf(s_d.charAt(k)));
                                            }


                                            int s_year= Integer.valueOf(y);
                                            int s_month = Integer.valueOf(m);
                                            int s_dd = Integer.valueOf(d1);


                                            String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                            Date date111 = null;
                                            Date date12 = null;



                                            try {

                                                date111 = myFormat.parse(inputString11);
                                                date12 = myFormat.parse(inputString1);
                                                job_dif = job_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                        } while (cursor.moveToNext());
                                    }


                                    if(cursor1.moveToFirst()) {
                                        do {

                                            String s_d = cursor1.getString(cursor.getColumnIndexOrThrow("start_date"));
                                            String y = "";
                                            String m = "";
                                            String d1 = "";
                                            int i =0,jj=0;


                                            for(i = 0; i < s_d.length();i++){

                                                if(s_d.charAt(i) == '.'){
                                                    break;
                                                }
                                                d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                            }
                                            for(jj = i+1; jj < s_d.length();jj++){

                                                if(s_d.charAt(jj) == '.'){
                                                    break;
                                                }
                                                m=m.concat(String.valueOf(s_d.charAt(jj)));
                                            }
                                            for (int k = jj+1; k < s_d.length();k++){
                                                y = y.concat(String.valueOf(s_d.charAt(k)));
                                            }


                                            int s_year= Integer.valueOf(y);
                                            int s_month = Integer.valueOf(m);
                                            int s_dd = Integer.valueOf(d1);


                                            String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                            Date date111 = null;
                                            Date date12 = null;



                                            try {

                                                date111 = myFormat.parse(inputString11);
                                                date12 = myFormat.parse(inputString1);
                                                bill_dif = bill_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                        } while (cursor1.moveToNext());

                                        if(cursor2.moveToFirst()) {
                                            do {

                                                String s_d = cursor2.getString(cursor.getColumnIndexOrThrow("start_date"));
                                                String y = "";
                                                String m = "";
                                                String d1 = "";
                                                int i = 0, jj = 0;


                                                for (i = 0; i < s_d.length(); i++) {

                                                    if (s_d.charAt(i) == '.') {
                                                        break;
                                                    }
                                                    d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                                }
                                                for (jj = i + 1; jj < s_d.length(); jj++) {

                                                    if (s_d.charAt(jj) == '.') {
                                                        break;
                                                    }
                                                    m = m.concat(String.valueOf(s_d.charAt(jj)));
                                                }
                                                for (int k = jj + 1; k < s_d.length(); k++) {
                                                    y = y.concat(String.valueOf(s_d.charAt(k)));
                                                }


                                                int s_year = Integer.valueOf(y);
                                                int s_month = Integer.valueOf(m);
                                                int s_dd = Integer.valueOf(d1);


                                                String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                                Date date111 = null;
                                                Date date12 = null;


                                                try {

                                                    date111 = myFormat.parse(inputString11);
                                                    date12 = myFormat.parse(inputString1);
                                                    asset_dif = asset_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }


                                            } while (cursor2.moveToNext());
                                        }

                                        if(cursor3.moveToFirst()) {
                                            do {

                                                String lib_s_type = cursor2.getString(cursor2.getColumnIndexOrThrow("lib_type"));

                                                if(!lib_s_type.equals("fixed")) {

                                                    String s_d = cursor3.getString(cursor.getColumnIndexOrThrow("start_date"));
                                                    String y = "";
                                                    String m = "";
                                                    String d1 = "";
                                                    int i = 0, jj = 0;


                                                    for (i = 0; i < s_d.length(); i++) {

                                                        if (s_d.charAt(i) == '.') {
                                                            break;
                                                        }
                                                        d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                                    }
                                                    for (jj = i + 1; jj < s_d.length(); jj++) {

                                                        if (s_d.charAt(jj) == '.') {
                                                            break;
                                                        }
                                                        m = m.concat(String.valueOf(s_d.charAt(jj)));
                                                    }
                                                    for (int k = jj + 1; k < s_d.length(); k++) {
                                                        y = y.concat(String.valueOf(s_d.charAt(k)));
                                                    }


                                                    int s_year = Integer.valueOf(y);
                                                    int s_month = Integer.valueOf(m);
                                                    int s_dd = Integer.valueOf(d1);


                                                    String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                                    Date date111 = null;
                                                    Date date12 = null;


                                                    try {

                                                        date111 = myFormat.parse(inputString11);
                                                        date12 = myFormat.parse(inputString1);
                                                        lib_dif = lib_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }




                                            } while (cursor3.moveToNext());
                                        }
                                    }

                                    for(int i = 2 ; i < am.length();i++){
                                        if(am.charAt(i) == '\n'){
                                            break;
                                        }
                                        tempp[0] = tempp[0].concat(String.valueOf(am.charAt(i)));
                                    }
                                    //Toast.makeText(MainActivity.this, "Before "+preference.getString("amount_assets",""), Toast.LENGTH_SHORT).show();


                                    double temp = Double.valueOf(tempp[0]);
                                    total[0] = total[0] -temp;
                                    tv_amount_lib.setText("$ "+String.valueOf(total[0]));
                                    tv_total_lib.setText(String.valueOf(total[0]));

                                    editor.putString("amount_libs",String.format("%.2f", total[0]));
                                    editor.commit();

                                    int ll= 0;
                                    String amount_aaaa="";
                                    String asset_rrr = "";
                                    for(int i = 0; i < am.length(); i++){

                                        if(ll == 2){
                                            asset_rrr = asset_rrr.concat(String.valueOf(am.charAt(i)));
                                        }
                                        if(am.charAt(i) == '/'){
                                            ll = 2;
                                        }
                                        if(ll == 1){
                                            amount_aaaa = amount_aaaa.concat(String.valueOf(am.charAt(i)));
                                        }

                                        if(am.charAt(i) == '\n'){
                                            ll = 1;
                                        }

                                    }
                                    double rate_d = 0.0;

                                    if(asset_rrr.equals("week")){
                                        rate_d = Double.valueOf(amount_aaaa)/7.0;
                                    }
                                    else if(asset_rrr.equals("month")){
                                        rate_d = Double.valueOf(amount_aaaa)/30.0;
                                    }
                                    else if(asset_rrr.equals("year")){
                                        rate_d = Double.valueOf(amount_aaaa)/365.0;
                                    }


                                    //Toast.makeText(getApplicationContext(), "current "+String.valueOf(rate_d), Toast.LENGTH_SHORT).show();


                                    rate_d = Double.valueOf(preference.getString("net_lib_rate",""))-rate_d;

                                    //Toast.makeText(getApplicationContext(), "pre net lib rate  "+Double.valueOf(preference.getString("net_lib_rate","")), Toast.LENGTH_LONG).show();
                                    //Toast.makeText(getApplicationContext(), "net - deleted "+String.valueOf(rate_d), Toast.LENGTH_LONG).show();

                                    if(rate_d < 0.00001){
                                        rate_d = 0;
                                    }
                                    editor.putString("net_lib_rate",String.valueOf(rate_d));
                                    editor.commit();
                                    //db_helper.update_rate_lib(String.valueOf(rate_d));

                                    rate_d =  Double.valueOf(preference.getString("net_job_rate",""))+ Double.valueOf(preference.getString("net_asset_rate",""))
                                            -rate_d - Double.valueOf(preference.getString("net_bill_rate",""));

                                    //Toast.makeText(getApplicationContext(), "total rate "+String.valueOf(rate_d), Toast.LENGTH_LONG).show();

                                    if(rate_d <= 0.00001){
                                        rate_d = 0.0;
                                    }


                                    if(rate_d <= 0){
                                        editor.putString("goal_date","0");
                                        editor.commit();
                                    }else {
                                        double goal = Double.valueOf(preference.getString("goal",""));

                                        double net = Double.valueOf(preference.getString("net",""));

                                        //Toast.makeText(getApplicationContext(), "Goal "+String.valueOf(goal), Toast.LENGTH_LONG).show();
                                        //Toast.makeText(getApplicationContext(), "net "+String.valueOf(net), Toast.LENGTH_LONG).show();

                                        if(start_date_extract(ss_d1)){
                                            net = net + Double.valueOf(preference.getString("lib_"+a_name1,"")) ;
                                        }
                                        //Toast.makeText(getApplicationContext(), "net post "+String.valueOf(net), Toast.LENGTH_LONG).show();

                                        net = goal - net ;

                                        double days_count = net/rate_d;

                                        int dayss = (int) days_count;

                                        //Toast.makeText(getApplicationContext(), "net post1 "+String.valueOf(net), Toast.LENGTH_LONG).show();
                                        //Toast.makeText(getApplicationContext(), "rate_d "+String.valueOf(rate_d), Toast.LENGTH_LONG).show();
                                        //Toast.makeText(getApplicationContext(), "dayss "+String.valueOf(dayss), Toast.LENGTH_LONG).show();


                                        Date date1 = null;
                                        try {

                                            date1 = myFormat.parse(inputString1);

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }


                                        Calendar startCal = Calendar.getInstance();
                                        startCal.setTime(date1);

                                        dayss = (int) (dayss + job_dif + asset_dif - bill_dif - lib_dif);
                                        //Toast.makeText(getApplicationContext(), "dayss  "+String.valueOf(dayss), Toast.LENGTH_LONG).show();
                                        //Toast.makeText(getApplicationContext(), "net "+String.valueOf(net), Toast.LENGTH_LONG).show();

                                        int r = 0;
                                        do {
                                            startCal.add(Calendar.DAY_OF_MONTH, 1);
                                            r++;
                                        } while (r <= dayss); //excluding end date


                                        String dd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                                +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                                +String.valueOf(startCal.get(Calendar.YEAR));

                                        editor.putString("goal_date",dd);
                                        editor.commit();

                                        //db_helper.update_goal_date(dd);

                                    }

                                    editor.remove("lib_"+a_name1);
                                    editor.apply();


                                    final Cursor todoCursor1 = db.rawQuery("SELECT rowid _id,* FROM lib", null);

                                    final lib_cursor_adapter todoAdapter1 = new lib_cursor_adapter(getApplicationContext(), todoCursor1);
                                    lv_lib.setAdapter(todoAdapter1);
                                    //todoCursor1.close();

                                    sDialog.setTitleText("Deleted!")
                                            .setContentText("Your item has been deleted!")
                                            .setConfirmText("OK")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sDialog.dismiss();
                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                }
                            })
                            .show();

                }

            }
        });


    }

    private void View2_Bill(final View vv) {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        Fab fab = (Fab) vv.findViewById(R.id.fab_bill);
        View sheetView = vv.findViewById(R.id.fab_sheet);
        View overlay = vv.findViewById(R.id.overlay);
        int sheetColor = Color.rgb(255,255,255);
        int fabColor = Color.rgb(0,0,0);

        // Create material sheet FAB
        materialSheetFab_bill = new MaterialSheetFab<>(fab, sheetView, overlay, sheetColor, fabColor);

        materialSheetFab_bill.showFab();
        // Set material sheet event listener
        materialSheetFab_bill.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {

            }

            @Override
            public void onHideSheet() {

            }
        });

        TextView add_bill = (TextView) vv.findViewById(R.id.add_bill);
        add_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialSheetFab_bill.hideSheet();
               // startActivity(new Intent(getApplicationContext(),bills_form.class));
                Intent slideactivity = new Intent(MainActivity.this, bills_form.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
            }
        });

        TextView cancle_edit_bill = (TextView) vv.findViewById(R.id.tv_cancel_edit_bills);
        e3= (ExpandableLayout) vv.findViewById(R.id.expandable_edit_bill);
        final TextView edit_bill = (TextView) vv.findViewById(R.id.fab_edit_bills);

        edit_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_bills1","on");
                editor.commit();
                Toast.makeText(MainActivity.this, "Bills Edit Mode is On. Click item to edit", Toast.LENGTH_LONG).show();
                materialSheetFab_bill.hideSheet();
                if(!e3.isExpanded()){
                    e3.expand();
                }
            }
        });

        cancle_edit_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_bills1","off");
                editor.commit();
                Toast.makeText(MainActivity.this, "Bills Edit Mode is OFF", Toast.LENGTH_LONG).show();
                if(e3.isExpanded()){
                    e3.collapse();
                }
            }
        });

        //list

        tv_amount_bill = (TextView) vv.findViewById(R.id.total_bill_amount);

        lv_bill = (ListView) vv.findViewById(R.id.bill_list);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        tv_amount_bill.setTypeface(type);


        // for list view in job details

        final SQLiteDatabase db = db_helper.getReadableDatabase();

        final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM bills", null);

// Setup cursor adapter using cursor from last step
        final bills_cursor_adapter todoAdapter = new bills_cursor_adapter(this, todoCursor);
// Attach cursor adapter to the ListView
        lv_bill.setAdapter(todoAdapter);
        //todoCursor.close();

        lv_bill.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //String name = (String) adapterView.getItemAtPosition(i);
                TextView tv_job_name = (TextView) view.findViewById(R.id.tv_bills_name_lv);
                editor.putString("bills_name",tv_job_name.getText().toString());
                editor.commit();

                if(preference.getString("edit_bills1","").equals("on")){
                    editor.putString("edit_bills","on");
                    editor.commit();
                    Intent slideactivity = new Intent(MainActivity.this, bills_form.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                    startActivity(slideactivity, bndlanimation);
                }
                else {
                    Intent slideactivity = new Intent(MainActivity.this, bills_details.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                    startActivity(slideactivity, bndlanimation);
                }

                /*Intent intent = new Intent(getApplicationContext(),bills_details.class);
                startActivity(intent);*/

            }
        });

    }


    private void View1(View vv){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        TextView t = (TextView ) vv.findViewById(R.id.tv_income);
        t.setTypeface(typeface2);

        //ll_concat=(LinearLayout) vv.findViewById(R.id.ll_concat);
        mTopNavigationTabStrip = (NavigationTabStrip) vv.findViewById(R.id.nts_top);
        //mTopNavigationTabStrip.setTabIndex(0, true);
        mTopNavigationTabStrip.setTypeface(typeface);

        income_viewPager = (ViewPager) vv.findViewById(R.id.income_viewpage);
        income_jobs = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.jobs, null, false);
        income_assets = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.assets, null, false);

        income_viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {

                if(position == 0){
                        container.addView(income_jobs);
                        return income_jobs;
                }
                else {
                    container.addView(income_assets);
                    return income_assets;
                }
            }
        });
        View1_Job(income_jobs);
        View1_asset(income_assets);

        mTopNavigationTabStrip.setViewPager(income_viewPager,0);


    }

    private void View1_Job(final View vv) {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        Fab fab = (Fab) vv.findViewById(R.id.fab_job);
        View sheetView = vv.findViewById(R.id.fab_sheet);
        View overlay = vv.findViewById(R.id.overlay);
        int sheetColor = Color.rgb(255,255,255);
        int fabColor = Color.rgb(0,0,0);

        // Create material sheet FAB
        materialSheetFab_job = new MaterialSheetFab<>(fab, sheetView, overlay, sheetColor, fabColor);

        materialSheetFab_job.showFab();
        // Set material sheet event listener
        materialSheetFab_job.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {

            }

            @Override
            public void onHideSheet() {

            }
        });

        TextView add_job = (TextView) vv.findViewById(R.id.add_job);
        add_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialSheetFab_job.hideSheet();
                Intent slideactivity = new Intent(MainActivity.this, job_form1.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
                //startActivity(new Intent(getApplicationContext(),job_form1.class));
            }
        });


        TextView cancle_edit_job = (TextView) vv.findViewById(R.id.tv_cancel_edit_jobs);
        e1 = (ExpandableLayout) vv.findViewById(R.id.expandable_edit_job);
        final TextView edit_job = (TextView) vv.findViewById(R.id.edit_job);

        edit_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_jobs","on");
                editor.commit();
                Toast.makeText(MainActivity.this, "Job Edit Mode is On. Click item to edit", Toast.LENGTH_LONG).show();
                materialSheetFab_job.hideSheet();
                if(!e1.isExpanded()){
                    e1.expand();
                }
            }
        });

        cancle_edit_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_jobs","off");
                editor.commit();
                Toast.makeText(MainActivity.this, "Job Edit Mode is OFF", Toast.LENGTH_LONG).show();
                if(e1.isExpanded()){
                    e1.collapse();
                }
            }
        });

        //for list view

        lv_job = (ListView) vv.findViewById(R.id.job_list);
        tv_amount_job = (TextView) vv.findViewById(R.id.total_job_amount);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        tv_amount_job.setTypeface(type);


        final SQLiteDatabase db = db_helper.getReadableDatabase();

        final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM job", null);

        final TodoCursorAdapter todoAdapter = new TodoCursorAdapter(this, todoCursor);
        lv_job.setAdapter(todoAdapter);
        //todoCursor.close();

        lv_job.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView tv_job_name = (TextView) view.findViewById(R.id.tv_job_name_lv);
                editor.putString("job_name",tv_job_name.getText().toString());
                editor.commit();

                if(preference.getString("edit_jobs","").equals("on")){
                    editor.putString("edit","on");
                    editor.commit();
                    Intent slideactivity = new Intent(MainActivity.this, hungers.com.emroze.digits.job.edit_job.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                    startActivity(slideactivity, bndlanimation);
                }
                else {
                    Intent slideactivity = new Intent(MainActivity.this, display_jobs.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                    startActivity(slideactivity, bndlanimation);
                }

                /*Intent intent = new Intent(getApplicationContext(),display_jobs.class);
                startActivity(intent);*/

            }
        });


    }

    private void View1_asset(final View vv) {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        Fab fab = (Fab) vv.findViewById(R.id.fab_asset);
        View sheetView = vv.findViewById(R.id.fab_sheet);
        View overlay = vv.findViewById(R.id.overlay);
        int sheetColor = Color.rgb(255,255,255);
        int fabColor = Color.rgb(0,0,0);

        // Create material sheet FAB
        materialSheetFab_asset = new MaterialSheetFab<>(fab, sheetView, overlay, sheetColor, fabColor);

        materialSheetFab_asset.showFab();
        // Set material sheet event listener
        materialSheetFab_asset.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {

            }
            @Override
            public void onHideSheet() {

            }
        });
        TextView add_asset = (TextView) vv.findViewById(R.id.add_asset);
        add_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialSheetFab_asset.hideSheet();
                Intent slideactivity = new Intent(MainActivity.this, asset_insert.class);
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                startActivity(slideactivity, bndlanimation);
                //startActivity(new Intent(getApplicationContext(),asset_insert.class));
            }
        });

        TextView cancle_edit_asset = (TextView) vv.findViewById(R.id.tv_cancel_edit_assets);
        e2 = (ExpandableLayout) vv.findViewById(R.id.expandable_edit_assets);
        final TextView edit_asset = (TextView) vv.findViewById(R.id.edit_asset);

        edit_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_assets","on");
                editor.commit();
                Toast.makeText(MainActivity.this, "Asset Edit Mode is On. Click item to edit", Toast.LENGTH_LONG).show();
                materialSheetFab_asset.hideSheet();
                if(!e2.isExpanded()){
                    e2.expand();
                }
            }
        });

        cancle_edit_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("edit_assets","off");
                editor.commit();
                Toast.makeText(MainActivity.this, "Asset Edit Mode is OFF", Toast.LENGTH_LONG).show();
                if(e2.isExpanded()){
                    e2.collapse();
                }
            }
        });

        //asset list
        lv_asset = (ListView) vv.findViewById(R.id.asset_list);

        tv_amount_asset = (TextView) vv.findViewById(R.id.total_assets_amount);

        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/canaro_extra_bold.otf");
        tv_amount_asset.setTypeface(type);



        // for list view in job details

        final SQLiteDatabase db = db_helper.getReadableDatabase();

        final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM asset", null);

// Setup cursor adapter using cursor from last step
        final asset_cursor_adapter todoAdapter = new asset_cursor_adapter(this, todoCursor);
// Attach cursor adapter to the ListView
        lv_asset.setAdapter(todoAdapter);
        //todoCursor.close();

        lv_asset.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //String name = (String) adapterView.getItemAtPosition(i);


                final TextView tv_job_name = (TextView) view.findViewById(R.id.tv_asset_name_lv);

                editor.putString("asset_name",tv_job_name.getText().toString());
                editor.commit();

                final TextView ss_d = (TextView) view.findViewById(R.id.tv_asset_s_date);
                final String ss_date = ss_d.getText().toString();

                final TextView ammm = (TextView) view.findViewById(R.id.tv_amount_lv);
                final String am = ammm.getText().toString();

                final String a_name = tv_job_name.getText().toString();
                final String[] tempp = {""};
                final double[] total = {Double.valueOf(preference.getString("amount_assets",""))};

                if(preference.getString("edit_assets","").equals("on")){
                    Intent slideactivity = new Intent(MainActivity.this, asset_edit.class);
                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation,R.anim.animation2).toBundle();
                    startActivity(slideactivity, bndlanimation);
                }
                else {
                    new SweetAlertDialog(vv.getContext(), SweetAlertDialog.WARNING_TYPE)

                            .setTitleText("Are you sure?")
                            .setContentText("It can delete your item")
                            .setConfirmText("Yes,delete it!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sDialog) {
                                    // reuse previous dialog instance
                                    //Toast.makeText(getApplicationContext(), "net "+preference.getString("net",""), Toast.LENGTH_LONG).show();

                                    db_helper.delete_asset(a_name);

                                    double job_dif = 0;
                                    double bill_dif = 0;
                                    double asset_dif = 0;
                                    double lib_dif = 0;


                                    Calendar c1 = Calendar.getInstance();

                                    int date11 = c1.get(Calendar.DAY_OF_MONTH);
                                    int month11 = c1.get(Calendar.MONTH);
                                    int year11 = c1.get(Calendar.YEAR);

                                    SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                                    String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date


                                    final SQLiteDatabase db = db_helper.getWritableDatabase();
                                    final Cursor cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
                                    final Cursor cursor1 = db.rawQuery("SELECT rowid _id,* FROM bills", null);
                                    final Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM asset", null);
                                    final Cursor cursor3 = db.rawQuery("SELECT rowid _id,* FROM lib", null);


                                    if(cursor.moveToFirst()) {
                                        do {

                                            String s_d = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));
                                            String y = "";
                                            String m = "";
                                            String d1 = "";
                                            int i =0,jj=0;


                                            for(i = 0; i < s_d.length();i++){

                                                if(s_d.charAt(i) == '.'){
                                                    break;
                                                }
                                                d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                            }
                                            for(jj = i+1; jj < s_d.length();jj++){

                                                if(s_d.charAt(jj) == '.'){
                                                    break;
                                                }
                                                m=m.concat(String.valueOf(s_d.charAt(jj)));
                                            }
                                            for (int k = jj+1; k < s_d.length();k++){
                                                y = y.concat(String.valueOf(s_d.charAt(k)));
                                            }


                                            int s_year= Integer.valueOf(y);
                                            int s_month = Integer.valueOf(m);
                                            int s_dd = Integer.valueOf(d1);


                                            String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                            Date date111 = null;
                                            Date date12 = null;



                                            try {

                                                date111 = myFormat.parse(inputString11);
                                                date12 = myFormat.parse(inputString1);
                                                job_dif = job_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                        } while (cursor.moveToNext());
                                    }


                                    if(cursor1.moveToFirst()) {
                                        do {

                                            String s_d = cursor1.getString(cursor.getColumnIndexOrThrow("start_date"));
                                            String y = "";
                                            String m = "";
                                            String d1 = "";
                                            int i =0,jj=0;


                                            for(i = 0; i < s_d.length();i++){

                                                if(s_d.charAt(i) == '.'){
                                                    break;
                                                }
                                                d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                            }
                                            for(jj = i+1; jj < s_d.length();jj++){

                                                if(s_d.charAt(jj) == '.'){
                                                    break;
                                                }
                                                m=m.concat(String.valueOf(s_d.charAt(jj)));
                                            }
                                            for (int k = jj+1; k < s_d.length();k++){
                                                y = y.concat(String.valueOf(s_d.charAt(k)));
                                            }


                                            int s_year= Integer.valueOf(y);
                                            int s_month = Integer.valueOf(m);
                                            int s_dd = Integer.valueOf(d1);


                                            String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                            Date date111 = null;
                                            Date date12 = null;



                                            try {

                                                date111 = myFormat.parse(inputString11);
                                                date12 = myFormat.parse(inputString1);
                                                bill_dif = bill_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                        } while (cursor1.moveToNext());

                                        if(cursor2.moveToFirst()) {
                                            do {

                                                String asset1_s_type = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_type"));

                                                if(!asset1_s_type.equals("fixed")){
                                                    String s_d = cursor2.getString(cursor.getColumnIndexOrThrow("start_date"));
                                                    String y = "";
                                                    String m = "";
                                                    String d1 = "";
                                                    int i = 0, jj = 0;


                                                    for (i = 0; i < s_d.length(); i++) {

                                                        if (s_d.charAt(i) == '.') {
                                                            break;
                                                        }
                                                        d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                                    }
                                                    for (jj = i + 1; jj < s_d.length(); jj++) {

                                                        if (s_d.charAt(jj) == '.') {
                                                            break;
                                                        }
                                                        m = m.concat(String.valueOf(s_d.charAt(jj)));
                                                    }
                                                    for (int k = jj + 1; k < s_d.length(); k++) {
                                                        y = y.concat(String.valueOf(s_d.charAt(k)));
                                                    }


                                                    int s_year = Integer.valueOf(y);
                                                    int s_month = Integer.valueOf(m);
                                                    int s_dd = Integer.valueOf(d1);


                                                    String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                                    Date date111 = null;
                                                    Date date12 = null;


                                                    try {

                                                        date111 = myFormat.parse(inputString11);
                                                        date12 = myFormat.parse(inputString1);
                                                        asset_dif = asset_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                            } while (cursor2.moveToNext());
                                        }

                                        if(cursor3.moveToFirst()) {
                                            do {

                                                String s_d = cursor3.getString(cursor.getColumnIndexOrThrow("start_date"));
                                                String y = "";
                                                String m = "";
                                                String d1 = "";
                                                int i = 0, jj = 0;


                                                for (i = 0; i < s_d.length(); i++) {

                                                    if (s_d.charAt(i) == '.') {
                                                        break;
                                                    }
                                                    d1 = d1.concat(String.valueOf(s_d.charAt(i)));
                                                }
                                                for (jj = i + 1; jj < s_d.length(); jj++) {

                                                    if (s_d.charAt(jj) == '.') {
                                                        break;
                                                    }
                                                    m = m.concat(String.valueOf(s_d.charAt(jj)));
                                                }
                                                for (int k = jj + 1; k < s_d.length(); k++) {
                                                    y = y.concat(String.valueOf(s_d.charAt(k)));
                                                }


                                                int s_year = Integer.valueOf(y);
                                                int s_month = Integer.valueOf(m);
                                                int s_dd = Integer.valueOf(d1);


                                                String inputString11 = String.valueOf(s_dd) + " " + String.valueOf(s_month) + " " + String.valueOf(s_year); //start date

                                                Date date111 = null;
                                                Date date12 = null;


                                                try {

                                                    date111 = myFormat.parse(inputString11);
                                                    date12 = myFormat.parse(inputString1);
                                                    lib_dif = lib_dif + getWorkingDaysBetweenTwoDates1(date111, date12);

                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }


                                            } while (cursor3.moveToNext());
                                        }
                                    }

                                    for(int i = 2 ; i < am.length();i++){
                                        if(am.charAt(i) == '\n'){
                                            break;
                                        }
                                        tempp[0] = tempp[0].concat(String.valueOf(am.charAt(i)));
                                    }
                                    //Toast.makeText(MainActivity.this, "Before "+preference.getString("amount_assets",""), Toast.LENGTH_SHORT).show();
                                    double temp = Double.valueOf(tempp[0]);
                                    total[0] = total[0] - temp;
                                    tv_amount_asset.setText("$ "+String.valueOf(total[0]));
                                    tv_total_asset.setText(String.valueOf(total[0]));

                                    editor.putString("amount_assets",String.format("%.2f", total[0]));
                                    editor.commit();

                                    int ll= 0;
                                    String amount_aaaa="";
                                    String asset_rrr = "";
                                    for(int i = 0; i < am.length(); i++){

                                        if(ll == 2){
                                            asset_rrr = asset_rrr.concat(String.valueOf(am.charAt(i)));
                                        }
                                        if(am.charAt(i) == '/'){
                                            ll = 2;
                                        }
                                        if(ll == 1){
                                            amount_aaaa = amount_aaaa.concat(String.valueOf(am.charAt(i)));
                                        }

                                        if(am.charAt(i) == '\n'){
                                            ll = 1;
                                        }

                                    }
                                    double rate_d = 0.0;

                                    if(asset_rrr.equals("week")){
                                        rate_d = Double.valueOf(amount_aaaa)/7.0;
                                    }
                                    else if(asset_rrr.equals("month")){
                                        rate_d = Double.valueOf(amount_aaaa)/30.0;
                                    }
                                    else if(asset_rrr.equals("year")){
                                        rate_d = Double.valueOf(amount_aaaa)/365.0;
                                    }


                                    //Toast.makeText(getApplicationContext(), "current "+String.valueOf(rate_d), Toast.LENGTH_SHORT).show();


                                    rate_d = Double.valueOf(preference.getString("net_asset_rate",""))-rate_d;
                                    if(rate_d < 0.00001){
                                        rate_d = 0;
                                    }

                                    //Toast.makeText(getApplicationContext(), "pre net asset rate  "+Double.valueOf(preference.getString("net_asset_rate","")), Toast.LENGTH_LONG).show();
                                    //Toast.makeText(getApplicationContext(), "net - deleted "+String.valueOf(rate_d), Toast.LENGTH_LONG).show();

                                    editor.putString("net_asset_rate",String.valueOf(rate_d));
                                    editor.commit();
                                    //db_helper.update_rate_asset(String.valueOf(rate_d));

                                    rate_d = rate_d + Double.valueOf(preference.getString("net_job_rate",""))
                                            - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

                                    //Toast.makeText(getApplicationContext(), "total rate "+String.valueOf(rate_d), Toast.LENGTH_LONG).show();

                                    if(rate_d <= 0.00001){
                                        rate_d = 0.0;
                                    }


                                    if(rate_d <= 0){
                                        editor.putString("goal_date","0");
                                        editor.commit();
                                    }else {
                                        double goal = Double.valueOf(preference.getString("goal",""));

                                        double net = Double.valueOf(preference.getString("net",""));

                                        //Toast.makeText(getApplicationContext(), "Goal "+String.valueOf(goal), Toast.LENGTH_LONG).show();
                                        //Toast.makeText(getApplicationContext(), "net "+String.valueOf(net), Toast.LENGTH_LONG).show();


                                        if(start_date_extract(ss_date)){
                                            net = net - Double.valueOf(preference.getString("asset_"+a_name,"")) ;
                                        }


                                        //Toast.makeText(getApplicationContext(), "net post "+String.valueOf(net), Toast.LENGTH_LONG).show();

                                        net = goal - net ;

                                        double days_count = net/rate_d;

                                        int dayss = (int) days_count;

                                        //Toast.makeText(getApplicationContext(), "net post1 "+String.valueOf(net), Toast.LENGTH_LONG).show();
                                        //Toast.makeText(getApplicationContext(), "rate_d "+String.valueOf(rate_d), Toast.LENGTH_LONG).show();
                                        //Toast.makeText(getApplicationContext(), "dayss "+String.valueOf(dayss), Toast.LENGTH_LONG).show();

                                        Date date1 = null;
                                        try {

                                            date1 = myFormat.parse(inputString1);

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }


                                        Calendar startCal = Calendar.getInstance();
                                        startCal.setTime(date1);

                                        dayss = (int) (dayss + job_dif + asset_dif - bill_dif - lib_dif);

                                        //Toast.makeText(getApplicationContext(), "dayss2  "+String.valueOf(dayss), Toast.LENGTH_SHORT).show();

                                        int r = 0;
                                        do {
                                            startCal.add(Calendar.DAY_OF_MONTH, 1);
                                            r++;
                                        } while (r <= dayss); //excluding end date


                                        String dd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                                +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                                +String.valueOf(startCal.get(Calendar.YEAR));

                                        editor.putString("goal_date",dd);
                                        editor.commit();

                                        //db_helper.update_goal_date(dd);
                                    }


                                    editor.putString("amount_assets",String.valueOf(total[0]));
                                    editor.commit();



                                    //asset_goal_date_delete_handler g = new asset_goal_date_delete_handler();
                                    //g.asset_goal_timer(getApplicationContext(),temp);

                                    //Toast.makeText(MainActivity.this, "After "+String.valueOf(temp), Toast.LENGTH_SHORT).show();

                                    editor.remove("asset_"+a_name);
                                    editor.apply();
                                    //recreate the list view
                                    final Cursor todoCursor1 = db.rawQuery("SELECT rowid _id,* FROM asset", null);

                                    final asset_cursor_adapter todoAdapter1 = new asset_cursor_adapter(getApplicationContext(), todoCursor1);
                                    lv_asset.setAdapter(todoAdapter1);
                                    // todoCursor1.close();

                                   // Toast.makeText(MainActivity.this, "After "+preference.getString("net",""), Toast.LENGTH_SHORT).show();

                                    sDialog.setTitleText("Deleted!")
                                            .setContentText("Your item has been deleted!")
                                            .setConfirmText("OK")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sDialog.dismiss();
                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                }
                            })
                            .show();
                }



                /*Intent intent = new Intent(getApplicationContext(),display_jobs.class);
                startActivity(intent);*/

            }
        });

        lv_asset.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                return false;
            }
        });


    }

    private void calc_ttotal(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        final SQLiteDatabase db = db_helper.getWritableDatabase();
        final Cursor cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
        final Cursor cursor1 = db.rawQuery("SELECT rowid _id,* FROM bills", null);
        final Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM asset", null);
        final Cursor cursor3 = db.rawQuery("SELECT rowid _id,* FROM lib", null);

        salary_rate s = new salary_rate();
        bills_rate b = new bills_rate();

        String net_amount = "";

        salary_lack_calculation calc = new salary_lack_calculation();
        bills_lack_calculation calc1 = new bills_lack_calculation();

        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int sec = c.get(Calendar.SECOND);
        int date = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        int day_of_week = c.get(Calendar.DAY_OF_WEEK);

        int jobi = 0;

        if(cursor.moveToFirst()){
            do{

                job_name = cursor.getString(cursor.getColumnIndexOrThrow("job_name")).toString();
                s_time = cursor.getString(cursor.getColumnIndexOrThrow("start_time"));
                e_time = cursor.getString(cursor.getColumnIndexOrThrow("end_time"));
                off_day = cursor.getString(cursor.getColumnIndexOrThrow("off_day"));
                amount = cursor.getString(cursor.getColumnIndexOrThrow("salary_amount"));
                s_type = cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));
                s_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));


                net_amount = cursor.getString(cursor.getColumnIndexOrThrow("net_amount"));
                start_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));

                if(start_date_extract(start_date)){
                    boolean sun,mon,tue,wed,thu,fri,sat,valid;

                    String off = off_day;

                    sun=false; //1
                    mon=false; //2
                    tue=false; //3
                    wed=false; //4
                    thu=false; //5
                    fri=false; //6
                    sat=false; //7
                    valid = true;

                    try{
                        if(off.equals("0")){

                        }
                    }
                    catch (NullPointerException e){
                        off="0";
                    }

                    if(off.equals("0") || off == null){
                        sun=false;
                        mon=false;
                        tue=false;
                        wed=false;
                        thu=false;
                        fri=false;
                        sat=false;
                    }
                    else {
                        for (int i =0; i < off.length();i++){
                            if(off.charAt(i) == '1'){
                                sun = true;
                            }
                            else if(off.charAt(i) == '2'){
                                mon = true;
                            }
                            else if(off.charAt(i) == '3'){
                                tue = true;
                            }
                            else if(off.charAt(i) == '4'){
                                wed = true;
                            }
                            else if(off.charAt(i) == '5'){
                                thu = true;
                            }
                            else if(off.charAt(i) == '6'){
                                fri = true;
                            }
                            else if(off.charAt(i) == '7'){
                                sat = true;
                            }
                        }
                    }

                    if(sun == true){
                        if(day_of_week == 1){
                            valid = false;
                        }
                    }
                    if(mon == true){
                        if(day_of_week == 2){
                            valid = false;
                        }
                    }
                    if(tue == true){
                        if(day_of_week == 3){
                            valid = false;
                        }
                    }
                    if(wed == true){
                        if(day_of_week == 4){
                            valid = false;
                        }
                    }
                    if(thu == true){
                        if(day_of_week == 5){
                            valid = false;
                        }
                    }
                    if(fri == true){
                        if(day_of_week == 6){
                            valid = false;
                        }
                    }
                    if(sat == true){
                        if(day_of_week == 7){
                            valid = false;
                        }
                    }

                    if(valid){
                        float start_h = s.hour(s_time);
                        float start_m = s.min(s_time);
                        float end_h = s.hour(e_time);
                        float end_m = s.min(e_time);



                        Double l_salary_sec ;
                        l_salary_sec = calc.calc(this,s_time,e_time,sec,min,hour,date,month,year,off_day);

                        Double rrr = s.calulate_salary_rate(s_time,e_time,amount,s_type,off_day);

                        double salary_lack =  (l_salary_sec*rrr);

                        //Toast.makeText(this, "time lack "+String.valueOf(l_salary_sec), Toast.LENGTH_SHORT).show();

                        String temp_sal = preference.getString(job_name,"");

                        //Toast.makeText(this, "temp_salary "+String.valueOf(temp_sal), Toast.LENGTH_SHORT).show();
                        double net_sal;

                        try{
                            net_sal = salary_lack + Double.valueOf(temp_sal);

                        }catch (NumberFormatException e){
                            net_sal = 0.0;
                        }

                        //Toast.makeText(this, "net_sal "+String.valueOf(net_sal), Toast.LENGTH_SHORT).show();

                        editor.putString(job_name,String.valueOf(net_sal));

                        editor.commit();

                        db_helper.update_jobs_net_amount(job_name,String.valueOf(net_sal));

                    }
                }



                    /*handler h = new handler();

                    h.job_timer(getApplicationContext(),
                            job_name,
                            s_time,
                            e_time,
                            off_day,
                            amount,
                            s_type,
                            start_date
                    );*/

            }
            while(cursor.moveToNext());
        }
        //cursor.close();

        Calendar cc = Calendar.getInstance();

        int hour1 = cc.get(Calendar.HOUR_OF_DAY);
        int min1= cc.get(Calendar.MINUTE);
        int sec1 = cc.get(Calendar.SECOND);
        int date1 = cc.get(Calendar.DAY_OF_MONTH);
        int month1 = cc.get(Calendar.MONTH);
        int year1 = cc.get(Calendar.YEAR);

        if(cursor1.moveToFirst()){
            do{

                bills_name = cursor1.getString(cursor1.getColumnIndexOrThrow("bills_name")).toString();
                bills_amount = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_amount"));
                bills_s_type = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_type"));
                bills_s_time = cursor1.getString(cursor1.getColumnIndexOrThrow("start_time"));
                bills_e_time = cursor1.getString(cursor1.getColumnIndexOrThrow("end_time"));
                bills_s_date = cursor1.getString(cursor1.getColumnIndexOrThrow("start_date"));
                bills_off_day = cursor1.getString(cursor1.getColumnIndexOrThrow("off_day"));
                bills_start_date = cursor1.getString(cursor1.getColumnIndexOrThrow("start_date"));


                if(start_date_extract(bills_start_date)){
                    boolean sun,mon,tue,wed,thu,fri,sat,valid;

                    String off = off_day;

                    sun=false; //1
                    mon=false; //2
                    tue=false; //3
                    wed=false; //4
                    thu=false; //5
                    fri=false; //6
                    sat=false; //7
                    valid = true;

                    try{
                        if(off.equals("0")){

                        }
                    }
                    catch (NullPointerException e){
                        off="0";
                    }

                    if(off.equals("0")){
                        sun=false;
                        mon=false;
                        tue=false;
                        wed=false;
                        thu=false;
                        fri=false;
                        sat=false;
                    }
                    else {
                        for (int i =0; i < off.length();i++){
                            if(off.charAt(i) == '1'){
                                sun = true;
                            }
                            else if(off.charAt(i) == '2'){
                                mon = true;
                            }
                            else if(off.charAt(i) == '3'){
                                tue = true;
                            }
                            else if(off.charAt(i) == '4'){
                                wed = true;
                            }
                            else if(off.charAt(i) == '5'){
                                thu = true;
                            }
                            else if(off.charAt(i) == '6'){
                                fri = true;
                            }
                            else if(off.charAt(i) == '7'){
                                sat = true;
                            }
                        }
                    }

                    if(sun == true){
                        if(day_of_week == 1){
                            valid = false;
                        }
                    }
                    if(mon == true){
                        if(day_of_week == 2){
                            valid = false;
                        }
                    }
                    if(tue == true){
                        if(day_of_week == 3){
                            valid = false;
                        }
                    }
                    if(wed == true){
                        if(day_of_week == 4){
                            valid = false;
                        }
                    }
                    if(thu == true){
                        if(day_of_week == 5){
                            valid = false;
                        }
                    }
                    if(fri == true){
                        if(day_of_week == 6){
                            valid = false;
                        }
                    }
                    if(sat == true){
                        if(day_of_week == 7){
                            valid = false;
                        }
                    }

                    if(valid){
                        float start_h = b.hour(bills_s_time);
                        float start_m = b.min(bills_s_time);
                        float end_h = b.hour(bills_e_time);
                        float end_m = b.min(bills_e_time);



                        Double l_salary_sec ;
                        l_salary_sec = calc1.calc(this,bills_s_time,bills_e_time,sec1,min1,hour1,date1,month1,year1,bills_off_day);

                        Double rrr = b.calulate_bill_rate(bills_s_time,bills_e_time,bills_amount,bills_s_type,bills_off_day);

                        float salary_lack = (float) (l_salary_sec*rrr);

                        //Toast.makeText(this, "time lack "+String.valueOf(l_salary_sec), Toast.LENGTH_SHORT).show();

                        String temp_sal = preference.getString("bill_"+bills_name,"");

                        //Toast.makeText(this, "temp_salary "+String.valueOf(temp_sal), Toast.LENGTH_SHORT).show();

                        float net_sal = salary_lack + Float.valueOf(temp_sal);

                        //Toast.makeText(this, "net_sal "+String.valueOf(net_sal), Toast.LENGTH_SHORT).show();

                        editor.putString("bill_"+bills_name,String.valueOf(net_sal));

                        editor.commit();

                        db_helper.update_bills_net_amount(bills_name,String.valueOf(net_sal));

                    }
                }



                    /*bills_handler bh = new bills_handler();

                    bh.job_timer(getApplicationContext(),
                            bills_name,
                            bills_s_time,
                            bills_e_time,
                            bills_off_day,
                            bills_amount,
                            bills_s_type,
                            bills_start_date
                    );*/

            }
            while(cursor1.moveToNext());
        }
        //cursor1.close();

        if(cursor2.moveToFirst()){
            do{

                String asset1_name = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_name")).toString();
                String asset1_amount = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_amount"));
                String asset1_s_type = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_type"));
                String asset1_s_date = cursor2.getString(cursor2.getColumnIndexOrThrow("start_date"));
                String asset1_net_amount = cursor2.getString(cursor2.getColumnIndexOrThrow("net_amount"));


                asset_lack_calculation a =  new asset_lack_calculation();

                double lack = a.calc_lack(getApplicationContext(),asset1_s_date,asset1_amount,asset1_s_type);

                db_helper.update_asset_net_amount(asset1_name,String.valueOf(lack));

                editor.putString("asset_"+asset1_name,String.valueOf(lack));
                editor.commit();

                    /*asset_handler hand = new asset_handler();

                    hand.asset_handle(
                            getApplicationContext(),
                            asset1_name,
                            asset1_amount,
                            asset1_s_date,
                            asset1_s_type);*/

            }
            while(cursor2.moveToNext());
        }

        if(cursor3.moveToFirst()){
            do{

                String lib1_name = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_name")).toString();
                String lib1_amount = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_amount"));
                String lib1_s_type = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_type"));
                String lib1_s_date = cursor3.getString(cursor3.getColumnIndexOrThrow("start_date"));
                String asset1_net_amount = cursor3.getString(cursor3.getColumnIndexOrThrow("net_amount"));


                asset_lack_calculation a =  new asset_lack_calculation();

                double lack = a.calc_lack(getApplicationContext(),lib1_s_date,lib1_amount,lib1_s_type);

                db_helper.update_lib_net_amount(lib1_name,String.valueOf(lack));

                editor.putString("lib_"+lib1_name,String.valueOf(lack));
                editor.commit();

                lib_handler hand = new lib_handler();

                   /* hand.lib_handler(
                            getApplicationContext(),
                            lib1_name,
                            lib1_amount,
                            lib1_s_date,
                            lib1_s_type);*/

            }
            while(cursor3.moveToNext());
        }
    }

    public void view0(final View vv){

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();


        if(preference.getString("trigger","").equals("on") || preference.getString("restart","").equals("on")){
            //Toast.makeText(this, "here", Toast.LENGTH_SHORT).show();
            calc_ttotal();
        }

        //analog clock
        CustomAnalogClock customAnalogClock = (CustomAnalogClock) view0.findViewById(R.id.analog_clock);
        customAnalogClock.init(this, R.drawable.body1, R.drawable.hour, R.drawable.min, 0, false, false);
        customAnalogClock.setAutoUpdate(true);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        TextView t = (TextView ) view0.findViewById(R.id.tv_digit);
        TextView t1 = (TextView ) view0.findViewById(R.id.tv1);
        TextView t2= (TextView ) view0.findViewById(R.id.tv2);
        TextView t3 = (TextView ) view0.findViewById(R.id.tv3);
        TextView t4 = (TextView ) view0.findViewById(R.id.tv4);
        TextView t5 = (TextView ) view0.findViewById(R.id.tv5);

        tv_total_asset = (TextView) view0.findViewById(R.id.tv_total_asset);
        tv_total_job = (TextView) view0.findViewById(R.id.tv_total_job);
        tv_total_bill = (TextView) view0.findViewById(R.id.tv_total_bill);
        tv_total_lib = (TextView) view0.findViewById(R.id.tv_total_lib);
        tv_goal = (TextView) view0.findViewById(R.id.tv_goal);
        tv_net = (TextView)view0.findViewById(R.id.tv_net);
        tv_time = (TextView)view0.findViewById(R.id.tv_required_time);


        t.setTypeface(typeface2);
        tv_total_asset.setTypeface(typeface2);
        tv_total_job.setTypeface(typeface2);
        tv_total_bill.setTypeface(typeface2);
        tv_total_lib.setTypeface(typeface2);
        tv_goal.setTypeface(typeface2);
        t1.setTypeface(typeface);
        t2.setTypeface(typeface);
        t3.setTypeface(typeface);
        t4.setTypeface(typeface);
        t5.setTypeface(typeface);
        tv_net.setTypeface(typeface2);
        tv_time.setTypeface(typeface);

        //rotating text view
        /*RotatingTextWrapper rotatingTextWrapper = (RotatingTextWrapper) view0.findViewById(R.id.custom_switcher);
        rotatingTextWrapper.setSize(30);
        rotatingTextWrapper.setTypeface(typeface);

        Rotatable rotatable = new Rotatable(Color.parseColor("#FFFFFF"), 1000, "Assets", "Jobs", "Bills","Libilities","can","manage","easily","< Digits >","");
        rotatable.setSize(30);
        rotatable.setCenter(true);
        rotatable.setAnimationDuration(700);
        rotatable.setTypeface(typeface);
        rotatable.setInterpolator(new BounceInterpolator());

        rotatingTextWrapper.setContent("?", rotatable);*/

        //clock
        /*mClock = (ClockView) view0.findViewById(R.id.clock);
        LayoutInflater inflater = LayoutInflater.from(this);
        inflater.inflate(R.layout.clock,mClock);
        mClock.resume();*/

        //summary
        home = (RelativeLayout) view0.findViewById(R.id.home_layout);
        btn = (info.hoang8f.widget.FButton) view0.findViewById(R.id.summary_btn);

        mSweetSheet = new SweetSheet(home);
        CustomDelegate customDelegate = new CustomDelegate(true,
                CustomDelegate.AnimationType.DuangAnimation);
        final View vieww = LayoutInflater.from(getApplicationContext()).inflate(R.layout.digit_summary, null, false);
        customDelegate.setCustomView(vieww);
        mSweetSheet.setDelegate(customDelegate);
        mSweetSheet.setBackgroundEffect(new DimEffect(0.9f));
        mSweetSheet.setBackgroundClickEnable(false);



        final TextView tv_goall = (TextView)vieww.findViewById(R.id.tv_goal_display);
        final TextView tvvv_goal = (TextView) vieww.findViewById(R.id.tv_goal12);

        if(preference.getString("goal",null) != null){
            tv_goall.setText(preference.getString("goal",""));
            tv_goal.setText(preference.getString("goal",""));
            tvvv_goal.setText(preference.getString("goal",""));
        }


        final int time = 10;

        if(preference.getString("resume_home","").equals("on")){
            editor.putString("resume_home","off");
            editor.commit();

            final TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            job_controller(time);
                            bill_controller(time);
                            asset_controller(time);
                            lib_controller(time);

                            tv_total_bill.setText(preference.getString("amount_bills",""));
                            tv_total_job.setText(preference.getString("amount_jobs",""));
                            tv_total_asset.setText(preference.getString("amount_assets",""));
                            tv_total_lib.setText(preference.getString("amount_libs",""));
                            tv_goall.setText(preference.getString("goal",""));
                            tv_goal.setText(preference.getString("goal",""));
                            tvvv_goal.setText(preference.getString("goal",""));

                            Double net_income = Double.valueOf(preference.getString("amount_jobs",""))
                                    +Double.valueOf(preference.getString("amount_assets",""));
                            Double net_cost = Double.valueOf(preference.getString("amount_bills",""))
                                    +Double.valueOf(preference.getString("amount_libs",""));

                            Double goall = Double.valueOf(preference.getString("goal",""));

                            Double net = 0.0;
                            double net1 = 0.0;
                            if(net_income >= net_cost){
                                net = net_income-net_cost;
                                net1 = net_income-net_cost;
                                editor.putString("net",String.valueOf(net));
                                editor.putString("net_pos","1");
                                editor.commit();
                                goall = goall - net;
                                tv_net.setTextColor(Color.WHITE);
                                tv_net.setText(String.format("%.2f", net_income-net_cost));
                            }
                            else {
                                net = net_cost-net_income;
                                net1 = net_income-net_cost;
                                editor.putString("net",String.valueOf(net));
                                editor.putString("net_pos","0");
                                editor.commit();
                                goall = goall + net;
                                tv_net.setTextColor(Color.RED);
                                tv_net.setText(String.format("%.2f", net_cost-net_income));
                            }


                            //Toast.makeText(MainActivity.this, "\n"+String.valueOf(net), Toast.LENGTH_SHORT).show();
                            final double rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                                    - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

                            double ggg = Double.valueOf(preference.getString("goal",""));

                            if(rate_d == 0){
                                tv_time.setText("00/00/0000");
                            }
                            else if(net1 < ggg && net1 >= 0){
                                tv_time.setText(preference.getString("goal_date",""));
                            }
                            else if(net1 >= ggg){
                                tv_time.setText("Target goal is filled up");
                            }
                            else {
                                tv_time.setText("Net worth is negative");
                            }


                        }
                    });
                }
            };
            timer.schedule(timerTask, 0, time*1000);
        }



        btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP){

                    mSweetSheet.toggle();

                    return true;
                }
                return false;
            }
        });
        /*btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btn.getBackground().setAlpha(180);
                        mSweetSheet.toggle();
                        btn.getBackground().setAlpha(255);
                        break;
                    case MotionEvent.ACTION_UP:
                        btn.getBackground().setAlpha(255);

                        break;
                }
                return false;
            }
        });*/
        vieww.findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSweetSheet.dismiss();
            }
        });

        vieww.findViewById(R.id.tv_set_goal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Goal");
                alertDialog.setMessage("Enter your goal");

                final EditText input = new EditText(MainActivity.this);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setRawInputType(Configuration.KEYBOARD_12KEY);
                alertDialog.setView(input);


                ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                                /*
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                //imm.hideSoftInputFromWindow(vv.getWindowToken(), 0);
                                imm.showSoftInput(input, InputMethodManager.SHOW_FORCED);
*/
                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, int which) {
                                String inputt = input.getText().toString();
                                Boolean bb = true;
                                for(int i = 0; i < inputt.length(); i++){
                                    char c = inputt.charAt(i);
                                    if(Character.isDigit(c) || c == '.'){

                                    }
                                    else {
                                        Toast.makeText(MainActivity.this, "Goal is not correct Digit", Toast.LENGTH_SHORT).show();
                                        bb=false;
                                        break;
                                    }
                                }
                                if(bb){
                                    editor.putString("goal",inputt);
                                    editor.commit();
                                    //db_helper.update_goal(inputt);
                                    tv_goall.setText(inputt);
                                    tv_goal.setText(inputt);
                                    tvvv_goal.setText(inputt);



                                    Toast.makeText(MainActivity.this, "Your goal "+input.getText(), Toast.LENGTH_SHORT).show();

                                    double goal = Double.valueOf(preference.getString("goal",""));

                                    double net = Double.valueOf(preference.getString("net",""));

                                    net = goal - net;

                                    if(net <= 0){

                                    }
                                    else {
                                        double rate_d=0;
                                        rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                                                - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

                                        if(rate_d <= 0){
                                            editor.putString("goal_date","0");
                                            editor.commit();


                                        }
                                        else {
                                            double days_count = net/rate_d;

                                            int dayss = (int) days_count;

                                            Calendar c1 = Calendar.getInstance();

                                            int date11 = c1.get(Calendar.DAY_OF_MONTH);
                                            int month11 = c1.get(Calendar.MONTH);
                                            int year11 = c1.get(Calendar.YEAR);

                                            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                                            String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date

                                            Date date1 = null;
                                            try {

                                                date1 = myFormat.parse(inputString1);

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            Calendar startCal = Calendar.getInstance();
                                            startCal.setTime(date1);


                                            //if(startCal.getTimeInMillis() <= endCal.getTimeInMillis())

                                            //dayss = dayss+ (int)dif;


                                            int r = 0;
                                            do {
                                                startCal.add(Calendar.DAY_OF_MONTH, 1);
                                                r++;
                                            } while (r < dayss); //excluding end date


                                            String ddd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                                    +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                                    +String.valueOf(startCal.get(Calendar.YEAR));

                                            editor.putString("goal_date",ddd);
                                            editor.commit();
                                            //db_helper.update_goal_date(ddd);
                                        }
                                    }






                                    //goal_date_handler j_h = new goal_date_handler();

                                    //j_h.goal_time_handler(getApplicationContext());

                                    new SweetAlertDialog(vv.getContext(), SweetAlertDialog.WARNING_TYPE)

                                            .setTitleText("Goal Set!")
                                            .setContentText("Your Goal has been set!")
                                            .setConfirmText("Ok")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(final SweetAlertDialog sDialog) {
                                                    Toast.makeText(MainActivity.this, "Your goal "+input.getText(), Toast.LENGTH_SHORT).show();

                                                    dialog.dismiss();
                                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                }
                                else {
                                    new SweetAlertDialog(vv.getContext(), SweetAlertDialog.ERROR_TYPE)

                                            .setTitleText("Goal Not Correct !!!")
                                            .setContentText("Your Input Goal is not correct")
                                            .show();
                                }

                            }
                        });

                alertDialog.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();

            }
        });

        final EditText input = new EditText(vv.getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        vieww.findViewById(R.id.tv_goal_display).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Goal");
                alertDialog.setMessage("Enter your goal");

                final EditText input = new EditText(MainActivity.this);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setRawInputType(Configuration.KEYBOARD_12KEY);
                alertDialog.setView(input);


                ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                                /*
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                //imm.hideSoftInputFromWindow(vv.getWindowToken(), 0);
                                imm.showSoftInput(input, InputMethodManager.SHOW_FORCED);
*/
                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, int which) {
                                String inputt = input.getText().toString();
                                Boolean bb = true;
                                for(int i = 0; i < inputt.length(); i++){
                                    char c = inputt.charAt(i);
                                    if(Character.isDigit(c) || c == '.'){

                                    }
                                    else {
                                        Toast.makeText(MainActivity.this, "Goal is not correct Digit", Toast.LENGTH_SHORT).show();
                                        bb=false;
                                        break;
                                    }
                                }
                                if(bb){
                                    editor.putString("goal",inputt);
                                    editor.commit();
                                    //db_helper.update_goal(inputt);
                                    tv_goall.setText(inputt);
                                    tv_goal.setText(inputt);
                                    tvvv_goal.setText(inputt);


                                    double goal = Double.valueOf(preference.getString("goal",""));

                                    double net = Double.valueOf(preference.getString("net",""));

                                    net = goal - net;

                                    if(net <= 0){

                                    }
                                    else {
                                        double rate_d=0;
                                        rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                                                - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

                                        if(rate_d <= 0){
                                            editor.putString("goal_date","0");
                                            editor.commit();
                                            //Toast.makeText(MainActivity.this, "ttt", Toast.LENGTH_SHORT).show();
                                        }else{
                                            double days_count = net/rate_d;

                                            int dayss = (int) days_count;

                                            Calendar c1 = Calendar.getInstance();

                                            int date11 = c1.get(Calendar.DAY_OF_MONTH);
                                            int month11 = c1.get(Calendar.MONTH);
                                            int year11 = c1.get(Calendar.YEAR);

                                            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
                                            String inputString1 = String.valueOf(date11) + " " + String.valueOf(month11+1) + " " + String.valueOf(year11); //start date

                                            Date date1 = null;
                                            try {

                                                date1 = myFormat.parse(inputString1);

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            Calendar startCal = Calendar.getInstance();
                                            startCal.setTime(date1);


                                            //if(startCal.getTimeInMillis() <= endCal.getTimeInMillis())

                                            //dayss = dayss+ (int)dif;


                                            int r = 0;
                                            do {
                                                startCal.add(Calendar.DAY_OF_MONTH, 1);
                                                r++;
                                            } while (r < dayss); //excluding end date


                                            String ddd =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                                    +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                                    +String.valueOf(startCal.get(Calendar.YEAR));

                                            editor.putString("goal_date",ddd);
                                            editor.commit();
                                            //db_helper.update_goal_date(ddd);

                                        }

                                    }


                                    //goal_date_handler j_h = new goal_date_handler();

                                    //j_h.goal_time_handler(getApplicationContext());

                                    Toast.makeText(MainActivity.this, "Your goal "+input.getText(), Toast.LENGTH_SHORT).show();

                                    new SweetAlertDialog(vv.getContext(), SweetAlertDialog.WARNING_TYPE)

                                            .setTitleText("Goal Set!")
                                            .setContentText("Your Goal has been set!")
                                            .setConfirmText("Ok")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(final SweetAlertDialog sDialog) {
                                                    Toast.makeText(MainActivity.this, "Your goal "+input.getText(), Toast.LENGTH_SHORT).show();

                                                    dialog.dismiss();
                                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                }
                                else {
                                    new SweetAlertDialog(vv.getContext(), SweetAlertDialog.ERROR_TYPE)

                                            .setTitleText("Goal Not Correct !!!")
                                            .setContentText("Your Input Goal is not correct")
                                            .show();
                                }

                            }
                        });

                alertDialog.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();

            }
        });


        expandableLayout0 = (ExpandableLayout) view0.findViewById(R.id.expandable_layout_0);


    }

    private void array_create(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        final SQLiteDatabase db = db_helper.getWritableDatabase();
        final Cursor cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
        final Cursor cursor1 = db.rawQuery("SELECT rowid _id,* FROM bills", null);
        final Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM asset", null);
        final Cursor cursor3 = db.rawQuery("SELECT rowid _id,* FROM lib", null);


        if(cursor.getCount() != 0){

            job_details = new String[cursor.getCount()][7];
            int jobi = 0;

            if(cursor.moveToFirst()){
                do{

                    job_name = cursor.getString(cursor.getColumnIndexOrThrow("job_name")).toString();
                    s_time = cursor.getString(cursor.getColumnIndexOrThrow("start_time"));
                    e_time = cursor.getString(cursor.getColumnIndexOrThrow("end_time"));
                    off_day = cursor.getString(cursor.getColumnIndexOrThrow("off_day"));
                    amount = cursor.getString(cursor.getColumnIndexOrThrow("salary_amount"));
                    s_type = cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));
                    s_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));

                    job_details[jobi][0]= job_name;
                    job_details[jobi][1]= s_time;
                    job_details[jobi][2]= e_time;
                    job_details[jobi][3]= off_day;
                    job_details[jobi][4]= amount;
                    job_details[jobi][5]= s_type;
                    job_details[jobi][6]= s_date;


                    jobi++;



                }
                while(cursor.moveToNext());
            }
        }
        else {
            //Toast.makeText(this, "0", Toast.LENGTH_SHORT).show();
        }


        //cursor.close();
        if(cursor1.getCount() != 0) {
            //Toast.makeText(this, "!0", Toast.LENGTH_SHORT).show();
            bill_details = new String[cursor1.getCount()][7];
            int billi = 0;

            if(cursor1.moveToFirst()){
                do{

                    bills_name = cursor1.getString(cursor1.getColumnIndexOrThrow("bills_name")).toString();
                    bills_amount = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_amount"));
                    bills_s_type = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_type"));
                    bills_s_time = cursor1.getString(cursor1.getColumnIndexOrThrow("start_time"));
                    bills_e_time = cursor1.getString(cursor1.getColumnIndexOrThrow("end_time"));
                    bills_s_date = cursor1.getString(cursor1.getColumnIndexOrThrow("start_date"));
                    bills_off_day = cursor1.getString(cursor1.getColumnIndexOrThrow("off_day"));
                    bills_start_date = cursor1.getString(cursor1.getColumnIndexOrThrow("start_date"));

                    bill_details[billi][0] = bills_name;
                    bill_details[billi][1] = bills_s_time;
                    bill_details[billi][2] = bills_e_time;
                    bill_details[billi][3] = bills_off_day;
                    bill_details[billi][4] = bills_amount;
                    bill_details[billi][5] = bills_s_type;
                    bill_details[billi][6] = bills_s_date;

                    billi++;

                }
                while(cursor1.moveToNext());
            }
        }


        //cursor1.close();
        if(cursor2.getCount() != 0) {

            asset_details = new String[cursor2.getCount()][4];
            int asseti = 0;

            if(cursor2.moveToFirst()){
                do{

                    String asset1_name = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_name")).toString();
                    String asset1_amount = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_amount"));
                    String asset1_s_type = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_type"));
                    String asset1_s_date = cursor2.getString(cursor2.getColumnIndexOrThrow("start_date"));
                    String asset1_net_amount = cursor2.getString(cursor2.getColumnIndexOrThrow("net_amount"));


                    asset_details[asseti][0] = asset1_name;
                    asset_details[asseti][1] = asset1_amount;
                    asset_details[asseti][2] = asset1_s_date;
                    asset_details[asseti][3] = asset1_s_type;

                    Toast.makeText(this, "asset net amount "+asset1_net_amount, Toast.LENGTH_SHORT).show();

                    asseti++;
                    /*asset_handler hand = new asset_handler();

                    hand.asset_handle(
                            getApplicationContext(),
                            asset1_name,
                            asset1_amount,
                            asset1_s_date,
                            asset1_s_type);*/

                }
                while(cursor2.moveToNext());
            }
        }

        if(cursor3.getCount() != 0) {

            lib_details = new String[cursor3.getCount()][4];
            int libi = 0;

            if(cursor3.moveToFirst()){
                do{

                    String lib1_name = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_name")).toString();
                    String lib1_amount = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_amount"));
                    String lib1_s_type = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_type"));
                    String lib1_s_date = cursor3.getString(cursor3.getColumnIndexOrThrow("start_date"));
                    String asset1_net_amount = cursor3.getString(cursor3.getColumnIndexOrThrow("net_amount"));


                    lib_details[libi][0] = lib1_name;
                    lib_details[libi][1] = lib1_amount;
                    lib_details[libi][2] = lib1_s_date;
                    lib_details[libi][3] = lib1_s_type;

                    libi++;

                }
                while(cursor3.moveToNext());
            }
        }

    }

    private void job_controller(final int t){
        try{
            for(int i = 0; i < job_details.length; i++){
                job_handler(t,
                        job_details[i][0],
                        job_details[i][1],
                        job_details[i][2],
                        job_details[i][3],
                        job_details[i][4],
                        job_details[i][5],
                        job_details[i][6]
                );
            }
        }
        catch (NullPointerException e){
            //Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }

    }
    private void bill_controller(final int t){
        try{
            for(int i = 0; i < bill_details.length; i++){
                bill_handler(t,
                        bill_details[i][0],
                        bill_details[i][1],
                        bill_details[i][2],
                        bill_details[i][3],
                        bill_details[i][4],
                        bill_details[i][5],
                        bill_details[i][6]
                );
            }
        }
        catch (NullPointerException e){
            //Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }

    }
    private void asset_controller(final int t){
        try{
            for(int i = 0; i < asset_details.length; i++){
                asset_handler(
                        asset_details[i][0],
                        asset_details[i][1],
                        asset_details[i][2],
                        asset_details[i][3]
                );
            }
        }
        catch (NullPointerException e){
            //Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }

    }
    private void lib_controller(final int t){
        try{
            for(int i = 0; i < lib_details.length; i++){
                lib_handler(
                        lib_details[i][0],
                        lib_details[i][1],
                        lib_details[i][2],
                        lib_details[i][3]
                );
            }
        }
        catch (NullPointerException e){
            //Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }

    }

    private void job_handler(final int time_var, final String job_name, final String s_time, final String e_time, final String off_day, final String amount, final String s_type , final String start_date){
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        final boolean[] check = {true};
        final boolean[] check1 = {true};

        double sum = 0.0 ;
        double rate ;
        int flag = 0;
        int flag1 = 0;
        float s_h,l_h;

        boolean ttt = true;


        String current_date = "";
        /*if(check1[0]){
            Toast.makeText(c, "check1 true", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(c, "check1 false", Toast.LENGTH_SHORT).show();
        }*/

        final float[] c_h = {0};

        final salary_rate s = new salary_rate();

        Calendar c1 = Calendar.getInstance();

        //sum = Double.valueOf(net_salary);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");


        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int date = c.get(Calendar.DAY_OF_WEEK);

        int ddd = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        current_date = String.valueOf(ddd) + "." + String.valueOf(month) +"."+String.valueOf(year) ;

        String ssss= String.valueOf(date);

        int calculation = 10;
        for(int i = 0; i < off_day.length(); i++ ){

            if(ssss.charAt(0) == off_day.charAt(i)){
                calculation = 0;
                break;
            }
            else {
                calculation = 1;
            }
        }

        c_h[0] = (float) (hour +(min/60.0));

        //Sunday == 1, Monday == 2, tuesday == 3.......Satarday == 7

        //Toast.makeText(finalCc, off_day/*String.valueOf(calculation)*/, Toast.LENGTH_SHORT).show();

        if(start_date_extract(start_date)
                && preference.getString(job_name,null) != null){

            if(calculation == 1){

                int last_hour = s.hour(e_time);

                int last_min = s.min(e_time);

                int start_hour = s.hour(s_time);

                int start_min = s.min(s_time);

                s_h = (float) (start_hour + (start_min/60.00));
                l_h = (float) (last_hour + (last_min/60.00));

                rate = s.calulate_salary_rate(s_time,e_time,amount,s_type,off_day);

                if(s_h < l_h){
                    if (c_h[0] >= s_h && c_h[0] <= l_h) {
                        flag = 1;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here1", Toast.LENGTH_SHORT).show();
                        wakeLock.acquire();
                    }
                    else {
                        flag = 2;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here2", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if (c_h[0] <= l_h && c_h[0] <= s_h) {
                        flag = 1;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here3", Toast.LENGTH_SHORT).show();
                        wakeLock.acquire();
                    }
                    else {
                        flag = 2;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here4", Toast.LENGTH_SHORT).show();
                    }
                }


                if (c_h[0] == l_h) {
                    flag = 2;
                    flag1 = 1;
                    wakeLock.release();
                }

                if(flag == 1){

                    if (flag1 == 1) {
                        flag1 = 2;
                        try{
                            sum = Double.valueOf(preference.getString(job_name,""));
                        }
                        catch (NumberFormatException e){

                        }

                    }
                    sum = sum + (rate*time_var);

                    db_helper.update_jobs_net_amount(job_name,String.format("%.5f",sum));
                    editor.putString(job_name,String.format("%.5f",sum));
                    editor.commit();


                    //Toast.makeText(finalCc, preference.getString(job_name,"")+job_name, Toast.LENGTH_SHORT).show();

                }
            }
        }
        else {
            check1[0] = false;
        }
    }
    private void bill_handler(final int time_var, final String job_name, final String s_time, final String e_time, final String off_day, final String amount, final String s_type , final String start_date){
        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();
        final boolean[] check = {true};
        final boolean[] check1 = {true};

        double sum = 0.0 ;
        double rate ;
        int flag = 0;
        int flag1 = 0;
        float s_h,l_h;

        boolean ttt = true;


        String current_date = "";
        /*if(check1[0]){
            Toast.makeText(c, "check1 true", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(c, "check1 false", Toast.LENGTH_SHORT).show();
        }*/

        final float[] c_h = {0};

        final salary_rate s = new salary_rate();

        Calendar c1 = Calendar.getInstance();

        //sum = Double.valueOf(net_salary);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");


        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int date = c.get(Calendar.DAY_OF_WEEK);

        int ddd = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        current_date = String.valueOf(ddd) + "." + String.valueOf(month) +"."+String.valueOf(year) ;

        String ssss= String.valueOf(date);

        int calculation = 10;
        for(int i = 0; i < off_day.length(); i++ ){

            if(ssss.charAt(0) == off_day.charAt(i)){
                calculation = 0;
                break;
            }
            else {
                calculation = 1;
            }
        }

        c_h[0] = (float) (hour +(min/60.0));

        //Sunday == 1, Monday == 2, tuesday == 3.......Satarday == 7

        //Toast.makeText(finalCc, off_day/*String.valueOf(calculation)*/, Toast.LENGTH_SHORT).show();

        if(start_date_extract(start_date)
                && preference.getString("bill_"+job_name,null) != null){

            if(calculation == 1){

                int last_hour = s.hour(e_time);

                int last_min = s.min(e_time);

                int start_hour = s.hour(s_time);

                int start_min = s.min(s_time);

                s_h = (float) (start_hour + (start_min/60.00));
                l_h = (float) (last_hour + (last_min/60.00));

                rate = s.calulate_salary_rate(s_time,e_time,amount,s_type,off_day);

                if(s_h < l_h){
                    if (c_h[0] >= s_h && c_h[0] <= l_h) {
                        flag = 1;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here1", Toast.LENGTH_SHORT).show();
                        wakeLock.acquire();
                    }
                    else {
                        flag = 2;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here2", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if (c_h[0] <= l_h && c_h[0] <= s_h) {
                        flag = 1;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here3", Toast.LENGTH_SHORT).show();
                        wakeLock.acquire();
                    }
                    else {
                        flag = 2;
                        flag1 = 1;
                        //Toast.makeText(finalCc, "here4", Toast.LENGTH_SHORT).show();
                    }
                }


                if (c_h[0] == l_h) {
                    flag = 2;
                    flag1 = 1;
                    wakeLock.release();
                }

                if(flag == 1){

                    if (flag1 == 1) {
                        flag1 = 2;
                        try{
                            sum = Double.valueOf(preference.getString("bill_"+job_name,""));
                        }
                        catch (NumberFormatException e){

                        }

                    }
                    sum = sum + (rate*time_var);

                    db_helper.update_bills_net_amount(job_name,String.format("%.5f",sum));
                    editor.putString("bill_"+job_name,String.format("%.5f",sum));
                    editor.commit();

                    //Toast.makeText(getApplicationContext(), preference.getString("bill_"+job_name,"")+job_name, Toast.LENGTH_SHORT).show();

                }
            }
        }
        else {
            check1[0] = false;
        }
    }
    private void asset_handler(final String name, final String amount, final String start_date, final String rate){
        boolean ttt = true;
        int start_day,start_month,start_year;
        double diff = 0.0;
        double net_amount = 0.0;

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");

        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        int start_day1=1;
        int start_month1=1;
        int start_year1=1;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < start_date.length();i++){

            if(start_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(start_date.charAt(i)));
        }
        for(j = i+1; j < start_date.length();j++){

            if(start_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(start_date.charAt(j)));
        }
        for (int k = j+1; k < start_date.length();k++){
            year = year.concat(String.valueOf(start_date.charAt(k)));
        }

        start_day1 = Integer.valueOf(dd);
        start_month1 = Integer.valueOf(month);
        start_year1 = Integer.valueOf(year);

        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);

        int date = c.get(Calendar.DAY_OF_MONTH);
        int month1 = c.get(Calendar.MONTH);
        int year1 = c.get(Calendar.YEAR);


        if(start_date_extract(start_date)
                && preference.getString("asset_"+name,null) != null) {
                            /*if(preference.getString(name+"_check_asset","").equals("true")) {
                                asset_goal_date_handler j_h = new asset_goal_date_handler();

                                j_h.asset_goal_timer(finalCc,
                                        name
                                );
                                editor.putString(name+"_check_asset","false");
                                editor.commit();
                            }*/


            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
            String inputString1 = String.valueOf(start_day1) + " " + String.valueOf(start_month1) + " " + String.valueOf(start_year1); //start date
            String inputString2 = String.valueOf(date) + " " + String.valueOf(month1 + 1) + " " + String.valueOf(year1); //present date

            Date date1 = null;
            Date date2 = null;

            net_amount = Double.valueOf(preference.getString("asset_" + name, ""));

            try {

                date1 = myFormat.parse(inputString1);
                date2 = myFormat.parse(inputString2);
                diff = getWorkingDaysBetweenTwoDates(date1, date2);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (rate.equals("week")) {
                if (diff != 0 && (diff % 7.0) == 0) {
                    net_amount = net_amount + Double.valueOf(amount);

                }
            } else if (rate.equals("month")) {
                if (diff != 0 && (diff % 30.0) == 0) {
                    net_amount = net_amount + Double.valueOf(amount);
                }
            } else if (rate.equals("year")) {
                if (diff != 0 && (diff % 365.0) == 0) {
                    net_amount = net_amount + Double.valueOf(amount);
                }
            }

            //Toast.makeText(this, "Handler "+ String.valueOf(net_amount), Toast.LENGTH_SHORT).show();
            db_helper.update_asset_net_amount(name, String.format("%.2f", net_amount));
            editor.putString("asset_" + name, String.format("%.2f", net_amount));
            editor.commit();
        }

    }
    private void lib_handler(final String name, final String amount, final String start_date, final String rate){
        boolean ttt = true;
        int start_day,start_month,start_year;
        double diff = 0.0;
        double net_amount = 0.0;

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");

        final SharedPreferences preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        int start_day1=1;
        int start_month1=1;
        int start_year1=1;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < start_date.length();i++){

            if(start_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(start_date.charAt(i)));
        }
        for(j = i+1; j < start_date.length();j++){

            if(start_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(start_date.charAt(j)));
        }
        for (int k = j+1; k < start_date.length();k++){
            year = year.concat(String.valueOf(start_date.charAt(k)));
        }

        start_day1 = Integer.valueOf(dd);
        start_month1 = Integer.valueOf(month);
        start_year1 = Integer.valueOf(year);

        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);

        int date = c.get(Calendar.DAY_OF_MONTH);
        int month1 = c.get(Calendar.MONTH);
        int year1 = c.get(Calendar.YEAR);


        if(start_date_extract(start_date)
                && preference.getString("lib_"+name,null) != null){

                           /* if(preference.getString(name+"_check_lib","").equals("true")) {
                                lib_goal_date_handler hh = new lib_goal_date_handler();
                                hh.timer(finalCc,name);
                                editor.putString(name+"_check_lib","false");
                                editor.commit();
                            }
*/
            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
            String inputString1 = String.valueOf(start_day1)+" "+String.valueOf(start_month1)+" "+String.valueOf(start_year1); //start date
            String inputString2 = String.valueOf(date)+" "+String.valueOf(month1+1)+" "+String.valueOf(year1); //present date

            Date date1 = null;
            Date date2 = null;

            net_amount = Double.valueOf(preference.getString("lib_"+name,""));

            try {
                date1 = myFormat.parse(inputString1);
                date2 = myFormat.parse(inputString2);
                diff = getWorkingDaysBetweenTwoDates(date1,date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(rate.equals("week")){
                if(diff != 0 && (diff % 7.0) == 0){
                    net_amount = net_amount + Double.valueOf(amount);

                }
            }
            else if(rate.equals("month")){
                if(diff != 0 && (diff % 30.0) == 0){
                    net_amount = net_amount + Double.valueOf(amount);
                }
            }
            else if(rate.equals("year")){
                if(diff != 0 && (diff % 365.0) == 0){
                    net_amount = net_amount + Double.valueOf(amount);
                }
            }

            db_helper.update_asset_net_amount(name,String.format("%.5f",net_amount));
            editor.putString("lib_"+name,String.format("%.5f",net_amount));
            editor.commit();
        }

    }

    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            //startCal.setTime(endDate);
            //endCal.setTime(startDate);
            return 0.0;
        }

        do {

            startCal.add(Calendar.DAY_OF_MONTH, 1);

            ++workDays;

        } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()); //excluding end date

        return workDays;
    }

    public static Double getWorkingDaysBetweenTwoDates1(Date startDate, Date endDate) {


        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;

        //Return 0 if start and present are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        //Return 0 if start date is less than present date
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);

            do {

                startCal.add(Calendar.DAY_OF_MONTH, 1);

                ++workDays;

            } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis()); //excluding end date

        }

        return workDays;
    }

    public boolean start_date_extract(String s_date){

        Boolean bbb=false;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < s_date.length();i++){

            if(s_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(s_date.charAt(i)));
        }
        for(j = i+1; j < s_date.length();j++){

            if(s_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(s_date.charAt(j)));
        }
        for (int k = j+1; k < s_date.length();k++){
            year = year.concat(String.valueOf(s_date.charAt(k)));
        }


        int s_year= Integer.valueOf(year);
        int s_month = Integer.valueOf(month);
        int s_dd = Integer.valueOf(dd);

        s_month=s_month-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year){
            bbb=true;
        }
        else if(c_year == s_year){
            if(c_month > s_month){
                bbb=true;
            }
            else if(c_month == s_month){
                if(c_dd >= s_dd){
                    bbb=true;
                }
            }
        }
        return bbb;
    }

    @Override
    protected void onResume() {
        super.onResume();
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("add","").equals("true")){
            editor.putString("add","f");
            editor.commit();

            array_create();
        }

        onResume_dropbox();
        //resume();
        if(preference.getString("import","").equals("true")){
            onResume_job();
            onResume_asset();
            onResume_bill();
            onResume_lib();
        }
    }

    private void onResume_dropbox(){
        //Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("auth","").equals("true")){
            editor.putString("auth","false");
            editor.commit();
            Auth_dropbox();
            //Toast.makeText(this, "onResume auth true", Toast.LENGTH_SHORT).show();

        }

        //Toast.makeText(this, ACCESS_TOKEN, Toast.LENGTH_SHORT).show();


        /*if(preference.getString("restart_after_download","").equals("true")){
            editor.putString("restart_after_download","false");
            editor.commit();
            finish();
            Intent intent = new Intent(getApplicationContext(),intro.class);
            startActivity(intent);
        }*/

            //l.setVisibility(View.INVISIBLE);

            if(preference.getString("dropbox","").equals("true")){
                editor.putString("dropbox","false");
                editor.commit();

                final TimerTask timerTask = new TimerTask() {

                    @Override
                    public void run() {
                        handler.post(new Runnable() {

                            @Override
                            public void run() {


                                if(preference.getString("sync","").equals("on") &&
                                        preference.getString("download_ok","").equals("true")){
                                    //Toast.makeText(getApplicationContext(), "onResume dropbox true", Toast.LENGTH_SHORT).show();
                                    editor.putString("download_ok","false");
                                    editor.commit();
                                    importDB_dropBox();
                                }
                                if(preference.getString("sync","").equals("on") &&
                                        preference.getString("download","").equals("false")){
                                    //Toast.makeText(getApplicationContext(), "onResume dropbox true1", Toast.LENGTH_SHORT).show();
                                    upload();
                                }
                            }
                        });
                    }
                };
                timer.schedule(timerTask, 0,10*1000);
            }


    }

    private void onResume_lib(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("edit_libs","").equals("on")){
            if(!e4.isExpanded()){
                e4.expand();
            }
        }
        else{
            if(e4.isExpanded()){
                e4.collapse();
            }
        }
        final SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor2 = null;
        Double total = 0.0;

        try {
            cursor2 = db.rawQuery("SELECT rowid _id,* FROM lib", null);


            if(cursor2.moveToFirst()){
                do{

                     String s_d = cursor2.getString(cursor2.getColumnIndexOrThrow("lib_name"));
                    String am =  cursor2.getString(cursor2.getColumnIndexOrThrow("net_amount"));
                    String asset1_amount = cursor2.getString(cursor2.getColumnIndexOrThrow("lib_amount"));
                    String asset1_s_type = cursor2.getString(cursor2.getColumnIndexOrThrow("lib_type"));

                    Double temp;

                    if(asset1_s_type.equals("fixed")){
                        temp= Double.valueOf(asset1_amount);
                    }
                    else {
                        temp = Double.valueOf(am);
                    }

                    total = total+temp;



                }
                while(cursor2.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor2 != null)
                cursor2.close();
        }


        tv_amount_lib.setText("$ "+String.format("%.2f", total));
        tv_total_lib.setText(String.format("%.2f", total));
        editor.putString("amount_libs",String.format("%.2f", total));
        editor.commit();

        if(preference.getString("refresh_lib","").equals("needed")){
            View2_Libs(cost_libs);
        }
    }

    protected void onResume_bill(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("edit_bills1","").equals("on")){
            if(!e3.isExpanded()){
                e3.expand();
            }
        }
        else {
            if(e3.isExpanded()){
                e3.collapse();
            }
        }

        if(preference.getString("resume_main_bill",null) == null || preference.getString("resume_main_bill","").equals("on")){
            editor.putString("resume_main_bill","off");
            editor.commit();

            final TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            final SQLiteDatabase db = db_helper.getReadableDatabase();

                            try{
                                final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM bills", null);

                                final String [] array_job_name = new String[todoCursor.getCount()];
                                int ii = 0;
                                if(todoCursor.moveToFirst()){
                                    do{

                                        array_job_name[ii] = todoCursor.getString(todoCursor.getColumnIndexOrThrow("bills_name")).toString();
                                        ii++;

                                    }
                                    while(todoCursor.moveToNext());
                                }

                                Double total = 0.0;

                                for(int jj = 0;jj < todoCursor.getCount();jj++){
                                    String value = preference.getString("bill_"+array_job_name[jj],"");
                                    Double temp;
                                    try{
                                        temp = Double.valueOf(value);
                                    }catch (NumberFormatException e){
                                        temp=0.0;
                                    }

                                    total = total+temp;
                                }

                                tv_amount_bill.setText("$ "+String.format("%.2f", total));
                                editor.putString("amount_bills",String.format("%.2f", total));
                                editor.commit();
                            }catch (SQLiteException e){

                            }

                        }
                    });
                }
            };
            timer.schedule(timerTask, 0, 5000);

        }

        if(preference.getString("refresh_bills","").equals("needed")){
            View2_Bill(cost_bills);
        }
    }

    protected void onResume_job(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        final SQLiteDatabase db = db_helper.getReadableDatabase();
        final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM job", null);

        if(preference.getString("edit_jobs","").equals("on")){
            if(!e1.isExpanded()){
                e1.expand();
            }
        }
        else {
            if(e1.isExpanded()){
                e1.collapse();
            }
        }

        if(preference.getString("resume_main",null) == null || preference.getString("resume_main","").equals("on")){
            editor.putString("resume_main","off");
            editor.commit();

            final TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            int ii = 0;

                            final SQLiteDatabase db = db_helper.getReadableDatabase();

                            try{
                                final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
                                String [] array_job_name = new String[0];
                                if(todoCursor.getCount() != 0){
                                    array_job_name = new String[todoCursor.getCount()];
                                }

                                if(todoCursor.moveToFirst()){
                                    do{

                                        array_job_name[ii] = todoCursor.getString(todoCursor.getColumnIndexOrThrow("job_name")).toString();
                                        ii++;

                                    }
                                    while(todoCursor.moveToNext());
                                }


                                final Double[] total = {0.0};
                                for(int jj = 0;jj < todoCursor.getCount();jj++){
                                    String value = preference.getString(array_job_name[jj],"");
                                    Double temp;
                                    try{
                                        temp = Double.valueOf(value);
                                    }catch (NumberFormatException e){
                                        temp = 0.0;
                                    }

                                    total[0] = total[0] +temp;
                                }

                                tv_amount_job.setText("$ "+String.format("%.2f", total[0]));
                                //Toast.makeText(MainActivity.this, "Jobs total"+String.format("%.2f", total[0]), Toast.LENGTH_SHORT).show();
                                editor.putString("amount_jobs",String.format("%.2f", total[0]));
                                editor.commit();
                            }catch (SQLiteException e){

                            }



                        }
                    });
                }
            };
            timer.schedule(timerTask, 0, 5000);
        }

        if(preference.getString("refresh","").equals("needed")){
            View1_Job(income_jobs);
        }
    }

    protected  void onResume_asset(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        if(preference.getString("edit_assets","").equals("on")){
            if(!e2.isExpanded()){
                e2.expand();
            }
        }
        else {
            if(e2.isExpanded()){
                e2.collapse();;
            }
        }

        final SQLiteDatabase db = db_helper.getReadableDatabase();

        Double total = 0.0;
        if(preference.getString("refresh_asset","").equals("needed")){
            //View1_asset(income_assets);
            //Toast.makeText(this, "okk", Toast.LENGTH_SHORT).show();
            //Toast.makeText(this, "asset "+preference.getString("amount_assets",""), Toast.LENGTH_SHORT).show();
        }
        View1_asset(income_assets);

        final Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM asset", null);
        //Toast.makeText(this, "count "+String.valueOf(cursor2.getCount()), Toast.LENGTH_SHORT).show();
        if(cursor2.moveToFirst()) {

            do {

                String s_d = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_name"));
                String am =  cursor2.getString(cursor2.getColumnIndexOrThrow("net_amount"));
                String asset1_amount = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_amount"));
                String asset1_s_type = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_type"));
                Double temp;

                if(asset1_s_type.equals("fixed")){
                     temp= Double.valueOf(asset1_amount);
                }
                else {
                    temp = Double.valueOf(am);

                }

                total = total+temp;
                //Toast.makeText(this, " "+am+" "+s_d+" "+asset1_amount, Toast.LENGTH_SHORT).show();


            } while (cursor2.moveToNext());
        }

        tv_amount_asset.setText("$ "+String.format("%.2f", total));
        tv_total_asset.setText(String.format("%.2f", total));
        editor.putString("amount_assets",String.format("%.2f", total));
        editor.commit();
        //Toast.makeText(this, "asset "+preference.getString("amount_assets",""), Toast.LENGTH_SHORT).show();

       /* final Cursor todoCursor = db.rawQuery("SELECT rowid _id,* FROM asset", null);

// Setup cursor adapter using cursor from last step
        final asset_cursor_adapter todoAdapter = new asset_cursor_adapter(this, todoCursor);
// Attach cursor adapter to the ListView
        lv_asset.setAdapter(todoAdapter);*/

    }


    @Override
    protected void onPause() {
        super.onPause();
       // t1.interrupt();
       // pause();
    }


    protected void clock(View v){
        mCharHighSecond = (TabDigit) v.findViewById(R.id.charHighSecond);
        mCharLowSecond = (TabDigit) v.findViewById(R.id.charLowSecond);
        mCharHighMinute = (TabDigit) v.findViewById(R.id.charHighMinute);
        mCharLowMinute = (TabDigit) v.findViewById(R.id.charLowMinute);
        mCharHighHour = (TabDigit) v.findViewById(R.id.charHighHour);
        mCharLowHour = (TabDigit) v.findViewById(R.id.charLowHour);
        mCharHighSecond.setTextSize(100);

        mCharHighSecond.setTextSize(100);
        mCharHighSecond.setChars(SEXAGISIMAL);
        mCharLowSecond.setTextSize(100);

        mCharHighMinute.setTextSize(100);
        mCharHighMinute.setChars(SEXAGISIMAL);
        mCharLowMinute.setTextSize(100);

        mCharHighHour.setTextSize(100);
        mCharHighHour.setChars(HOURS);
        mCharLowHour.setTextSize(100);

        mClock = (LinearLayout) v.findViewById(R.id.clock_l);

    }

    public void resume() {
        mPause = false;
        Calendar time = Calendar.getInstance();
        /* hours*/
        int hour = time.get(Calendar.HOUR_OF_DAY);
        int highHour = hour / 10;
        mCharHighHour.setChar(highHour);

        int lowHour = (hour - highHour * 10);
        mCharLowHour.setChar(lowHour);

        /* minutes*/
        int minutes = time.get(Calendar.MINUTE);
        int highMinute = minutes / 10;
        mCharHighMinute.setChar(highMinute);

        int lowMinute = (minutes - highMinute * 10);
        mCharLowMinute.setChar(lowMinute);

        /* seconds*/
        int seconds = time.get(Calendar.SECOND);
        int highSecond = seconds / 10;
        mCharHighSecond.setChar(highSecond);

        int lowSecond = (seconds - highSecond * 10);
        mCharLowSecond.setChar(lowSecond);

        elapsedTime = lowSecond + highSecond * 10
                + lowMinute * 60 + highMinute * 600
                + lowHour * 3600 + highHour * 36000;

        ViewCompat.postOnAnimationDelayed(mClock, this, 1000);
    }

    @Override
    public void run() {
        if(mPause){
            return;
        }
        elapsedTime += 1;
        mCharLowSecond.start();
        if (elapsedTime % 10 == 0) {
            mCharHighSecond.start();
        }
        if (elapsedTime % 60 == 0) {
            mCharLowMinute.start();
        }
        if (elapsedTime % 600 == 0) {
            mCharHighMinute.start();
        }
        if (elapsedTime % 3600 == 0) {
            mCharLowHour.start();
        }
        if (elapsedTime % 36000 == 0) {
            mCharHighHour.start();
        }
        ViewCompat.postOnAnimationDelayed(mClock, this, 1000);
    }

    public void pause() {
        mPause = true;
        mCharHighSecond.sync();
        mCharLowSecond.sync();
        mCharHighMinute.sync();
        mCharLowMinute.sync();
        mCharHighHour.sync();
        mCharLowHour.sync();
    }


    @Override
    public void onBackPressed() {

        if(mSweetSheet.isShow()){

            mSweetSheet.dismiss();
        }
        else{

            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Are you sure to Exit?")
                    .setContentText("Thanks for using DIGITS")
                    .setCancelText("May be Later")
                    .setConfirmText("Rate our App")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sDialog) {

                            Toast.makeText(MainActivity.this, "Under developing", Toast.LENGTH_SHORT).show();
                            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=hungers.com.emroze.digits"); // missing 'http://' will cause crashed
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                            finish();
                            //sDialog.onBackPressed();
                            /*sDialog.setTitleText("Deleted!")
                                    .setContentText("Your item has been deleted!")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sDialog.dismiss();
                                        }
                                    })
                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*/

                        }
                    })
                    .show()
                    ;

        }
    }


    private int getStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getWindow().getStatusBarColor();
        }
        return 0;
    }

    private void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        editor.putString("page","0");
        editor.commit();
    }


    public Animation inFromRightAnimation()
    {
        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(340);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public void fade_in_out(View v){

        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(v, "alpha",  1f, .3f);
        fadeOut.setDuration(1000);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(v, "alpha", 0f, 1f);
        fadeIn.setDuration(700);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn);

        mAnimationSet.start();
    }


    private void SQLite2Excel(){
        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Digits";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        // Export SQLite DB as EXCEL FILE
        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), db_helper.DATABASE_NAME, directory_path);

        sqliteToExcel.exportAllTables("digits.xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }
            @Override
            public void onCompleted(String filePath) {
               // Toast.makeText(getApplicationContext(), "All File has been created", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(Exception e) {

            }
        });

    }

    private void exportDB(){

        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Digits";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/data/"+ "hungers.com.emroze.digits" +"/databases/"+db_helper.DATABASE_NAME;
        String backupDBPath = "/Digits/"+db_helper.DATABASE_NAME;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            //Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
        }
    }

    private void restoreDB(){

        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Digits/Download";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/Digits/"+db_helper.DATABASE_NAME;;
        String backupDBPath = "/Digits/Download/"+db_helper.DATABASE_NAME;
        File currentDB = new File(sd, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Toast.makeText(this, "DB Exported to ../Digits/Download", Toast.LENGTH_LONG).show();
        } catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
        }
    }

    private void importDB_dropBox(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Found some previous Data from DropBox !!")
                        .setContentText("Do you want to restore it?")
                        .setCancelText("No,cancel plz!")
                        .setConfirmText("Yes,restore it!")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                //restoreDB();
                                //final SQLiteDatabase db = db_helper.getWritableDatabase();
                                //db_helper.onCreate(db);
                                editor.putString("download_ok", "false");
                                editor.putString("download", "false");
                                editor.commit();
                                // reuse previous dialog instance, keep widget user state, reset them if you need
                                sDialog.setTitleText("Cancelled!")
                                        .setContentText("If you want to restore it again.Plz go to setting")
                                        .setConfirmText("OK")
                                        .showCancelButton(false)
                                        .setCancelClickListener(null)
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);

                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                importDB_dropBox1();
                            }
                        })
                        .show();




    }

    private void importDB_dropBox1(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/data/"+ "hungers.com.emroze.digits" +"/databases/"+db_helper.DATABASE_NAME;
        String backupDBPath = "/Digits/Download/"+db_helper.DATABASE_NAME;
        File currentDB = new File(sd, backupDBPath);
        File backupDB = new File(data, currentDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();

            Calendar cc = Calendar.getInstance();

            int hour = cc.get(Calendar.HOUR_OF_DAY);
            int min = cc.get(Calendar.MINUTE);
            int sec = cc.get(Calendar.SECOND);
            int date = cc.get(Calendar.DAY_OF_MONTH);
            int month = cc.get(Calendar.MONTH);
            int year = cc.get(Calendar.YEAR);

            editor.putString("import","true");
            editor.putString("backup_exit","true");
            editor.putInt("hour",hour);
            editor.putInt("min",min);
            editor.putInt("sec",sec);
            editor.putInt("date",date);
            editor.putInt("month",month);
            editor.putInt("year",year);
            editor.putString("trigger","on");
            editor.putString("resume_main","on");
            editor.putString("download","false");
            editor.putString("page","0");
            editor.commit();

            finish();
            Intent intent = new Intent(getApplicationContext(),intro.class);
            startActivity(intent);


        } catch(IOException e) {
            e.printStackTrace();
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
        }
    }

    private void importDB1(){

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/data/"+ "hungers.com.emroze.digits" +"/databases/"+db_helper.DATABASE_NAME;
        String backupDBPath = "/Digits/"+db_helper.DATABASE_NAME;
        File currentDB = new File(sd, backupDBPath);
        File backupDB = new File(data, currentDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();

            Calendar cc = Calendar.getInstance();

            int hour = cc.get(Calendar.HOUR_OF_DAY);
            int min = cc.get(Calendar.MINUTE);
            int sec = cc.get(Calendar.SECOND);
            int date = cc.get(Calendar.DAY_OF_MONTH);
            int month = cc.get(Calendar.MONTH);
            int year = cc.get(Calendar.YEAR);

            editor.putString("import","true");
            editor.putString("backup_exit","true");
            editor.putInt("hour",hour);
            editor.putInt("min",min);
            editor.putInt("sec",sec);
            editor.putInt("date",date);
            editor.putInt("month",month);
            editor.putInt("year",year);
            editor.putString("trigger","on");
            editor.putString("resume_main","on");
            editor.putString("page","0");
            editor.commit();

            finish();
            Intent intent = new Intent(getApplicationContext(),intro.class);
            startActivity(intent);


        } catch(IOException e) {
            e.printStackTrace();
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
        }
    }

    private void importDB(){
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();

        String destination_path = "/data/"+ "hungers.com.emroze.digits" +"/databases/"+db_helper.DATABASE_NAME;
        String current_path = "/Digits/"+db_helper.DATABASE_NAME;
        File currentDB = new File(sd, current_path);
        File backupDB = new File(data, destination_path);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            if(preference.getString("error","").equals("true")){
                editor.putString("error","false");
                editor.commit();
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Found some previous Data !!")
                        .setContentText("Do you want to restore it?")
                        .setCancelText("No,cancel plz!")
                        .setConfirmText("Yes,restore it!")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                //restoreDB();
                                //final SQLiteDatabase db = db_helper.getWritableDatabase();
                                //db_helper.onCreate(db);
                                editor.putString("import","true");
                                editor.commit();
                                // reuse previous dialog instance, keep widget user state, reset them if you need
                                sDialog.setTitleText("Cancelled!")
                                        .setContentText("If you want to restore it again.Plz go to setting")
                                        .setConfirmText("OK")
                                        .showCancelButton(false)
                                        .setCancelClickListener(null)
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);

                                // or you can new a SweetAlertDialog to show
                               /* sDialog.dismiss();
                                new SweetAlertDialog(SampleActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Cancelled!")
                                        .setContentText("Your imaginary file is safe :)")
                                        .setConfirmText("OK")
                                        .show();*/
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sDialog) {
                                importDB1();
                                sDialog.setTitleText("DB imported!")
                                        .setContentText("Your previous data successfully imported!")
                                        .setConfirmText("OK")
                                        .showCancelButton(false)
                                        .setCancelClickListener(null)
                                        .setConfirmClickListener(null)
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                //System.exit(0);
                                                sDialog.dismiss();
                                            }
                                        })
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            }
                        })
                        .show();
                source.close();
                destination.close();

            }

        } catch(IOException e) {
            e.printStackTrace();
            editor.putString("error","true");
            editor.commit();
            //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_LONG).show();
        }
    }

    private void create_preference() {
        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final android.content.SharedPreferences.Editor editor=preference.edit();
        final salary_rate s = new salary_rate();

        final SQLiteDatabase db = db_helper.getWritableDatabase();
        final Cursor cursor = db.rawQuery("SELECT rowid _id,* FROM job", null);
        final Cursor cursor1 = db.rawQuery("SELECT rowid _id,* FROM bills", null);
        final Cursor cursor2 = db.rawQuery("SELECT rowid _id,* FROM asset", null);
        final Cursor cursor3 = db.rawQuery("SELECT rowid _id,* FROM lib", null);
        //final Cursor cursor4 = db.rawQuery("SELECT rowid _id,* FROM goal", null);

        //Toast.makeText(this, String.valueOf(cursor4.getCount()), Toast.LENGTH_SHORT).show();

        double rate_d_job = 0.0;
        double rate_d_bill = 0.0;
        double rate_d_asset = 0.0;
        double rate_d_lib = 0.0;


        if(cursor.moveToFirst()) {
            do {
                String s_d = cursor.getString(cursor.getColumnIndexOrThrow("job_name"));
                String am =  cursor.getString(cursor.getColumnIndexOrThrow("net_amount"));

                //job_name = cursor.getString(cursor.getColumnIndexOrThrow("job_name")).toString();
                s_time = cursor.getString(cursor.getColumnIndexOrThrow("start_time"));
                e_time = cursor.getString(cursor.getColumnIndexOrThrow("end_time"));
                off_day = cursor.getString(cursor.getColumnIndexOrThrow("off_day"));
                amount = cursor.getString(cursor.getColumnIndexOrThrow("salary_amount"));
                s_type = cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));
                //s_date = cursor.getString(cursor.getColumnIndexOrThrow("start_date"));

                //rate_d_job = s.calulate_salary_rate_per_day(s_time,e_time,amount,s_type,off_day);

                rate_d_job = rate_d_job + s.calulate_salary_rate_per_day(s_time,e_time,amount,s_type,off_day);

                editor.putString(s_d,am);
                editor.commit();
            } while (cursor.moveToNext());
        }

        if(cursor1.moveToFirst()) {
            do {


                bills_name = cursor1.getString(cursor1.getColumnIndexOrThrow("bills_name")).toString();
                bills_amount = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_amount"));
                bills_s_type = cursor1.getString(cursor1.getColumnIndexOrThrow("salary_type"));
                bills_s_time = cursor1.getString(cursor1.getColumnIndexOrThrow("start_time"));
                bills_e_time = cursor1.getString(cursor1.getColumnIndexOrThrow("end_time"));
                bills_off_day = cursor1.getString(cursor1.getColumnIndexOrThrow("off_day"));

                rate_d_bill = rate_d_bill + s.calulate_salary_rate_per_day(bills_s_time,bills_e_time,bills_amount,bills_s_type,bills_off_day);


                editor.putString("bill_" + bills_name, bills_amount);
                editor.commit();


            } while (cursor1.moveToNext());
        }
        //Toast.makeText(this, "asset row count create preference "+String.valueOf(cursor2.getCount()), Toast.LENGTH_SHORT).show();
        if(cursor2.moveToFirst()) {

                do {

                    String s_d = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_name"));
                    String am =  cursor2.getString(cursor2.getColumnIndexOrThrow("net_amount"));
                    String asset1_amount = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_amount"));
                    String asset1_s_type = cursor2.getString(cursor2.getColumnIndexOrThrow("asset_type"));

                    if(asset1_s_type.equals("week")){
                        rate_d_asset = rate_d_asset + (Double.valueOf(asset1_amount)/7.0);
                    }
                    else if(asset1_s_type.equals("month")){
                        rate_d_asset = rate_d_asset + (Double.valueOf(asset1_amount)/30.0);
                    }
                    else if(asset1_s_type.equals("year")){
                        rate_d_asset = rate_d_asset + (Double.valueOf(asset1_amount)/365.0);
                    }

                    editor.putString("asset_"+s_d,am);
                    editor.commit();
                    //Toast.makeText(this, "create preference "+am, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(this, "preferernce "+preference.getString("asset_"+s_d,""), Toast.LENGTH_SHORT).show();


                } while (cursor2.moveToNext());
        }
        if(cursor3.moveToFirst()) {
                do {

                    String s_d = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_name"));
                    String am =  cursor3.getString(cursor3.getColumnIndexOrThrow("net_amount"));
                    String lib1_amount = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_amount"));
                    String lib1_s_type = cursor3.getString(cursor3.getColumnIndexOrThrow("lib_type"));

                    if(lib1_s_type.equals("week")){
                        rate_d_lib = rate_d_lib + (Double.valueOf(lib1_amount)/7.0);
                    }
                    else if(lib1_s_type.equals("month")){
                        rate_d_lib = rate_d_lib + (Double.valueOf(lib1_amount)/30.0);
                    }
                    else if(lib1_s_type.equals("year")){
                        rate_d_lib = rate_d_lib + (Double.valueOf(lib1_amount)/365.0);
                    }
                    editor.putString("lib_"+s_d,am);
                    editor.commit();


                } while (cursor3.moveToNext());
        }
        editor.putString("net_job_rate",String.valueOf(rate_d_job));
        editor.putString("net_asset_rate",String.valueOf(rate_d_asset));
        editor.putString("net_bill_rate",String.valueOf(rate_d_bill));
        editor.putString("net_lib_rate",String.valueOf(rate_d_lib));
        editor.commit();

        /*if(cursor4.moveToFirst()) {
            do {
                String ggg = cursor4.getString(cursor4.getColumnIndexOrThrow("goal"));
                String r_j =  cursor4.getString(cursor4.getColumnIndexOrThrow("rate_d_job"));
                String r_a =  cursor4.getString(cursor4.getColumnIndexOrThrow("rate_d_asset"));
                String r_b =  cursor4.getString(cursor4.getColumnIndexOrThrow("rate_d_bill"));
                String r_l =  cursor4.getString(cursor4.getColumnIndexOrThrow("rate_d_lib"));
                String g_d =  cursor4.getString(cursor4.getColumnIndexOrThrow("goal_d"));

                editor.putString("goal",ggg);
                editor.putString("net_job_rate",r_j);
                editor.putString("net_asset_rate",r_a);
                editor.putString("net_bill_rate",r_b);
                editor.putString("net_lib_rate",r_l);
                editor.putString("goal_date",g_d);
                editor.commit();

                //Toast.makeText(this, ggg+" "+g_d+" "+r_j, Toast.LENGTH_SHORT).show();

            } while (cursor4.moveToNext());
        }*/



    }

    private void checkFilePermissions() {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            int permissionCheck = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permissionCheck = this.checkSelfPermission("Manifest.permission.READ_EXTERNAL_STORAGE");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permissionCheck += this.checkSelfPermission("Manifest.permission.WRITE_EXTERNAL_STORAGE");
            }
            if (permissionCheck != 0) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1001); //Any number
                }
            }
        }else{

        }
    }

}
