package hungers.com.emroze.digits.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import hungers.com.emroze.digits.bills.bills_item_details;
import hungers.com.emroze.digits.job.job_item;

/**
 * Created by emroze on 6/3/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VIRSION = 1;
    public static final String DATABASE_NAME = "digits.db";

    public  String TABLE_NAME = "job";
    public  String TABLE_NAME2 = "asset";
    public  String TABLE_NAME3 = "bills";
    public  String TABLE_NAME4 = "lib";
    //public  String TABLE_NAME5 = "goal";

    public  String COLUMN_ID = "id";
    public  String COLUMN_JOB_NAME = "job_name";
    public  String COLUMN_SALARY = "salary_amount";
    public  String COLUMN_SALARY_TYPE = "salary_type";
    public  String COLUMN_START_TIME = "start_time";
    public  String COLUMN_END_TIME = "end_time";
    public  String COLUMN_START_DATE = "start_date";
    public  String COLUMN_OFF_DAY = "off_day";
    public  String COLUMN_NET_AMOUNT = "net_amount";

    public  String COLUMN_ASSET_NAME = "asset_name";
    public  String COLUMN_ASSET_AMOUNT = "asset_amount";
    public  String COLUMN_ASSET_TYPE = "asset_type";
    public  String COLUMN_ASSET_START_DATE = "start_date";
    public  String COLUMN_ASSET_NET_AMOUNT = "net_amount";

    public  String COLUMN_BILLS_NAME = "bills_name";

    public  String COLUMN_LIB_NAME = "lib_name";
    public  String COLUMN_LIB_AMOUNT = "lib_amount";
    public  String COLUMN_LIB_TYPE = "lib_type";
    public  String COLUMN_LIB_START_DATE = "start_date";
    public  String COLUMN_LIB_NET_AMOUNT = "net_amount";

    public static String PREFS_NAME="digits";

    private static final String TABLE_CREATE = "CREATE TABLE IF NOT EXISTS job ( _id INTEGER PRIMARY KEY AUTOINCREMENT, job_name VARCHAR,salary_amount VARCHAR, salary_type VARCHAR, start_time VARCHAR, end_time VARCHAR, start_date VARCHAR, off_day VARCHAR, net_amount VARCHAR);";

    private static final String TABLE_CREATE2 = "CREATE TABLE IF NOT EXISTS asset ( _id INTEGER PRIMARY KEY AUTOINCREMENT, asset_name VARCHAR,asset_amount VARCHAR ,asset_type VARCHAR, start_date VARCHAR, net_amount VARCHAR);";

    private static final String TABLE_CREATE3 = "CREATE TABLE IF NOT EXISTS bills ( _id INTEGER PRIMARY KEY AUTOINCREMENT, bills_name VARCHAR,salary_amount VARCHAR, salary_type VARCHAR, start_time VARCHAR, end_time VARCHAR, start_date VARCHAR, off_day VARCHAR, net_amount VARCHAR);";

    private static final String TABLE_CREATE4 = "CREATE TABLE IF NOT EXISTS lib ( _id INTEGER PRIMARY KEY AUTOINCREMENT, lib_name VARCHAR,lib_amount VARCHAR,lib_type VARCHAR, start_date VARCHAR, net_amount VARCHAR);";

    //private static final String essential_table = "CREATE TABLE IF NOT EXISTS goal ( _id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, goal VARCHAR,rate_d_job VARCHAR,rate_d_bill VARCHAR, rate_d_asset VARCHAR, rate_d_lib VARCHAR, goal_d VARCHAR);";

    public SQLiteDatabase db;

    public DatabaseHelper(Context context){
         super(context, DATABASE_NAME , null , DATABASE_VIRSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_CREATE2);
        db.execSQL(TABLE_CREATE3);
        db.execSQL(TABLE_CREATE4);

        this.db = db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String query = "DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(query);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME3);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME4);

        this.onCreate(db);
    }

    /*@Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int old, int n) {
        if(old == 1){
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            db.execSQL(TABLE_CREATE);

        }
        else if( old == 2){
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);
            db.execSQL(TABLE_CREATE2);

        }
        else if( old == 3){
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME3);
            db.execSQL(TABLE_CREATE3);

        }
        else if( old == 4){
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME4);
            db.execSQL(TABLE_CREATE4);

        }
        *//*else if( old == 5){
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME5);
            db.execSQL(essential_table);

        }*//*

        this.db = db;
    }*/

    /*public void e_insert(){
        db = this.getWritableDatabase();
        final Cursor cursor4 = db.rawQuery("SELECT rowid _id,* FROM goal", null);

        String nn = "digit";
        boolean t = true;
        //Toast.makeText(MainActivity.this, "goooooooal", Toast.LENGTH_SHORT).show();
        if(cursor4.moveToFirst()) {
            do {
                if(nn.equals(cursor4.getString(cursor4.getColumnIndexOrThrow("name")))){
                    t = false;
                }
            } while (cursor4.moveToNext());
        }

        if(t){
            ContentValues values = new ContentValues();

            values.put("name", "digit");
            values.put("goal", "0");
            values.put("rate_d_job","0");
            values.put("rate_d_asset","0");
            values.put("rate_d_bill","0");
            values.put("rate_d_lib","0");
            values.put("goal_d","0");

            db.insert(TABLE_NAME5,null,values);
        }

    }
    public void update_goal(String g){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        String name = "digit";
        Cursor cursor4545 = null;

            cursor4545 = db.rawQuery(" SELECT rowid _id,* FROM goal",null);
            if(cursor4545.moveToFirst()){
                do{
                    if(name.equals(cursor4545.getString(cursor4545.getColumnIndexOrThrow("name")))){
                        values.put("goal", g);

                        db.update(TABLE_NAME5, values, "_id="+cursor4545.getString(cursor4545.getColumnIndexOrThrow("_id")), null);

                        break;
                    }
                }
                while(cursor4545.moveToNext());
            }

    }
    public void update_goal_date(String g){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        String name = "digit";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM goal",null);
            if(cursor.moveToFirst()){
                do{
                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("name")))){
                        values.put("goal_d", g);

                        db.update(TABLE_NAME5, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }
    }
    public void update_rate_job(String g){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        String name = "digit";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM goal",null);
            if(cursor.moveToFirst()){
                do{
                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("name")))){
                        values.put("rate_d_job", g);

                        db.update(TABLE_NAME5, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }
    }
    public void update_rate_asset(String g){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        String name = "digit";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM goal",null);
            if(cursor.moveToFirst()){
                do{
                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("name")))){
                        values.put("rate_d_asset", g);

                        db.update(TABLE_NAME5, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }
    }
    public void update_rate_bill(String g){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        String name = "digit";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM goal",null);
            if(cursor.moveToFirst()){
                do{
                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("name")))){
                        values.put("rate_d_bill", g);

                        db.update(TABLE_NAME5, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }
    }
    public void update_rate_lib(String g){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        String name = "digit";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM goal",null);
            if(cursor.moveToFirst()){
                do{
                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("name")))){
                        values.put("rate_d_lib", g);

                        db.update(TABLE_NAME5, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }
    }*/

    public void insertJobs(job_item jobs){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        /*String query = "select * from job";
        Cursor cursor = db.rawQuery(query,null);
        int count = cursor.getCount();
        values.put(COLUMN_ID,count);*/

        values.put(COLUMN_JOB_NAME, jobs.getJob_name());
        values.put(COLUMN_SALARY,jobs.getSalary_amount());
        values.put(COLUMN_SALARY_TYPE,jobs.getSalary_type());
        values.put(COLUMN_START_TIME,jobs.getStart_time());
        values.put(COLUMN_END_TIME,jobs.getEnd_time());
        values.put(COLUMN_START_DATE,jobs.getStart_date());
        values.put(COLUMN_OFF_DAY,jobs.getOff_day());
        values.put(COLUMN_NET_AMOUNT,"0");

        db.insert(TABLE_NAME,null,values);
    }
    public void insertBills(bills_item_details bills){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_BILLS_NAME, bills.getBills_name());
        values.put(COLUMN_SALARY,bills.getBills_amount());
        values.put(COLUMN_SALARY_TYPE,bills.getBills_type());
        values.put(COLUMN_START_TIME,bills.getStart_time());
        values.put(COLUMN_END_TIME,bills.getEnd_time());
        values.put(COLUMN_START_DATE,bills.getStart_date());
        values.put(COLUMN_OFF_DAY,bills.getOff_day());
        values.put(COLUMN_NET_AMOUNT,"0");

        db.insert(TABLE_NAME3,null,values);
    }

    public void insertAsset(String asset_name, String amount, String type, String date, String net){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_ASSET_NAME, asset_name);
        values.put(COLUMN_ASSET_AMOUNT,amount);
        values.put(COLUMN_ASSET_TYPE,type);
        values.put(COLUMN_ASSET_START_DATE,date);
        values.put(COLUMN_ASSET_NET_AMOUNT,net);

        db.insert(TABLE_NAME2,null,values);


    }

    public void insertLib(String lib_name, String amount, String type, String date, String net){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_LIB_NAME, lib_name);
        values.put(COLUMN_LIB_AMOUNT,amount);
        values.put(COLUMN_LIB_TYPE,type);
        values.put(COLUMN_LIB_START_DATE,date);
        values.put(COLUMN_LIB_NET_AMOUNT,net);

        db.insert(TABLE_NAME4,null,values);

    }

    public void delete_job(String jobs){
        db = this.getWritableDatabase();

        db.delete(TABLE_NAME,"job_name='"+jobs+"'",null);
    }

    public void delete_asset(String name){
        db = this.getWritableDatabase();

        db.delete(TABLE_NAME2,"asset_name='"+name+"'",null);
    }

    public void delete_lib(String name){
        db = this.getWritableDatabase();

        db.delete(TABLE_NAME4,"lib_name='"+name+"'",null);
    }

    public void delete_bills(String name){
        db = this.getWritableDatabase();

        db.delete(TABLE_NAME3,"bills_name='"+name+"'",null);

    }

    public boolean list_empty(String table_name){
        Boolean check;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM '"+table_name+"'",null);

            int count = cursor.getCount();
            if(count == 0){
                check = true;
            }
            else {
                check = false;
            }

        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }

        return check;
    }

    public void update_job_item(job_item j,String name){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM job",null);
            if(cursor.moveToFirst()){

                do{

                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("job_name")))){

                        values.put(COLUMN_JOB_NAME, j.getJob_name());
                        values.put(COLUMN_SALARY,j.getSalary_amount());
                        values.put(COLUMN_SALARY_TYPE,j.getSalary_type());
                        values.put(COLUMN_START_TIME,j.getStart_time());
                        values.put(COLUMN_END_TIME,j.getEnd_time());
                        values.put(COLUMN_START_DATE,j.getStart_date());
                        values.put(COLUMN_OFF_DAY,j.getOff_day());


                        db.update(TABLE_NAME, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }


    }
    public void update_bills_item(bills_item_details j, String name){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM bills",null);
            if(cursor.moveToFirst()){

                do{

                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("bills_name")))){

                        values.put(COLUMN_BILLS_NAME, j.getBills_name());
                        values.put(COLUMN_SALARY,j.getBills_amount());
                        values.put(COLUMN_SALARY_TYPE,j.getBills_type());
                        values.put(COLUMN_START_TIME,j.getStart_time());
                        values.put(COLUMN_END_TIME,j.getEnd_time());
                        values.put(COLUMN_START_DATE,j.getStart_date());
                        values.put(COLUMN_OFF_DAY,j.getOff_day());


                        db.update(TABLE_NAME3, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }


    }
    public void update_jobs_net_amount(String job_name,String amount){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id, job_name, net_amount FROM job",null);
            if(cursor.moveToFirst()){

                do{

                    if(job_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("job_name")))){

                        values.put(COLUMN_NET_AMOUNT, amount);

                        db.update(TABLE_NAME, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }


    }

    public void update_bills_net_amount(String bills_name,String amount){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM bills",null);
            if(cursor.moveToFirst()){

                do{

                    if(bills_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("bills_name")))){

                        values.put(COLUMN_NET_AMOUNT, amount);

                        db.update(TABLE_NAME3, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }


    }

    public void update_asset_net_amount(String asset_name,String amount){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM asset",null);
            if(cursor.moveToFirst()){

                do{

                    if(asset_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("asset_name")))){

                        values.put(COLUMN_ASSET_NET_AMOUNT, amount);

                        db.update(TABLE_NAME2, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }


    }

    public void update_lib_net_amount(String lib_name,String amount){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM lib",null);
            if(cursor.moveToFirst()){

                do{

                    if(lib_name.equals(cursor.getString(cursor.getColumnIndexOrThrow("lib_name")))){

                        values.put(COLUMN_ASSET_NET_AMOUNT, amount);

                        db.update(TABLE_NAME4, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }


    }

    public void update_asset_item(String name,String asset_name, String amount, String type, String date, String net){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM asset",null);
            if(cursor.moveToFirst()){

                do{

                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("asset_name")))){

                        values.put(COLUMN_ASSET_NAME, asset_name);
                        values.put(COLUMN_ASSET_AMOUNT,amount);
                        values.put(COLUMN_ASSET_TYPE,type);
                        values.put(COLUMN_ASSET_START_DATE,date);
                        values.put(COLUMN_ASSET_NET_AMOUNT,net);

                        db.update(TABLE_NAME2, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }



    }
    public void update_lib_item(String name,String lib_name, String amount, String type, String date, String net){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(" SELECT rowid _id,* FROM lib",null);
            if(cursor.moveToFirst()){

                do{

                    if(name.equals(cursor.getString(cursor.getColumnIndexOrThrow("lib_name")))){

                        values.put(COLUMN_LIB_NAME, lib_name);
                        values.put(COLUMN_LIB_AMOUNT,amount);
                        values.put(COLUMN_LIB_TYPE,type);
                        values.put(COLUMN_LIB_START_DATE,date);
                        values.put(COLUMN_LIB_NET_AMOUNT,net);

                        db.update(TABLE_NAME4, values, "_id="+cursor.getString(cursor.getColumnIndexOrThrow("_id")), null);

                        //Toast.makeText(this, "okkk", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                while(cursor.moveToNext());
            }
        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }



    }
}