package hungers.com.emroze.digits.lib;

import android.content.Context;
import android.content.SharedPreferences;

import hungers.com.emroze.digits.job.salary_rate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by emroze on 8/5/17.
 */

public class lib_goal_date_handler {
    public static String PREFS_NAME="digits";

    int start_day,start_month,start_year;

    private int daay,moonth,yeear;

    private Boolean check = false;

    public lib_goal_date_handler(){}

    public void timer (Context cc, String asset, String s_date, final double pre_rate){
        start_date_extract(cc,s_date);

        final SharedPreferences preference = cc.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final salary_rate s = new salary_rate();

        final double rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

        final double[] lack = {0.0};


        try{
            lack[0] = Double.valueOf(preference.getString("lib_"+asset,""));

        }catch(NumberFormatException e){
            lack[0] = 0.0;
        }

        final double[] net_worth = {0.0};
        net_worth[0] = Double.valueOf(preference.getString("net",""));
        int net_worth_ = (int) net_worth[0];
        final double goal = Double.valueOf(preference.getString("goal","")) + lack[0];


        Calendar c = Calendar.getInstance();

        int dday = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH)+1;
        int year = c.get(Calendar.YEAR);


        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = String.valueOf(dday)+" "+String.valueOf(month)+" "+String.valueOf(year);
        Date date1 = null;
        final Calendar startCal = Calendar.getInstance();
        try {
            date1 = myFormat.parse(inputString1);
            startCal.setTime(date1);
            //Toast.makeText(getApplicationContext(), startCal.getTime().toString(), Toast.LENGTH_SHORT).show();
        } catch (ParseException e) {
            e.printStackTrace();

        }







        final String[] ddd = {""};

        double rate = 0;

        if(lack[0] == 0){
            rate = pre_rate;
            final double[] finalRate = {rate};
            Thread t = new Thread() {

                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {


                            if(net_worth[0] <= goal && rate_d >= 0  && preference.getString("net_pos","").equals("1")) {

                                if(startCal.get(Calendar.DAY_OF_MONTH) == start_day
                                        && (startCal.get(Calendar.MONTH)+1) == start_month
                                        && startCal.get(Calendar.YEAR) == start_year){
                                    finalRate[0] = rate_d;
                                }

                                net_worth[0] = net_worth[0] + finalRate[0];


                                startCal.add(Calendar.DAY_OF_MONTH, 1);

                                ddd[0] =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                        +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                        +String.valueOf(startCal.get(Calendar.YEAR));

                                editor.putString("goal_date",ddd[0]);
                                editor.commit();
                            }

                        /*editor.putString("goal_date",String.valueOf(net_worth[0])+" "+String.valueOf(goal));
                        editor.commit();*/

                            Thread.sleep(5);
                        }
                    } catch (InterruptedException e) {
                    }
                }
            };
            t.start();
        }
        else {
            Thread t = new Thread() {

                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {


                            if(net_worth[0] <= goal && rate_d != 0 && preference.getString("net_pos","").equals("1")) {


                                net_worth[0] = net_worth[0] + rate_d;


                                startCal.add(Calendar.DAY_OF_MONTH, 1);

                                ddd[0] =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                        +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                        +String.valueOf(startCal.get(Calendar.YEAR));

                                editor.putString("goal_date",ddd[0]);
                                editor.commit();
                            }

                        /*editor.putString("goal_date",String.valueOf(net_worth[0])+" "+String.valueOf(goal));
                        editor.commit();*/

                            Thread.sleep(5);
                        }
                    } catch (InterruptedException e) {
                    }
                }
            };
            t.start();
        }

    }
    public boolean start_date_extract(Context cccc,String s_date){

        Boolean bbb=false;

        String year = "";
        String month = "";
        String dd = "";
        int i =0,j=0;

        for(i = 0; i < s_date.length();i++){

            if(s_date.charAt(i) == '.'){
                break;
            }
            dd = dd.concat(String.valueOf(s_date.charAt(i)));
        }
        for(j = i+1; j < s_date.length();j++){

            if(s_date.charAt(j) == '.'){
                break;
            }
            month=month.concat(String.valueOf(s_date.charAt(j)));
        }
        for (int k = j+1; k < s_date.length();k++){
            year = year.concat(String.valueOf(s_date.charAt(k)));
        }


        int s_year= Integer.valueOf(year);
        int s_month = Integer.valueOf(month);
        int s_dd = Integer.valueOf(dd);

        start_day = Integer.valueOf(dd);
        start_month = Integer.valueOf(month);
        start_year = Integer.valueOf(year);

        s_month=s_month-1;

        Calendar c = Calendar.getInstance();

        int c_dd = c.get(Calendar.DAY_OF_MONTH);
        int c_month = c.get(Calendar.MONTH);
        int c_year = c.get(Calendar.YEAR);

        if(c_year > s_year){
            bbb=true;
        }
        else if(c_year == s_year){
            if(c_month > s_month){
                bbb=true;
            }
            else if(c_month == s_month){
                if(c_dd >= s_dd){
                    bbb=true;
                }
            }
        }
        return bbb;
    }
}
