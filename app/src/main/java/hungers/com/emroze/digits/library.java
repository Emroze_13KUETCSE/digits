package hungers.com.emroze.digits;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class library extends AppCompatActivity {

    TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/font6.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/canaro_extra_bold.otf");

        t1 = (TextView ) findViewById(R.id.tv1);
        t2 = (TextView ) findViewById(R.id.tv2);
        t3 = (TextView ) findViewById(R.id.tv3);
        t4 = (TextView ) findViewById(R.id.tv4);
        t5 = (TextView ) findViewById(R.id.tv5);
        t6 = (TextView ) findViewById(R.id.tv6);
        t7 = (TextView ) findViewById(R.id.tv7);
        t8 = (TextView ) findViewById(R.id.tv8);
        t9 = (TextView ) findViewById(R.id.tv9);
        t10 = (TextView ) findViewById(R.id.tv10);
        t11 = (TextView ) findViewById(R.id.tv11);
        t12 = (TextView ) findViewById(R.id.tv12);
        t13 = (TextView ) findViewById(R.id.tv13);
        t14 = (TextView ) findViewById(R.id.tv14);
        t15 = (TextView ) findViewById(R.id.tv15);
        t16 = (TextView ) findViewById(R.id.tv16);

        t1.setTypeface(typeface2);
        t2.setTypeface(typeface);
        t3.setTypeface(typeface);
        t4.setTypeface(typeface);
        t5.setTypeface(typeface);
        t6.setTypeface(typeface);
        t7.setTypeface(typeface);
        t8.setTypeface(typeface);
        t9.setTypeface(typeface);
        t10.setTypeface(typeface);
        t11.setTypeface(typeface);
        t12.setTypeface(typeface);
        t13.setTypeface(typeface);
        t14.setTypeface(typeface);
        t15.setTypeface(typeface);
        t16.setTypeface(typeface);


    }
}
