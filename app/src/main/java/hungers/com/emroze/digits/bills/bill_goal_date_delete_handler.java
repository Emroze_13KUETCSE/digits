package hungers.com.emroze.digits.bills;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by emroze on 8/5/17.
 */

public class bill_goal_date_delete_handler {
    public static String PREFS_NAME="digits";

    private int daay,moonth,yeear;

    private Boolean check = false;

    public bill_goal_date_delete_handler(){}

    public void d_timer(Context c1, final String job_name, final String s_time, final String e_time, final String off_day, final String amount, final String s_type , final String start_date){

        final SharedPreferences preference = c1.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        final double rate_d = Double.valueOf(preference.getString("net_job_rate","")) + Double.valueOf(preference.getString("net_asset_rate",""))
                - Double.valueOf(preference.getString("net_lib_rate",""))- Double.valueOf(preference.getString("net_bill_rate",""));

        final double[] net_worth = {0.0};
        net_worth[0] = Double.valueOf(preference.getString("net",""));
        int net_worth_ = (int) net_worth[0];
        final double goal = Double.valueOf(preference.getString("goal",""));

        Calendar c = Calendar.getInstance();

        int dday = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH)+1;
        int year = c.get(Calendar.YEAR);


        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = String.valueOf(dday)+" "+String.valueOf(month)+" "+String.valueOf(year);
        Date date1 = null;
        final Calendar startCal = Calendar.getInstance();
        try {
            date1 = myFormat.parse(inputString1);
            startCal.setTime(date1);
            //Toast.makeText(getApplicationContext(), startCal.getTime().toString(), Toast.LENGTH_SHORT).show();
        } catch (ParseException e) {
            e.printStackTrace();

        }

        final String[] ddd = {""};



        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {


                        if(net_worth[0] <= goal && rate_d > 0 && preference.getString("net_pos","").equals("1")) {


                            net_worth[0] = net_worth[0] + rate_d;


                            startCal.add(Calendar.DAY_OF_MONTH, 1);

                            ddd[0] =String.valueOf(startCal.get(Calendar.DAY_OF_MONTH))+"/"
                                    +String.valueOf(startCal.get(Calendar.MONTH)+1)+"/"
                                    +String.valueOf(startCal.get(Calendar.YEAR));

                            editor.putString("goal_date",ddd[0]);
                            editor.commit();
                        }

                        /*editor.putString("goal_date",String.valueOf(net_worth[0])+" "+String.valueOf(goal));
                        editor.commit();*/

                        Thread.sleep(5);
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }

}
