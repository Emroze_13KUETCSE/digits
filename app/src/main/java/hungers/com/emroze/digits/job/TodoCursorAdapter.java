package hungers.com.emroze.digits.job;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by emroze on 6/3/17.
 */

public class TodoCursorAdapter extends CursorAdapter {

    public TodoCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(hungers.com.emroze.digits.R.layout.tv_in_job_listview, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tv_job_name = (TextView) view.findViewById(hungers.com.emroze.digits.R.id.tv_job_name_lv);
        TextView big = (TextView) view.findViewById(hungers.com.emroze.digits.R.id.tv_big_lv);

        TextView amount = (TextView) view.findViewById(hungers.com.emroze.digits.R.id.tv_amount_lv);

        Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/canaro_extra_bold.otf");
        tv_job_name.setTypeface(type);
        big.setTypeface(type);
        amount.setTypeface(type);


        String body = cursor.getString(cursor.getColumnIndexOrThrow("job_name"));

        String amount_and_type = cursor.getString(cursor.getColumnIndexOrThrow("salary_amount")) +
                "/"+ cursor.getString(cursor.getColumnIndexOrThrow("salary_type"));

        String str = String.valueOf(body.charAt(0));
        big.setText(str);
        amount.setText(amount_and_type);
        tv_job_name.setText(body);
    }
}
