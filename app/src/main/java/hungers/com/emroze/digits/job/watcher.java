package hungers.com.emroze.digits.job;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.widget.Toast;

import hungers.com.emroze.digits.database.DatabaseHelper;

import java.util.Calendar;

public class watcher extends Service {
    public static String PREFS_NAME="digits";
    String job_name,amount,s_type,s_time,e_time,s_date,off_day = "";
    DatabaseHelper db_helper = new DatabaseHelper(this);

    public watcher() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate()
    {

        /*RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.widget);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.drawer).setContent(
                remoteViews);

        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.button1, resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        startForeground(999, mBuilder.build());*/

    }
    @Override
    public int onStartCommand(Intent intent, final int flags, int startId){




        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

        final SharedPreferences preference =getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final SharedPreferences.Editor editor=preference.edit();

        Calendar c = Calendar.getInstance();

        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int sec = c.get(Calendar.SECOND);
        int date = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        editor.putInt("hour",hour);
        editor.putInt("min",min);
        editor.putInt("sec",sec);
        editor.putInt("date",date);
        editor.putInt("month",month);
        editor.putInt("year",year);
        editor.putString("trigger","on");
        editor.putString("resume_main","on");
        editor.putString("page","0");
        editor.putString("dropbox","true");
        editor.commit();

        //Toast.makeText(getApplicationContext(),"removed "+String.valueOf(min), Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Toast.makeText(getApplicationContext(),"Destroy" , Toast.LENGTH_SHORT).show();
    }
}
