package hungers.com.emroze.digits.bills;

import android.content.Context;
import android.content.SharedPreferences;

import hungers.com.emroze.digits.job.salary_rate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by emroze on 6/4/17.
 */

public class pre_date_bills_lack {
    public static String PREFS_NAME="digits";

    public pre_date_bills_lack() {
    }
    public Double calc(Context contex, String start_time, String end_time, int sec, int min, int hour, int date, int month, int year){

        final SharedPreferences preference =contex.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);


        /*Calendar c1 = Calendar.getInstance();*/
        salary_rate ss = new salary_rate();

        //current hour
        /*int hour = c1.get(Calendar.HOUR_OF_DAY);
        int min = c1.get(Calendar.MINUTE);
        int sec = c1.get(Calendar.SECOND);

        int date = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH);
        int year = c1.get(Calendar.YEAR);*/

        //start hour
        int s_hour = preference.getInt("s_hour",0);
        int s_min = preference.getInt("s_min",0);
        int s_sec = 0;

        int s_date = preference.getInt("s_date",0);
        int s_month = preference.getInt("s_month",0);
        int s_year = preference.getInt("s_year",0);


        //start time and end time
        int start_hour = ss.hour(start_time);
        int start_min = ss.min(start_time);
        int end_hour = ss.hour(end_time);
        int end_min = ss.min(end_time);

        //convert to sec
        Double s_t = (Double)(((start_hour*60.0)+start_min)*60.0);
        Double e_t = (Double)(((end_hour*60.0)+end_min)*60.0);

        Double s = (Double)((((s_hour*60.0)+s_min)*60.0)+s_sec);
        Double c = (Double)((((hour*60.0)+min)*60.0)+sec);

        //convert to day
        Double c_d = (Double)((((year*12.0)+month+1)*30.0)+date);
        Double s_d = (Double)((((s_year*12.0)+s_month)*30.0)+s_date);
        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        String inputString1 = String.valueOf(s_date)+" "+String.valueOf(s_month)+" "+String.valueOf(s_year);
        String inputString2 = String.valueOf(date)+" "+String.valueOf(month+1)+" "+String.valueOf(year);

        Double calc_sec = 0.0;

        Double diff = 0.0;

        if(s_d < c_d){
            calc_sec = (c_d -s_d)*(e_t-s_t);

            if(c > s_t && c < e_t){
                calc_sec = calc_sec+(c-s_t);
            }
            else if(c >= e_t){
                calc_sec = calc_sec + (e_t-s_t);
            }
        }
        else if(s_d.equals(c_d)){
            if(c >= s_t && c < e_t){
                calc_sec = calc_sec+(c-s_t);
            }
            else if(c >= e_t){
                calc_sec = calc_sec + (e_t-s_t);
            }
        }


        return calc_sec;
    }
    public static Double getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        Double workDays = 0.0;
        Double closeDays = 0.0;

        //Return 0 if start and end are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0.0;
        }

        if (startCal.getTimeInMillis() >= endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
        }

        do {
            //excluding start date
            startCal.add(Calendar.DAY_OF_MONTH, 1);
            if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                ++closeDays;
            }
            if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY){
                ++closeDays;
            }
            ++workDays;

        } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        return workDays-closeDays;
    }
}
